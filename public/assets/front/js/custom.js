/* Write here your custom javascript codes */
$(function() {
    App.init();
    App.initCounter();
    App.initParallaxBg();
    FancyBox.initFancybox();
    MSfullWidth.initMSfullWidth();
    OwlCarousel.initOwlCarousel();
    StyleSwitcher.initStyleSwitcher();
});