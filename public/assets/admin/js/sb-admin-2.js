$(function() {

    if($('#dataTables-example').length > 0){
        $('#dataTables-example').DataTable({
                responsive: true,
                /*"order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }],
                "aaSorting": [],*/
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/Bulgarian.json"
                },
                "bStateSave": true,
                "fnStateSave": function (oSettings, oData) {
                    localStorage.setItem('DataTables_' + window.location.pathname, JSON.stringify(oData));
                },
                "fnStateLoad": function (oSettings) {
                    var data = localStorage.getItem('DataTables_' + window.location.pathname);
                    return JSON.parse(data);
                }
        });
    }

    $('#side-menu').metisMenu();

    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

    $('.action-with-confirm').click(function () {
        var resource_id = $(this).data('id');
        bootbox.confirm($(this).data('message'), function (result) {
            if (result) {
                $('#delete_id_' + resource_id).submit();
            }
        });
    });

    if($('.sortable-boxes').length){
        $('.sortable-boxes').sortable({
                items: $(this).data('which-class'),
                cursor: 'move',
                opacity: 0.5,
                containment: "parent",
                distance: 20,
                tolerance: 'pointer',
                update: function(e, ui){
                var data = $(this).sortable('serialize');
                var this_ = $(this);
                $.ajax({
                    data: data,
                    dataType: 'json',
                    type: 'POST',
                    url: $(this_).data('url')
                }).done(function (data) {
                    //window.location.href = $(this_).data('url-redirect');
                }).fail(function () {

                });
            }
        });
    }

    if($('.sortable-categories').length){
        $('.sortable-categories').sortable({
            items: $(this).data('which-class'),
            cursor: 'move',
            opacity: 0.5,
            containment: "parent",
            distance: 20,
            tolerance: 'pointer',
            stop: function(e, ui){
            var data = $(this).sortable('serialize');
            console.log(data);
            var this_ = $(this);
           /* $.ajax({
                data: data,
                dataType: 'json',
                type: 'POST',
                url: $(this_).data('url')
            }).done(function (data) {
                //window.location.href = $(this_).data('url-redirect');
            }).fail(function () {

            });*/
            }
        });
    }

    if($('.nestable').length){
        //collapsed all elements
        $(".nestable").nestable({collapsedClass:'dd-collapsed'});
        $('.nestable').nestable('collapseAll').on('change', function () {
            var json_text = $(this).nestable('serialize');
            var nav_url = $(this).data('url');
            /*var sorting_data = {};
            sorting_data = $.map(json_text, function (value, index) {
                return [value];
            });*/
            $.post(nav_url, {
                d: $(this).nestable('serialize')
            }, function (response) {

            });
        });
    }

    $('.ajax-image-delete').click(function(){
        var this_ = $(this);
        $.ajax({
            url: $(this_).data('url')
        }).done(function (data) {
            $('.' + $(this_).data('for-remove')).remove();
        }).fail(function () {

        });

    });

    var allFiles = '';
    $('.upload').change(function(){
        if (this.files) {
            var allFiles = [];
            for(var f = 0; f < this.files.length; f += 1){
                $('.n-message').html('');
                if(!this.files[f].type.match("image.*")) {
                    $('.n-message').html('<div class="alert alert-danger">' + not_valid_image_format + '</div>');
                    return;
                }
                var reader = new FileReader();
                var html = '';
                n = 0;
                reader.onload = function (e) {
                    var title_input = '<input type="text" class="form-control noPlaceholder" name="image_title[]" data-placeholder="' + write_a_title + '" placeholder="' + write_a_title + '">';

                    $('.for-form-upload').html('');

                    html += ['<div class="thumb-gallery" id="item-' + n + '">',
                        '<a href="#" style="z-index: 10;">',
                        '    <!--button class="delete-img" type="button" data-item="' + n + '" style="z-index: 100;"></button-->',
                        '    <img src="' + e.target.result + '" class="img-resonsive" style="width: 200px;" alt="">',
                        '</a>',
                        (typeof is_title_input !== 'undefined' && is_title_input? title_input: ''),
                    '</div>'].join('');
                    $('.for-form-upload').append(html);

                    $('.thumb-gallery a .delete-img').click(function(e){
                        e.preventDefault();
                        $(this).closest('div').remove();

                    });
                    n++;

                }
                reader.readAsDataURL(this.files[f]);
            }
        }
    });

    if($(".thumb-container").length){
        $(".thumb-container").sortable({
            items: '.thumb-gallery',
            cursor: 'move',
            opacity: 0.5,
            containment: "parent",
            distance: 20,
            tolerance: 'pointer',
            update: function(e, ui){
                $('input[name="sorting"]').val($(this).sortable('toArray').toString());
            }
        });
    }

    if($(".old-images").length){
        $(".old-images").sortable({
            items: '',
            cursor: 'move',
            opacity: 0.5,
            containment: "parent",
            distance: 20,
            tolerance: 'pointer',
            update: function(e, ui){
                $('input[name="sorting"]').val($(this).sortable('toArray').toString());
            }
        });
    }

    if($('.tinymce').length){
        tinymce.init({
            selector: 'textarea',
            height: 300,
            relative_urls: false,
            remove_script_host: false,
            convert_urls: true,
            language: tinyLang,
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools image'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            autosave_ask_before_unload: false,
            autosave_retention: '60m',
            file_browser_callback: function (d, a, b, c) {
                tinyMCE.activeEditor.windowManager.open({
                    file: '/assets/admin/js/olmdsvhwew/browse.php?opener=tinymce4&field=' + d + '&type=' + b + '&lang=bg&dir=' + b + '/public',
                    title: 'Качи снимки',
                    width: 700,
                    height: 500,
                    inline: true,
                    close_previous: false
                }, {
                    window: c,
                    input: d
                });
                return false
            },
            style_formats: [
                {title: 'Image responsive class', selector: 'img', classes: 'img-responsive'},
                {title: 'Image left class', selector: 'img', classes: 'imgleft'},
                {title: 'Image right class', selector: '*', classes: 'imgright'},
            ],
            /*formats: {
                alignleft: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
                aligncenter: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
                alignright: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
                alignfull: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
                bold: {inline : 'span', 'classes' : 'bold'},
                italic: {inline : 'span', 'classes' : 'italic'},
                underline: {inline : 'span', 'classes' : 'underline', exact : true},
                strikethrough: {inline : 'del'},
                forecolor: {inline : 'span', classes : 'forecolor', styles : {color : '%value'}},
                hilitecolor: {inline : 'span', classes : 'hilitecolor', styles : {backgroundColor : '%value'}},
                customformat: {block : 'h1', attributes : {title : 'Header'}, styles : {color : 'red'}},
            },*/
            style_formats_merge: true,
            content_css: [
                base_url + '/assets/front/plugins/bootstrap/css/bootstrap.min.css',
                base_url + '/assets/front/css/style.css'
              ]
        });
    }

    if($('.datepicker').length){
        showDatepicker();
    }

    function showDatepicker() {
        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            firstDay: 1,
            dateFormat: 'yy-mm-dd',
        });
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
