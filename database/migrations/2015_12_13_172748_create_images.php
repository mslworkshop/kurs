<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('object_id')->unsigned()->index();
            $table->string('title');
            $table->text('description');
            $table->string('filename', 100);
            $table->text('object_type');
            $table->tinyinteger('show')->unsigned()->default(1)->index();
            $table->integer('sortorder')->index();
            $table->timestamps();
            //$table->softDeletes();
        });

        /*Schema::create('images_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->unsigned();
            $table->string('locale')->index();

            $table->string('title');
            $table->text('description');

            $table->unique(['news_id','locale']);
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
        //Schema::drop('images_translations');
    }
}
