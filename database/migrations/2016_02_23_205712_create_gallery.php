<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Gallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable()->index();
            $table->tinyinteger('level')->unsigned()->nullable();
            $table->string('slug')->unique();
            $table->tinyinteger('home')->unsigned()->default(0);
            $table->tinyinteger('published')->unsigned()->default(1)->index();
            $table->integer('sortorder')->index();
            $table->timestamps();
        });

        Schema::create('gallery_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gallery_id')->unsigned();
            $table->string('locale')->index();

            $table->string('title');
            $table->string('description');
            $table->string('short_description');
            $table->string('meta_title');
            $table->string('meta_description');

            $table->unique(['gallery_id','locale']);
            $table->foreign('gallery_id')->references('id')->on('gallery')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery');
        Schema::drop('gallery_translations');
    }
}
