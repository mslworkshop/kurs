<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable()->index();
            $table->tinyinteger('level')->unsigned()->nullable()->index();
            $table->string('slug')->unique();
            $table->tinyinteger('in_menu')->unsigned()->default(1)->index();
            $table->tinyinteger('home')->unsigned()->default(0)->index();
            $table->tinyinteger('published')->unsigned()->default(1)->index();
            $table->integer('sortorder')->index();
            $table->timestamps();
            //$table->softDeletes();
        });

        Schema::create('pages_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pages_id')->unsigned();
            $table->string('locale')->index();

            $table->string('title');
            $table->text('content');
            $table->text('short_description');
            $table->string('meta_title');
            $table->string('meta_description');

            $table->unique(['pages_id','locale']);
            $table->foreign('pages_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
        Schema::drop('pages_translations');
    }
}
