<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {

        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->tinyinteger('home')->unsigned()->default(0)->index();
            $table->tinyinteger('publish')->unsigned()->default(1)->index();
            //$table->integer('sortorder')->index();
            $table->timestamps();
            //$table->softDeletes();
        });

        Schema::create('news_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->unsigned();
            $table->string('locale')->index();

            $table->string('title');
            $table->text('content');
            $table->string('meta_title');
            $table->string('meta_description', 255);

            $table->unique(['news_id','locale']);
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
        Schema::drop('news_translations');
    }
}
