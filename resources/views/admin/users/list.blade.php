@extends('admin.layout')

@section('css')

<!-- DataTables CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
@stop

@section('content')

<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('admin/user.name') }}</th>
                                            <th>{{ trans('admin/user.email') }}</th>
                                            <th>{{ trans('admin/app.edit') }}</th>
                                            <!-- <th>{{ trans('admin/app.delete') }}</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($all_users as $user)
                                        <tr class="odd gradeX">
                                            <td>
                                                {{ $user->name }}
                                            </td>
                                            <td>
                                                <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                            </td>
                                            <td>
                                                <a href="{{ route('users.edit', $user->id) }}">
                                                    <button id="sample_editable_1_new" class="btn btn-warning">
                                                        {{ trans('admin/app.edit') }} <i class="fa fa-edit"></i>
                                                    </button>
                                                </a>

                                                @if(Auth::user()->id !=  $user->id)
                                                <button class="action-with-confirm btn btn-danger" data-id="{{ $user->id }}" data-message="{{ trans('admin/app.delete_text') }}">
                                                    {{ trans('admin/app.delete') }} <i class="fa fa-trash"></i>
                                                </button>
                                                <form action="{{ route('users.destroy', $user->id) }}" id="delete_id_{{ $user->id }}" method="POST">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@stop

@section('js')
<!-- DataTables JavaScript -->
<script src="{{ asset('assets/admin/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
@stop