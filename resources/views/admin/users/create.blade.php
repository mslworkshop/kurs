@extends('admin.layout')

@section('content')
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        @include('errors.admin.errors')
                        <form role="form" method="POST" action="{{ URL::current() }}">
                            <div class="form-group">
                                <label>{{trans('admin/user.name')}} *</label>
                                <input type="text" class="form-control" required name="name" placeholder="{{trans('admin/user.name')}}" value="{{ isset($object->name) ? $object->name: old('name') }}">
                            </div>
                            <div class="form-group">
                                <label>{{trans('admin/user.email')}} *</label>
                                <input type="email" class="form-control" required name="email" placeholder="{{trans('admin/user.email')}}" value="{{ isset($object->email) ? $object->email: old('email') }}">
                            </div>
                            <div class="form-group">
                                <label>{{trans('admin/user.password')}}</label>
                                <input type="password" class="form-control" name="password" placeholder="{{trans('admin/user.password')}}" value="">
                                <p class="help-block">{{ trans('admin/user.paswword_must_min_6_symbols') }}</p>
                            </div>
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-default">{{ trans('admin/app.submit') }}</button>
                            <a href="{{ route('users.list') }}" class="btn btn-default">Cancel</a>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop