@extends('admin.layout')

@section('css')

<!-- DataTables CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">

@stop

@section('content')

<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>{{ trans('validation.attributes.title') }}</th>
                                            <th>{{ trans('validation.attributes.date') }}</th>
                                            <th>{{ trans('admin/app.published') }}</th>
                                            <th>{{ trans('admin/app.show_in_home_page') }}</th>
                                            <th>{{ trans('admin/app.edit') }}</th>
                                            <!-- <th>{{ trans('admin/app.delete') }}</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($objects as $object)
                                        <tr class="odd gradeX for-sortable" id="item-{{ $object->id }}">
                                            <td>
                                                {{ $object->id }}
                                            </td>
                                            <td>
                                                {{ $object->title }}
                                            </td>
                                            <td>
                                                {{ $object->created_at->format('Y-m-d') }}
                                            </td>
                                            <td>
                                                <form action="{{ route('news.publish', $object->id) }}" method="POST">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="published" value="{{ $object->published? 0: 1 }}">
                                                    <button class="btn btn-btn btn-{{ $object->published? 'success': 'primary' }}">
                                                        {{ trans('admin/app.published') }} <i class="fa fa-cut"></i>
                                                    </button>
                                                </form>
                                            </td>
                                            <td>
                                                <form action="{{ route('news.home', $object->id) }}" method="POST">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="home" value="{{ $object->home? 0: 1 }}">
                                                    <button class="btn btn-{{ $object->home? 'success': 'primary' }}">
                                                       {{ trans('admin/app.home_page') }} <i class="fa fa-home"></i>
                                                    </button>
                                                </form>
                                            </td>
                                            <td>
                                                <a href="{{ route('news.edit', $object->id) }}">
                                                    <button id="sample_editable_1_new" class="btn btn-warning">
                                                        {{ trans('admin/app.edit') }} <i class="fa fa-edit"></i>
                                                    </button>
                                                </a>

                                                <button class="action-with-confirm btn btn-danger" data-id="{{ $object->id }}" data-message="{{ trans('admin/app.delete_text') }}">
                                                    {{ trans('admin/app.delete') }} <i class="fa fa-trash"></i>
                                                </button>
                                                <form action="{{ route('news.destroy', $object->id) }}" id="delete_id_{{ $object->id }}" method="POST">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@stop

@section('js')
<script src="{{ asset('assets/admin/js/jquery-ui.min.js') }}"></script>
<!-- DataTables JavaScript -->
<script src="{{ asset('assets/admin/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
@stop