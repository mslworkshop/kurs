@extends('admin.layout')

@section('css')

<!-- DataTables CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">

<link href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
@stop

@section('content')

<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>{{ trans('validation.attributes.photo') }}</th>
                                            <th>{{ trans('admin/app.edit') }}</th>
                                            <!-- <th>{{ trans('admin/app.delete') }}</th> -->
                                        </tr>
                                    </thead>
                                    <tbody class="sortable-boxes" data-which-class="for-sortable" data-url-redirect="{{ URL::current() }}" data-url="{{ route('simple.images.sorting') }}">
                                        @foreach ($objects as $object)
                                        <tr class="odd gradeX for-sortable" id="item-{{ $object->id }}">
                                            <td>{{ $object->id }}</td>
                                            <td>
                                                @if($object->filename)
                                                <img src="{{ url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SIMPLE_IMAGES_DIR').$object->object_id.'/'.$object->filename) }}"  style="width: 200px;">
                                                @endif
                                            </td>
                                            <td>

                                                <button class="action-with-confirm btn btn-danger" data-id="{{ $object->id }}" data-message="{{ trans('admin/app.delete_text') }}">
                                                    {{ trans('admin/app.delete') }} <i class="fa fa-trash"></i>
                                                </button>
                                                <form action="{{ route('simple.images.destroy', $object->id) }}" id="delete_id_{{ $object->id }}" method="POST">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@stop

@section('js')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- DataTables JavaScript -->
<script src="{{ asset('assets/admin/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
@stop