@extends('admin.layout')

@section('content')
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                        @forelse($emails as $email)
                        {{ $email->email }},
                        @empty
                        No emails in db yet.
                        @endforelse
                        </p>
                        <div class="n-message">@include('errors.admin.errors')</div>
                        <form role="form" method="POST" action="{{ URL::current() }}" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Subject *</label>
                                <input type="text" class="form-control" required name="subject" placeholder="" value="{{ old('subject') }}">
                            </div>
                            <div class="form-group">
                                <label>Текст на email *</label>
                                <textarea class="form-control tinymce" name="content" placeholder="">{{ old('content') }}</textarea>
                            </div>
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-default">{{ trans('admin/app.send') }}</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop

@section('js')
<script src="{{asset('assets/admin/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
    var tinyLang = @if(App::getLocale() == 'bg')'bg_BG' @else'' @endif;
</script>
@stop