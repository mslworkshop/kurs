<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ trans('app.site_name') }}</title>
    <style type="text/css">
        * { margin:0; padding:0;}
        body, td {font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #9a9a9a;}
        a {font-size: 15px; color: #0018a8; text-decoration: none;}
        a:hover { text-decoration: underline;}
        h1{font-size:medium}
    </style>
</head>
<body bgcolor="#ffffff" link="#ffffff" alink="#ffffff" vlink="#ffffff">
    <table width="800" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff">
        <tr>
            <td valign="top">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <!-- <tr><td colspan="3"><img src="{{ asset('images/header-email.jpg') }}" width="800" height="162" alt="" /></td></tr> -->
                    <tr><td colspan="3" height="40">&nbsp;</td></tr>
                    <tr>
                        <td width="40">&nbsp;</td>
                        <td width="720">
                            <div>
                                {!! $content !!}
                            </div>
                        </td>
                        <td width="40">&nbsp;</td>
                    </tr>
                    <tr><td colspan="3" height="40">&nbsp;</td></tr>
                    <tr>
                        <td bgcolor="#005670" height="20" colspan="3">
                            <div style="font-size:11px; color: #ffffff; text-align: right; margin-right: 20px">
                                {{ trans('app.copyright') }}
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>