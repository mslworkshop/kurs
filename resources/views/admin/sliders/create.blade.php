@extends('admin.layout')

@section('content')
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="n-message">@include('errors.admin.errors')</div>
                        <form role="form" method="POST" action="{{ URL::current() }}" enctype="multipart/form-data">
                            @foreach(\Config::get('translatable.locales') as $k => $locale)
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('validation.attributes.title') }} *</label>
                                <input type="text" class="form-control" required name="title_{{ $locale }}" placeholder="{{ trans('validation.attributes.title') }}" value="{{ isset($object) ? $object->translate($locale)->title: old('title_'.$locale) }}">
                            </div>
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('validation.attributes.text') }} 1 *</label>
                                <input type="text" class="form-control" required name="text1_{{ $locale }}" placeholder="{{ trans('validation.attributes.text') }} 1" value="{{ isset($object) ? $object->translate($locale)->text1: old('text1_'.$locale) }}">
                            </div>
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('validation.attributes.text') }} 2 *</label>
                                <input type="text" class="form-control" required name="text2_{{ $locale }}" placeholder="{{ trans('validation.attributes.text') }} 2" value="{{ isset($object) ? $object->translate($locale)->text2: old('text2_'.$locale) }}">
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label>{{ trans('admin/app.link') }} *</label>
                                <input type="text" class="form-control" required name="url" placeholder="some-item-name" value="{{ isset($object)? $object['url']: old('url') }}">
                                <p class="help-block">{{ trans('admin/slider.what_page_is_show_when_click_event') }}</p>
                            </div>
                            <label for="">{{ trans('admin/app.image') }}</label>
                            <div class="thumb-container for-form-upload">
                            @if(isset($object) && $object->image)
                                <div class="thumb-gallery">
                                    <img src="{{ url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SLIDERS_DIR').$object->image) }}?new" class="img-resonsive" style="width: 200px;" alt="{{ isset($object) ? $object->translate($locale)->title: '' }}">
                                    <div class="">
                                        <button type="button" data-for-remove="thumb-gallery" data-url= "{{ route('sliders.deleteimage', $object->id) }}" class="btn btn-outline btn-danger btn-xs ajax-image-delete">{{ trans('admin/app.delete') }}</button>
                                    </div>
                                </div>
                            @endif()
                            </div>
                            <div class="form-group">
                                <label for=""></label>
                                <input type="file" name="photo" accept="image/*" class="upload">
                                <p class="help-block">{{ trans('admin/app.max_file_size_to_upload') }}{{ $max_file_to_upload }}</p>
                            </div>
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-default">{{ trans('admin/app.submit') }}</button>
                            <a href="{{ route('sliders.list') }}" class="btn btn-default">{{ trans('admin/app.cancel') }}</a>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop

@section('js')
<script type="text/javascript" async>
    var not_valid_image_format = '{{ trans('admin/app.this_is_must_be_an_image') }}';
    var write_a_title = '';
    var is_title_input = false;
</script>
@stop