<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('app.site_name') }} Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('assets/admin/bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">

    @yield('css')

    <!-- Timeline CSS -->
    <link href="{{ asset('assets/admin/css/timeline.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/admin/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('assets/admin/bower_components/morrisjs/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('assets/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv') }}"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min') }}"></script>
    <![endif]-->

    <script type="text/javascript">
        var base_url = "{{ '//'.parse_url(url('/'), PHP_URL_HOST) }}";
    </script>

    <!-- jQuery -->
    <script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/admin/bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <!--<script src="{{ asset('assets/admin/bower_components/raphael/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/admin/bower_components/morrisjs/morris.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/morris-data.js') }}"></script> -->

    <script src="{{ asset('assets/admin/js/bootbox.min.js') }}"></script>

    @yield('js')

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/admin/js/sb-admin-2.js') }}"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin">{{ trans('app.site_name') }} Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ route('users.list') }}"><i class="fa fa-user fa-fw"></i> {{ trans('admin/user.users') }}</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/admin/logout"><i class="fa fa-sign-out fa-fw"></i> {{ trans('admin/user.logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            /input-group
                        </li> -->
                        <!-- <li>
                            <a href="{{ route('admin.home') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li> -->
                        <li>
                            <a href="{{ route('sliders.list') }}"><i class="fa fa-photo fa-fw"></i> {{ trans('admin/slider.slider') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('pages.list') }}"><i class="fa fa-list-alt fa-fw"></i> {{ trans('admin/pages.pages') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('send.emails') }}"><i class="fa fa-send fa-fw"></i> {{ trans('admin/app.newsletter') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('simple.images.list') }}"><i class="fa fa-image fa-fw"></i> {{ trans('admin/app.gallery') }}</a>
                        </li>
                        <!-- <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Panels and Wells</a>
                                </li>
                            </ul>

                        </li> -->
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ $location_info or ''}} {{ $base_location or 'Home'}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                     @if (session('created'))
                    <div class="alert alert-success">
                        {{ Session('created') }}
                    </div>
                    @endif
                    @if (session('updated'))
                    <div class="alert alert-info">
                        {{ Session('updated') }}
                    </div>
                    @endif
                    @if (session('deleted'))
                    <div class="alert alert-danger">
                        {{ Session('deleted') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                @if(isset($add_button) && $add_button)
                <div class="col-md-12">
                    <a href="{{ $add_button }}" class="btn btn-primary">{{ $add_button_text or trans('admin/app.add') }}</a>
                </div>
                @endif
            </div>


            @yield('content')

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>

</html>
