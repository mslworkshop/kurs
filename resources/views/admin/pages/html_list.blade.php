

        <li class="dd-item dd3-item" data-id="{{ $object['id'] }}">
            <div class="dd-handle dd3-handle"></div>
            <div class="dd3-content">
                <div class="row">
                    <div class="col-md-10">
                        {{ $object['title'] }}
                    </div>
                    <div class="col-md-1">
                        <form action="{{ route('pages.publish', $object['id']) }}" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="published" value="{{ $object['published']? 0: 1 }}">
                            <button class="btn btn-btn btn-{{ $object['published']? 'success': 'primary' }}" title="{{ trans('admin/app.published') }}">
                                <i class="fa fa-cut"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-1">
                        <form action="{{ route('pages.menu', $object['id']) }}" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="in_menu" value="{{ $object['in_menu']? 0: 1 }}">
                            <button class="btn btn-{{ $object['in_menu']? 'success': 'primary' }}" title="{{ trans('admin/pages.in_menu_bar') }}">
                               <i class="glyphicon glyphicon-menu-hamburger"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-1">
                        <form action="{{ route('pages.home', $object['id']) }}" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="home" value="{{ $object['home']? 0: 1 }}">
                            <button class="btn btn-{{ $object['home']? 'success': 'primary' }}" title="{{ trans('admin/app.home_page') }}">
                                <i class="fa fa-home"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-1">
                        <a href="{{ route('pages.edit', $object['id']) }}">
                            <button id="sample_editable_1_new" class="btn btn-warning" title="{{ trans('admin/app.edit') }}">
                                 <i class="fa fa-edit"></i>
                            </button>
                        </a>
                    </div>
                    <div class="col-md-1">
                        <button class="action-with-confirm btn btn-danger" alt="{{ trans('admin/app.delete') }}" data-id="{{ $object['id'] }}" data-message="{{ trans('admin/app.delete_text') }}">
                             <i class="fa fa-trash"></i>
                        </button>
                        <form action="{{ route('pages.destroy', $object['id']) }}" id="delete_id_{{ $object['id'] }}" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                        </form>
                    </div>
                </div>
            </div>
            @if(!empty($object['children']))
             <ol class="dd-list">
                @each('admin.pages.html_list', $object['children'], 'object')
            </ol>
            @endif
        </li>

