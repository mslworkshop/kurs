@extends('admin.layout')

@section('css')
<link href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
@stop

@section('content')
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="n-message">@include('errors.admin.errors')</div>
                        <form role="form" method="POST" action="{{ URL::current() }}" enctype="multipart/form-data">
                            @foreach(\Config::get('translatable.locales') as $k => $locale)
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('validation.attributes.title') }} *</label>
                                <input type="text" class="form-control title-for-slug" onkeyup="slugify()" required name="title_{{ $locale }}" placeholder="{{ trans('validation.attributes.title') }}" value="{{ isset($object) ? $object->translate($locale)->title: old('title_'.$locale) }}">
                            </div>
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>Подкатегория </label>
                                <input type="hidden" name="old_cat" value="{{ (!empty($object['parent_id'])? $object['parent_id']: '') }}">
                                <select name="parent_id_level" class="form-control">
                                    <option value="0|0">--- без категория ---</option>
                                    {!! $categories->htmlList() !!}
                                </select>
                            </div>
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('admin/app.meta_title') }}</label>
                                <input type="text" class="form-control" name="meta_title_{{ $locale }}" placeholder="{{ trans('admin/app.meta_title') }}" value="{{ isset($object) ? $object->translate($locale)->meta_title: old('meta_title_'.$locale) }}">
                                <p class="help-block">{{ trans('admin/app.for_seo') }}</p>
                            </div>
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('admin/app.meta_description') }}</label>
                                <input type="text" class="form-control" name="meta_description_{{ $locale }}" placeholder="{{ trans('admin/app.meta_description') }}" value="{{ isset($object) ? $object->translate($locale)->meta_description: old('meta_description_'.$locale) }}">
                                <p class="help-block">{{ trans('admin/app.for_seo') }}</p>
                            </div>
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('validation.attributes.short_description') }} *</label>
                                <textarea class="form-control tinymce" name="short_description_{{ $locale }}" placeholder="{{ trans('validation.attributes.short_description') }}">{{ isset($object) ? $object->translate($locale)->short_description: old('short_description_'.$locale) }}</textarea>
                            </div>
                            <div class="form-group @if($k != 0)display-none @endif">
                                <label>{{ trans('validation.attributes.content') }} *</label>
                                <textarea class="form-control tinymce" name="content_{{ $locale }}" placeholder="{{ trans('validation.attributes.content') }}">{{ isset($object) ? $object->translate($locale)->content: old('content_'.$locale) }}</textarea>
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label>{{ trans('admin/app.link') }} *</label>
                                <input type="text" class="form-control slug" required name="slug" placeholder="some-friendly-url" value="{{ isset($object)? $object['slug']: old('slug') }}">
                            </div>
                            <!-- <div class="form-group">
                                <label>{{ trans('validation.attributes.date') }} *</label>
                                <input type="text" class="form-control datepicker" required name="created_at" placeholder="" value="{{ isset($object)? $object['created_at']: old('created_at') }}">
                            </div> -->
                            <label for="">{{ trans('admin/app.image') }}</label>
                            <div class="thumb-container">
                            @if(isset($images))
                                @foreach ($images as $image)
                                    <div class="thumb-gallery item-{{ $image['id'] }}" id="item-{{ $image['id'] }}">
                                        <img src="{{ url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$object->id.'/'.$image['filename']) }}?new" class="img-resonsive" style="width: 200px;" alt="{{ $image['title'] }}">
                                        @if(\Config::get('constants.settings.is_title_image_pages'))
                                        <input type="text" class="form-control noPlaceholder" name="image_title[]" value="{{ $image['title'] }}" data-placeholder="" placeholder="">
                                        @endif
                                        <input type="hidden" name="oldsorting[]" value="{{ $image['id'] }}">
                                        <div class="">
                                            <button type="button" data-for-remove="item-{{ $image['id'] }}" data-url= "{{ route('pages.deleteimage', $image['id']) }}" class="btn btn-outline btn-danger btn-xs ajax-image-delete">{{ trans('admin/app.delete') }}</button>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                                <div class="for-form-upload"></div>
                            </div>
                            <div class="form-group">
                                <label for=""></label>
                                <input type="file" name="photo[]" accept="image/*" @if(\Config::get('constants.settings.is_multiple_upload_pages'))multiple @endif class="upload">
                                <p class="help-block">{{ trans('admin/app.max_file_size_to_upload') }}{{ $max_file_to_upload }}</p>
                                <input type="hidden" name="sorting" value="">
                            </div>
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-default">{{ trans('admin/app.submit') }}</button>
                            <a href="{{ route('pages.list') }}" class="btn btn-default">{{ trans('admin/app.cancel') }}</a>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop

@section('js')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/speakingurl/7.0.0/speakingurl.min.js"></script>
<script src="{{asset('assets/admin/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
    var not_valid_image_format = '{{ trans('admin/app.this_is_must_be_an_image') }}';
    var write_a_title = '';
    var is_title_input = "{{ \Config::get('constants.settings.is_title_image_pages') }}";
    var tinyLang = @if(App::getLocale() == 'bg')'bg_BG' @else'' @endif;

    var slugify = function () {
        var slug = getSlug($('.title-for-slug').val());
        $('.slug').val(slug);
    };
</script>
@stop