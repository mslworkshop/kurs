@extends('admin.layout')

@section('css')

<!-- DataTables CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ asset('assets/admin/bower_components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">

@stop

@section('content')

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dd nestable" data-url="{{ route('pages.sorting') }}">
                    <ol class="dd-list">
                       @each('admin.pages.html_list', $list->viewList(), 'object')
                    </ol>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@stop

@section('js')
<script src="{{ asset('assets/admin/js/jquery-ui.min.js') }}"></script>
<!-- DataTables JavaScript -->
<script src="{{ asset('assets/admin/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.nestable.js') }}"></script>
@stop