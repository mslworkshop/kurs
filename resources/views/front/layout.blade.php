<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<title>@if(isset($metas['meta_title'])){{ $metas['meta_title'] }} | @endif{{ trans('app.site_name') }} </title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{{ $metas['meta_description'] or trans('app.description') }}">
<meta name="author" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW"><!-- Favicon -->
<meta name="google-site-verification" content="0vnzmZjGMG5e_LhCT-NS7ikU1t6bu94qYf5yTnwY060" />
<link rel="shortcut icon" href="favicon.ico">
<!-- Web Fonts -->

<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i,600,600i,700,700i|Yanone+Kaffeesatz:300,400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Exo+2:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=cyrillic" rel="stylesheet">
@if(!Request::is('/'))
<!-- canonical URL`s -->
<link rel="canonical" href="{{ \URL::current() }}" />
@endif

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="{{ asset('assets/front/plugins/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="{{ asset('assets/front/css/header-default.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/css/footer-v1.css') }}">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="{{ asset('assets/front/plugins/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/line-icons/line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/owl-carousel/owl-carousel/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/master-slider/masterslider/style/masterslider.css') }}">
<link rel='stylesheet' href="{{ asset('assets/front/plugins/master-slider/masterslider/skins/black-2/style.css') }}">

<!-- CSS Theme -->
<link rel="stylesheet" href="{{ asset('assets/front/css/dark-red.css') }}" id="style_color">
<link rel="stylesheet" href="{{ asset('assets/front/css/dark.css') }}">

@yield('css')

<!-- CSS Customization -->
<link rel="stylesheet" href="{{ asset('assets/front/css/custom.css') }}">
</head>

<body>
<div class="wrapper">
<!--=== Header ===-->
<div class="header">
<div class="container">
<!-- Logo -->
<a class="logo" href="{{ route('home') }}">
<img src="{{ asset('assets/front/img/logo1-default.png') }}" alt="KURS.bg"></a>
<!-- End Logo -->

<!-- Topbar -->
<div class="topbar">
<ul class="loginbar pull-right">
<li><a href="{{ url('/vaprosi') }}" title="{{ trans('app.faq') }}">{{ trans('app.faq') }} </a></li>
<li class="topbar-devider"></li>
<li><a href="{{ url('/mnenia-clienti') }}" title="{{ trans('app.testimonials') }}">{{ trans('app.testimonials') }}</a></li>
<li class="topbar-devider"></li>
<li><a href="{{ route('kontakti') }}" title="{{ trans('app.contacts') }}">{{ trans('app.contacts') }}</a></li>
</ul>
</div>
<!-- End Topbar -->

<!-- Toggle get grouped for better mobile display -->
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<!-- End Toggle -->
</div><!--/end container-->

@include('front/partials.menubar')

</div>
<!--=== End Header ===-->

    @yield('preContainer')

    @if(!Request::is('/'))
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">{{ $title or '' }}</h1>
            @if(isset($breadcrumbs))
            <ul class="pull-right breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li><a href="{{ route('home') }}">{{ trans('app.home') }}</a></li>
                @foreach($breadcrumbs as $crumb)
                {!! $crumb !!}
                @endforeach
            </ul>
            @endif
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->
    @endif

    <!--=== Content Part ===-->
    <div class="container content @yield('containerClass')">

        @yield('content')

    </div><!--/container-->
    <!-- End Content Part -->

    <!--=== Footer Version 1 ===-->
    <div class="footer-v1">
        <div class="footer">
            <div class="container">
                <div class="row">

                    <!-- Address -->
                    <div class="col-md-3 map-img md-margin-bottom-40">
                        <div class="headline"><h2>{{ trans('app.contacts') }}</h2></div>
                        {!! $contactsInfo->short_description !!}
                    </div><!--/col-md-3-->
                    <!-- End Address -->

                    <!-- Latest -->
                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="posts">
                            <div class="headline"><h2>{{ trans('app.useful_information') }}</h2></div>
                            @if(sizeof($usefulInfo) > 0)
                            <ul class="list-unstyled latest-list">
                                @foreach($usefulInfo as $obj)
                                <li>
                                    <a href="{{ url($obj->slug) }}" title="{{ $obj->title }}">{{ $obj->title }}</a>
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div><!--/col-md-3-->
                    <!-- End Latest -->
                    
                    <!-- Link List -->
                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline">
                          <h2>{{ trans('app.quick_links') }}</h2></div>
                        <ul class="list-unstyled link-list">
                            <li><a href="{{ url('/za-nas/misia') }}" title="{{ trans('app.about_us_title') }}">{{ trans('app.about_us_title') }}</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="{{ url('/vaprosi') }}" title="{{ trans('app.faq') }}">{{ trans('app.faq') }} </a><i class="fa fa-angle-right"></i></li>
                            <li><a href="{{ url('/mnenia-clienti') }}" title="{{ trans('app.opinion_from_clients') }}">{{ trans('app.opinion_from_clients') }}</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="{{ route('kontakti') }}" title="{{ trans('app.contacts_with_us') }}">{{ trans('app.contacts_with_us') }}</a><i class="fa fa-angle-right"></i></li>
                        </ul>
                    </div><!--/col-md-3-->
                    <!-- End Link List -->


                    <!-- About -->
                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>{{ trans('admin/app.newsletter') }}</h2></div>
                        <p>{{ trans('app.subscribe_text') }}</p>
                        <p id="emailfield">
                            @if($errors->first('email')){{ $errors->first('email') }}@endif
                            @if(session()->has('email_ok')){{ session('email_ok') }}@endif
                        </p>
                        <form class="footer-subsribe" action="{{ route('email.it') }}#emailfield" method="POST">
                            <div class="input-group">
                                {!! csrf_field() !!}
                                <input type="email" name="email" required class="form-control" placeholder="{{ trans('admin/user.email') }}">
                                <span class="input-group-btn">
                                    <button class="btn-u" type="submit">{{ trans('app.subscription') }}</button>
                                </span>
                            </div>
                        </form>
                    </div><!--/col-md-3-->
                    <!-- End About -->

                </div>
            </div>
        </div><!--/footer-->

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            {{ trans('app.web_design') }}: <a href="http://www.noavis.com/" target="_blank" rel="nofollow" >noavis.com</a> {{ trans('app.copyright') }}
                            <a href="{{ url('/uslovia-za-polzvane') }}" title="{{ trans('app.terms') }}">{{ trans('app.terms') }}</a> | <a href="{{ url('/sitemap') }}">{{ trans('app.sitemap') }}</a>
                        </p>
                    </div>

                    <!-- Social Links -->
                    <div class="col-md-6">
                        <ul class="footer-socials list-inline">
                            <li>
                                <a href="https://www.facebook.com/kursbg" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/+KursBgbg" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
<li>
                                <a href="https://www.instagram.com/kursbg/" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>

                            <!-- <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li> -->

                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div>
        </div><!--/copyright-->
    </div>
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="{{ asset('assets/front/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/jquery/jquery-migrate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- JS Implementing Plugins -->
<script type="text/javascript" src="{{ asset('assets/front/plugins/back-to-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/smoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/jquery.parallax.js') }}"></script>
<script src="{{ asset('assets/front/plugins/master-slider/masterslider/masterslider.min.js') }}"></script>
<script src="{{ asset('assets/front/plugins/master-slider/masterslider/jquery.easing.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/counter/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/counter/jquery.counterup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/fancybox/source/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/owl-carousel/owl-carousel/owl.carousel.js') }}"></script>

<!-- JS Page Level -->
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/plugins/fancy-box.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/plugins/owl-carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/plugins/master-slider-fw.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/plugins/style-switcher.js') }}"></script>

@yield('js')

<!--[if lt IE 9]>
    <script src="{{ asset('assets/front/plugins/respond.js') }}"></script>
    <script src="{{ asset('assets/front/plugins/html5shiv.js') }}"></script>
    <script src="{{ asset('assets/front/plugins/placeholder-IE-fixes.js') }}"></script>
<![endif]-->

<!-- JS Customization -->
<script type="text/javascript" src="{{ asset('assets/front/js/custom.js') }}"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55863405-1', 'auto');
  ga('send', 'pageview');

</script>





</body>
</html>
