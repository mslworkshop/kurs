@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            @if(!empty($objects))
            @foreach($objects as $object)
            <blockquote>
                <h2>{{ $object->title }}</h2>
                {!! $object->content !!}
            </blockquote>
            @endforeach
            <div class="text-right">
                {!! $objects->render() !!}
            </div>
            @endif
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop