﻿@extends('front.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/front/plugins/sky-forms-pro/skyforms/css/sky-forms.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/css/pages/page_contact.css') }}">
@stop

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

<div class="col-md-9 mb-margin-bottom-30">
        <!-- Google Map -->
        <!-- <div style="width: 100%; height: 450px;-webkit-filter: grayscale(100%);filter: grayscale(100%);   id="map_canvas"> -->

 <div class="row margin-bottom-30" style="background-color:#f4f4f4; border: 1px solid #e8e8e8;">
        <div class="col-md-3 mb-margin-bottom-10">
        <ul class="list-unstyled who margin-bottom-10" style="margin-top:10px;">
<li><strong>Офис София - Студентски град</strong></li>
<li>ул. Акад. Борис Стефанов №35, South Mall</li>
<li>моб.: <a title="+359 894 348 521" href="tel:+359894348521" rel="nofollow">+359 894 348 521</a></li>
<li>имейл:<a href="mailto: office@kurs.bg"> office@kurs.bg</a></li>
<li><a href="http://www.kurs.bg">http://www.kurs.bg</a></li>
</ul>
        </div>
        <div class="col-md-9 mb-margin-bottom-10" style="margin-top:10px;">

        <div style="width: 100%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d733.6909217213106!2d23.341811788091665!3d42.645169485159144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa841980c42817%3A0xd28d1fbe83ec844!2z0YPQuy4g4oCe0LDQutCw0LQuINCR0L7RgNC40YEg0KHRgtC10YTQsNC90L7QsiIgMzUsIDE3MDAg0KHQvtGE0LjRjw!5e0!3m2!1sbg!2sbg!4v1471880653423" width="100%" height="200" frameborder="0" style="border:0;pointer-events: none;" allowfullscreen></iframe></div>
            </div>
        </div>

        <div class="row margin-bottom-30" style="background-color:#f4f4f4; border: 1px solid #e8e8e8;">
        <div class="col-md-3 mb-margin-bottom-10" >
        <ul class="list-unstyled who margin-bottom-10" style="margin-top:10px;">
<li><strong>Офис София - Орлов мост</strong></li>
<li>бул. Цариградско шосе 15, вх. В, ет.1</li>
<li>моб.: <a title="+359 894 348 521" href="tel:+359894348521" rel="nofollow">+359 894 348 521</a></li>
<li>имейл:<a href="mailto: office@kurs.bg"> office@kurs.bg</a></li>
<li><a href="http://www.kurs.bg">http://www.kurs.bg</a></li>
</ul>
        </div>
        <div class="col-md-9 mb-margin-bottom-10" style="margin-top:10px;">
        <div style="width: 100%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1466.3362547718705!2d23.339747!3d42.689362!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa859f1a116079%3A0x8c68782a617a1446!2z0LHRg9C7LiDigJ7QptCw0YDQuNCz0YDQsNC00YHQutC-INGI0L7RgdC14oCcIDE1LdCSLCAxMTI0INCh0L7RhNC40Y8!5e0!3m2!1sbg!2sbg!4v1457634496802" width="100%" height="200" frameborder="0" style="border:0;pointer-events: none;" allowfullscreen></iframe>
        </div>
        </div>
        </div>


<div class="row margin-bottom-30" style="background-color:#f4f4f4; border: 1px solid #e8e8e8;">
        <div class="col-md-3 mb-margin-bottom-10">
        <ul class="list-unstyled who margin-bottom-10" style="margin-top:10px;">
<li><strong>Офис София - Център</strong></li>
<li>ул. Георги С. Раковски 86</li>
<li>моб.: <a title="+359 894 348 521" href="tel:+359894348521" rel="nofollow">+359 894 348 521</a></li>
<li>имейл:<a href="mailto: office@kurs.bg"> office@kurs.bg</a></li>
<li><a href="http://www.kurs.bg">http://www.kurs.bg</a></li>
</ul>
        </div>
        <div class="col-md-9 mb-margin-bottom-10" style="margin-top:10px;">

        <div style="width: 100%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.286490110847!2d23.32780131500952!3d42.69765422170797!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa857014f2ecff%3A0x51a7cd3d72a16b59!2sulitsa+%E2%80%9EGeorgi+S.+Rakovski%E2%80%9C+86%2C+1000+Sofia!5e0!3m2!1sen!2sbg!4v1491402446118" width="100%" height="200" frameborder="0" style="border:0;pointer-events: none;" allowfullscreen></iframe></div>
            </div>
        </div>

        <!-- End Google Map -->
<div class="row margin-bottom-30">
        <p>&nbsp;</p>
        @if(!session()->has('successfuly_sended_request'))
        <p>{{ trans('app.feedback_text') }}</p>
        <br />
        @endif
        @include('errors.admin.errors')
        <form action="{{ route('kontakti') }}" method="post" id="sky-form3" class="sky-form @if(session()->has('successfuly_sended_request'))submited @endif">
            {!! csrf_field() !!}
            @if(session()->has('successfuly_sended_request'))
            <div class="message" style="display:block;">
                <i class="rounded-x fa fa-check"></i>
                <p>{{ trans('app.your_messages_is_successfuly_you_will_connected_with_us_soon') }}</p>
            </div>
            @else
            <fieldset style="border: 1px solid #e8e8e8;">
                <label>{{ trans('app.name') }}: <span class="color-red">*</span></label>
                <div class="row sky-space-20">
                    <div class="col-md-7 col-md-offset-0">
                        <div>
                            <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                        </div>
                    </div>
                </div>

                <label>{{ trans('app.email') }}: <span class="color-red">*</span></label>
                <div class="row sky-space-20">
                    <div class="col-md-7 col-md-offset-0">
                        <div>
                            <input type="text" name="email" id="email" value="{{ old('email') }}" class="form-control">
                        </div>
                    </div>
                </div>

                <label>{{ trans('app.phone') }}: <span class="color-red">*</span></label>
                <div class="row sky-space-20">
                    <div class="col-md-7 col-md-offset-0">
                        <div>
                            <input type="text" name="phone" value="{{ old('phone') }}" id="phone" placeholder="0894348521" class="form-control">
                        </div>
                    </div>
                </div>
                <label>{{ trans('app.message') }}: <span class="color-red">*</span></label>
                <div class="row sky-space-20">
                    <div class="col-md-11 col-md-offset-0">
                        <div>
                            <textarea rows="8" name="message" id="message" class="form-control">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>

                <p><button type="submit" class="btn-u">{{ trans('app.send') }}</button></p>
            </fieldset>
            @endif
        </form>
        </div>
    </div><!--/col-md-9-->

    <!--/col-md-3-->
</div><!--/row-->

@stop

@section('js')
<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1466.3362547718705!2d23.338565743573554!3d42.68947899935762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa859f1a116079%3A0x8c68782a617a1446!2z0LHRg9C7LiDigJ7QptCw0YDQuNCz0YDQsNC00YHQutC-INGI0L7RgdC14oCcIDE1LdCSLCAxMTI0INCh0L7RhNC40Y8!5e0!3m2!1sbg!2sbg!4v1457634496802" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe> -->
<!-- <script src="http://maps.googleapis.com/maps/api/js?v=3&amp;libraries=geometry&amp;sensor=true" type="text/javascript"></script>
<script type="text/javascript">
function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    //map.setTilt(45);

    // Multiple Markers
    var markers = [
        ['{{ trans("app.site_name") }}', 42.689362, 23.339747],
        ['{{ trans("app.site_name") }}', 42.645215, 23.342294]
    ];

    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>{{ trans("app.site_name") }}</h3>' +
        '<p>София 1400, бул. Цариградско шосе 15, вх. В, ет.1 - партер Тел: 02/862 62 94 Имейл: office@kurs.bg http://www.kurs.bg' + '</div>'],
        ['<div class="info_content">' +
        '<h3>{{ trans("app.site_name") }}</h3>' +
        '<p>София,Студентски град,  ул. Акад. Борис Стефанов №35, South Mall</p>' +
        '</div>']
    ];

    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Loop through our array of markers & place each one on the map
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        // Allow each marker to have an info window
       /* google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));*/

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(13);
        google.maps.event.removeListener(boundsListener);
    });
}
google.maps.event.addDomListener(window, "load", initialize);
</script> -->
@stop