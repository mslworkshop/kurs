﻿@extends('front.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/front/plugins/sky-forms-pro/skyforms/css/sky-forms.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/css/pages/page_contact.css') }}">
@stop

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')
    <style type="text/css">
        .sky-form fieldset{
            background: #fff;
        }
    </style>
    <div class="col-md-9">
         <form action="{{ route('zayavka-za-kurs') }}" method="post" class="sky-form @if(session()->has('successfuly_sended_request'))submited @endif">
            {!! csrf_field() !!}
            @if(session()->has('successfuly_sended_request'))
            <div class="message" style="display:block;">
                <i class="rounded-x fa fa-check"></i>
                <p>{{ trans('app.your_messages_is_successfuly_you_will_connected_with_us_soon') }}</p>
            </div>
            @else
            <header>{{ trans('app.please_fill_in_the_details_below_to_request') }} </header>
            @include('errors.admin.errors')
            <fieldset>
                <section>
                    <label class="label"><strong>{{ trans('app.name') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="name" value="{{ old('name') }}"></label>
                </section>
                <section>
                    <label class="label"><strong>{{ trans('app.email') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="email" value="{{ old('email') }}"></label>
                </section>
                <section>
                    <label class="label"><strong>{{ trans('app.phone') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="phone" value="{{ old('phone') }}" placeholder="0894348521"></label>
                </section>
            </fieldset>

            <fieldset>

                <section>
                    <label class="label"><strong>{{ trans('app.language') }}:</strong> <span class="color-red">*</span></label>
                    @foreach(\Config::get('constants.language') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="language[]" value="{{ $k }}" @if(old('language') && in_array($k, old('level'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                    <!-- <select class="form-control" name="form_of_education"> -->
                        <!-- <option value="{{ $k }}" @if($k == old('form_of_education')) selected="selected" @endif>{{ $obj }}</option> -->
                        <!-- <option value="">{{ trans('app.select') }}:</option> -->
                    <!-- </select> -->
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.level') }}:</strong> <span class="color-red">*</span></label>
                    @foreach(\Config::get('constants.level') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="level[]" value="{{ $k }}" @if(old('level') && in_array($k, old('level'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                    <!-- <select class="form-control" name="form_of_education"> -->
                        <!-- <option value="{{ $k }}" @if($k == old('form_of_education')) selected="selected" @endif>{{ $obj }}</option> -->
                        <!-- <option value="">{{ trans('app.select') }}:</option> -->
                    <!-- </select> -->
                </section>






                <section>
                    <label class="label"><strong>{{ trans('app.format_of_education') }}:</strong> <span class="color-red">*</span></label>
                    @foreach(\Config::get('constants.form_of_education_exam') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="form_of_education[]" value="{{ $k }}" @if(old('form_of_education') && in_array($k, old('form_of_education'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                    <!-- <select class="form-control" name="form_of_education"> -->
                        <!-- <option value="{{ $k }}" @if($k == old('form_of_education')) selected="selected" @endif>{{ $obj }}</option> -->
                        <!-- <option value="">{{ trans('app.select') }}:</option> -->
                    <!-- </select> -->
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.course') }}:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="course">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.course') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('course')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    <div class="row">
                        <div class="col-md-12">
                            @foreach(\Config::get('constants.course') as $k => $obj)
                            <!-- <div class="@if($k == 1) col-md-12 @else col-md-6 @endif"> -->
                                <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox1" name="course[]" value="{{ $k }}" @if(old('course') && in_array($k, old('course'))) checked="checked" @endif>{{ $obj }}</label>
                            <!-- </div> -->
                            @endforeach
                        </div>
                    </div>
                </section>

                <section>
                    <label class="label"><strong>Дни на провеждане:</strong> <span class="color-red">*</span></label>
                    @foreach(\Config::get('constants.days_other') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="days[]" value="{{ $k }}" @if(old('days') && in_array($k, old('days'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                    <!-- <select class="form-control" name="days">
                        <option value="">{{ trans('app.select') }}:</option>
                        <option value="{{ $k }}" @if($k == old('days')) selected="selected" @endif>{{ $obj }}</option>
                    </select> -->
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.timing') }}:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="timing">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.timing_exam') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('timing')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    @foreach(\Config::get('constants.timing_exam') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="timing[]" value="{{ $k }}" @if(old('timing') && in_array($k, old('timing'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                </section>

                <!-- <section>
                    <label class="label"><strong>{{ trans('app.form_of_education') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="form_of_education">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.form_of_education') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('form_of_education')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section> -->

                <section>
                    <label class="label"><strong>{{ trans('app.form_of_education')}}:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="form_of_education">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.educ_form_list') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('form_of_education')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    <!-- !!!form_of_education -->
                    @foreach(\Config::get('constants.educ_form_list') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="educ_form[]" value="{{ $k }}" @if(old('educ_form') && in_array($k, old('educ_form'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                </section>

                <section>
                    <label class="label"><strong>Предпочитан офис:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="office">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.educ_office') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('office')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    @foreach(\Config::get('constants.educ_office') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="office[]" value="{{ $k }}" @if(old('office') && in_array($k, old('office'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                </section>

                <section>
                    <label class="label"><strong>Допълнителна информация:</strong></label>
                    <textarea name="details" class="form-control" maxlength="12500" rows="5"></textarea>
                </section>

            </fieldset>
            <fieldset>
                <section>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox1" name="check_privacy" @if(old('check_privacy') == 1) checked="checked" @endif value="1">{{ trans('app.i_agree_with') }} <a href="{{ route('privacy') }}" target="_blank"><strong class="text-danger">{{ trans('app.privacy') }}</strong></a>.
                    </label>
                </section>
            </fieldset>
            <footer>
                <button type="submit" class="btn-u">{{ trans('app.send') }}</button>
            </footer>
            @endif
        </form>
    </div><!--/col-md-9-->

    <!--/col-md-3-->
</div><!--/row-->

@stop

        <!-- <form action="{{ route('zayavka-za-kurs') }}" method="post" class="sky-form @if(session()->has('successfuly_sended_request'))submited @endif">
            {!! csrf_field() !!}
            @if(session()->has('successfuly_sended_request'))
            <div class="message" style="display:block;">
                <i class="rounded-x fa fa-check"></i>
                <p>{{ trans('app.your_messages_is_successfuly_you_will_connected_with_us_soon') }}</p>
            </div>
            @else
            <header>{{ trans('app.please_fill_in_the_details_below_to_request') }} </header>
            @include('errors.admin.errors')
            <fieldset>
                <section>
                    <label class="label"><strong>{{ trans('app.name') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="name" value="{{ old('name') }}"></label>
                </section>
                <section>
                    <label class="label"><strong>{{ trans('app.email') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="email" value="{{ old('email') }}"></label>
                </section>
                <section>
                    <label class="label"><strong>{{ trans('app.phone') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="phone" value="{{ old('phone') }}" placeholder="0894348521"></label>
                </section>
            </fieldset>

            <fieldset>
                <section>
                    <label class="label"><strong>{{ trans('app.language') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="language">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.language') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('language')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.level') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="level">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.level') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('level')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.course') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="course">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.course') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('course')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.days') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="days">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.days') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('days')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.timing') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="timing">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.timing') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('timing')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.form_of_education') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="form_of_education">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.form_of_education') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('form_of_education')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section>

            </fieldset>
            <fieldset>
                <section>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox1" name="check_privacy" @if(old('check_privacy') == 1) checked="checked" @endif value="1">{{ trans('app.i_agree_with') }} <a href="{{ route('privacy') }}" target="_blank"><strong class="text-danger">{{ trans('app.privacy') }}</strong></a>.
                    </label>
                </section>
            </fieldset>
            <footer>
                <button type="submit" class="btn-u">{{ trans('app.send') }}</button>
            </footer>
            @endif
        </form>-->