@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            <div class="headline"><h2>{{ trans('app.faq_for_language_learning') }}</h2></div>
            @if(!empty($objects))
            <div class="panel-group acc-v1 margin-bottom-40" id="accordion">
                @foreach($objects as $k => $object)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle @if($k != 0)collapsed @endif" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $object->id }}">
                                {{ $object->title }}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{ $object->id }}" class="panel-collapse @if($k != 0)collapse @else collapse in @endif">
                        <div class="panel-body">{!! $object->content !!}</div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop