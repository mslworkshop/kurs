@extends('front.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/front/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css') }}">
@stop

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            <div id="grid-container" class="cbp-l-grid-fullWidth">
                @foreach($images as $k => $image)
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <img src="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SIMPLE_IMAGES_DIR').$image->object_id.'/'.$image->filename, 186, 121) }}" alt="language {{ $k }}">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <ul class="link-captions">
                                        <li><a href="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SIMPLE_IMAGES_DIR').$image->object_id.'/'.$image->filename, 1000, 650) }}" class="cbp-lightbox" data-title=""><i class="rounded-x fa fa-search"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="text-right">
                {!! $images->render() !!}
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/front/js/plugins/cube-portfolio/cube-portfolio-5-fw.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop