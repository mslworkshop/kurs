@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            <!-- <div class="headline"><h2>{{ $obj->title }}</h2></div> -->
            @if(isset($obj->images[0]))
            <div class="thumbnail-img imgleft">
                <div class="overflow-hidden">
                    <img class="img-responsive" src="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$obj->id.'/'.$obj->images[0]->filename, 350, 220) }}" alt="{{ htmlspecialchars($obj->title) }}">
                </div>
            </div>
            @endif
            {!! $obj->content !!}
            @if(isset($more))
                <div class="headline"><h2>Прочетете още</h2></div>
                <ul class="list-unstyled-black">
                    @foreach($more as $page)
                        <li><a class="color-red" href="{{ url($page->slug) }}" title="{{ $page->title }}">{{ $page->title }}</a></li>
                    @endforeach
                </ul>
            @endif
            </div>
        </div>
    </div>
</div>

@stop
