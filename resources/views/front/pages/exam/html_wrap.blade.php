﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="bg"> <!--<![endif]-->
<head>
    <title>KURS.bg - Резултати от онлайн изпит</title>
    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- custom css -->
    <style>
        * { font-family: DejaVu Sans, sans-serif;}
        body {
            padding: 0; margin: 0; color: #005670!important; font-size: 14px!important;
        }
        p {margin: 0;}
        a{
            text-decoration: none!important;
        }
        table{
            width: 100%!important;
            border-collapse: collapse;
            border: 2px solid #003240;
        }
        tr, td{
            border: 1px solid #003240;
        }
        td{
            padding: 6px 8px;
        }
        h2{
            font-size: 22px!important;
            font-weight: normal;
            color: #299B47!important;
        }
        .red{
            color: #B24B51!important;
        }
        .green{
            color: #299B47!important;
        }
        .bold{
            font-weight: bold;
        }
        .center{
            text-align: center;
        }
        .logo-table, .logo-table tr, .logo-table td{
            border:none;
        }
        /*.pdf-title{
            font-size: 26px!important;
            font-weight: bold;
            text-align: center;
            padding-top: 20px;
            padding-left: 15px;
            color: #005670!important;
        }*/
        .new-page{
            page-break-before: always;
        }
        .titles{
            font-weight: normal;
            margin-top: 5px;
            margin-bottom: 0;
	    color: #005670!important;
        }
        .footer{
            position: fixed;
            bottom: 0px;
            width: 100%;
            font-size: 12px!important;
            font-weight: normal!important;
        }
    </style>
</head>

<body>
    <img style='width:190px;' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAABMCAYAAAB087t0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAABd3SURBVHic7Z15mBxVtcB/d7InbCExDwhLIAJakbDJKioIQrGGwOOVgohA4QKPD4pNfSKyRJAoFsQoPimQaCAUO0pIBY1oUBAhQAIUGMxjlUVMggkxyUwm9/1xqmequ6u7qmd6mUzq9339pbv61q07nTp1zz3bVVpr+grKci4AzgWu0747vdXjyclpNKrVAqgsZxQwGTgF2BcYDqwCngBmAvdr313euhHm5DSOlgmgspx9gDOAk4BRVZq+B9wB3Kp99+lmjC0np1k0VQCV5QwHjgds4JAedPFbwENmxbX1HFtOTitoigAqy9kFOA1RM3eoQ5evIOrpDO27S+rQX05OS2iYACrLUcCRwJnAscCgBlxmDfBrZFZ8WPtuAy6Rk9M46i6AynK2Ak5G1ncT6tp5dRYBtwCztO/+o4nXzcnpMXUTQGU5ByJruxOAzevSac9YDtwF3KJ994kWjiMnJ5VeCaCynM2AExE18xP1GlQdmY+op/dq313V6sHk5JTSIwFUljMBOB34PLBNvQfVAN4AZiGujBdbPZicnAKZBVBZziDgaETNPBJoa+C4GkUn8CAyK87RvtvZ4vHkbOSkCqCynO2AUxE3wi7NGFSTeBG4FbhN++7fWzyWnI2UigKoLOcQZLabBIxo5qCazErgXsRoM7/Vg8nZuCgSQGU5WyKhYWcC+7RqUC3kz8DNwN3ad99v9WBy+j+K/zp/BKJang58DvhQa4fUJ3gXMdr8AgjzsLecRtEGfBX4C5IGlAuf8B/A+cDT9E33Sk4/oU377nWIL29FqwfTx1gGHK9993etHkhO/6VrDRj59u4APtbSEfUNngUs7buLWz2QnP5Nly9P++4LiLp1T+uG0ye4AzgoF76cZpDohlCW8wowrumjaT2Xat/9bqsHkbPxMLDC8TVNHUXreR84Xfvu/a0eSM7GRVk4mbKcttjxVUgcZX/meeDAXPhyWkFaPOda4JOIT6w/cjdwQB6gndMq0gRwGLBM++7JwHeaMJ5mcoX23ZO0737Q6oHkbLykCaBGygSiffdKpHzghh6itRI4Sfvu5a0eSE5OTSlF0TrpAOCZxgyn4YTIeu/uVg8kJwd6kNOnffclxF94ex2ur4GlwBJgAfBH4E/RawHwMvBPJI+vt9yPCN/zdegrJ6cuVHJDVEX77mrgFGU5C4FLgU0znvoa8CRS9XpR9PltYFVScmxkkR0BbAVsD+yGZGnsC3w44zU7gGu07/a3NWxOP6BHAlhA++7UwZO/sn/H4OGTqzR7Ccm3mw0sqCWzQPvuemTNthKZDecBKMsZAOyJZOifiAhmJdYBN2S9Zk5OM+mVACrLGcfg4Z+t8PVs4EfAbyJBqhvRbPkU8JSynCuAw4GvIVW3SxmGCOlN9RxDTk496JUAIrU/Nyk59iAwpVASUFnOQKCuAhgnKsb7MPCwspw9gKuRmjVxPkMugDl9kDIjTDRbFdZjGvh30onKcgCORwPr2lCKJcBk7bvHat99QlnOOcpy7gWGNmboRWNpU5YzGViiffcowELWlgV2iyp15+T0KcpmQGU5O9JdWHcgsDfw+7Iz2/ROrGsz0IrRu7zXOebD/zjxhTODhcpyjgK+h6zLzs/q6FaWMwb4NDAWyU2co3337epnCdp310fFo95UlnOh9l1PWc48pOjSMVGfI5EcvyIMzzwYcEiepduA+aEdXJdlHIZnDgEuASYipfiz1nxUUdu/AleGdpD40Eu43qFI3Z7hZNcyCtdaBaxGfpMlwAvAwtAOehyYYHjmMMRItiewM/KbjyDb7zAAuC20A78n1440rSuRauwa+T1u0r47pyf9NYskFdSiu9bnEOBskgRw5ZCJbZu0D9hmjzcYOXb564tO/e1CNfniPRnM7KjFCqSkQyrKci4E/gfYMjr0dWqfOW8BpgA3Kcs5HjhF++6xynKuB85Dsv3LBBCxph6X0neqABqeOQhRvw+radTl/AHIetNMAfbv5fXivGV45v3A9aEdvJz1JMMzd0UqK0ymd5vvvAD0SACBMcA3S46tIftv2RKS/ICjSz4n7t03bMyqnbfb+zVGbrOcdWsHLgRg8LqDY00ez7KxprKcrwM/QIRvPXC49t2p2ndfyTD+LqKZdl708WjgnOj4+cC1lf4OoD2l60yzEXAnvRc+KF9TV2NdHa4XZxvkgfuM4ZnnpDU2PHOg4ZnfAxYiJTx6u/NVb7JwFDKrx1ndi/6aQpIAlla6LhVIAHY4cMl2m4z+gHXtAwEKyatGrMlTaRdXlrMDcFXs0DvAb0rabK0sJ2v17b/E3ndtDKN99xuI/7EhGJ7pkWyB7Qm1GKwatbfcCGC64ZnXVmpgeOaWwCOItjKkQePo9yQJ4NYln0cryxlW2kgpPXp9Z9fphcK228aa/F+G6x9N8bZlW1LuYJ8M/GeGvkqvOTb+hfbdjox91ER0k57ZiL77AJcYnnle6cForfsQcFDzh9S/KBLAyFJYWhlti+hVjO7eAUkp/a/obXzdlmVf9+1KPg8FblOWs5uynBHKcg5F1jmJs3ACcRWk4cWEDc/8OmJ06c983/DM0ofiNcB+rRhMf6N0BhxO+VppGMnlCuOqUpKJP4vZ/62EY/siRZEWI1tSjwRezdBXKQ3d+tfwTBux9vZ3BiFqJgCGZ45DSlg2guEN6rfPUmoFTZrtFMkzUFcZQ63VyOhtfAbKMms9BLiICTpOG91r0Q7E0Z6FzWLvV2Y8p2YMz8waWbMAcYVoulO7zgZ2bNTYSrgZUctLH4YaGIxYUI/I0M8JhmdeGNrBCqR4c5YAjk5kTf4C4pNdTfXg/4HARlcCsvSH/BDJ5v/SdSFAfBfawtovXr4iNVha++4SZTlTqJ7s+23tu2+m9RWxc+x9QzZcifyGWSoEhMARoR0sLTn/MJongD8O7aBq6pjhmZ8CZlK+HIizJeLfm0e2dd+jwFdDOwizDnRjpVQAt6rQbvuEY0ti73eN/n0udizT3hLady9XlqOBiylet60Evqt9t6IlLoH4uuS5iq16iOGZewEPkL7f/WuAWSp8EYPrPa4qlK/dSwjtYL7hmccg+2KUGdtiGIgAplmkO4Aza/EjNgtlOTshPt/9EZeJQsqu/A3ZzHW29t1/1tDfR5Cwx48jVQQHIEuz1xAvQBCV+yy0/xCwR3TddmBRqQAOR1SHUpUwycwcz6vbvQOlWHPx7xnaZWzcV1nOVtp330n7Q7TvXqEs5+dIzOZoZHb9XQ0zX2FjmXgZ+UeznpvWNYDhmeMRlXmz6s35BzLzbTDFrEI7WGR45j3AF6o027bKd3HWIO6kvoCCrp2cr0as1Uka3qeQuOalynJuRGKZK2btRII8BdnIKEkdPwBR1acqy7kP+Kb23ZeBadHxAgtKT/4V8FHgI8jTsBN5OiQVqV2IrAM3A7bd6xeH7a0fmPqUspyXEVVweHSx6yv9IXG0776OrJd6ygl0C8fryL4O9eB9wzM3A+Yie0ZUYyVwVGgHf63TtZvJn6kugIXfNmlWj7MpEv7XFwp5rVCWMxrxV2ap+D4KyW81leWcoH237CGqLOdI4DbEOJhGG5KJc6iynIMR7SDOuCIB1L67Dsm7S1UfQjtYbnjmY4AJ0Nk+wEKm3VnAZVGzc5XlTI/6bRhR4u6FsUN3ad9Ni3DJyljgPmB8Sru1wLGhHSyo03WbTdI6P07BqLWQ9IifGw3P7AjtoNWlP0Yi/3e1brfwcWCespz9te92hS9Ge2b+mnINMY0tkIrz75Ycb+/tNtN3xd6f/Im5+w1mzaAb6Q4B2oliwWgU5yCzNkh41o117PsYRDWuxvvA5NAO/lDH6zaNKIi62uwH3Wv+BzJ0uTlwl+GZjxieaRuemVV9rTcWxUajxYgqejwS+H8y8DPgX+WnsjMxjSyaSe8gWfjmAF+OrrUfEmByOcXLtPHAgaUn9jYf8D4kUHkLYJvlb448TT8w9SZlOdMRowrAFcpy5mjfXdTLayWiLGdXiv1xP9O+u6RS+waxAlFRNzgMzxyFWEHT4jj/CBDawaOGZ/4BuYHTODh6rTI8cz4yewShHdQU59sL4raLy4BrEzSjWcpyrkYE8fCS745VlnOc9t1fIQI1puT7VcCp2nfvS7j2Q8pyrgK+AVTc7qBXM2BoB8uBn3cd0HxnT/+Q4Xww+ApkHQbyI9ynLCdt/VQzynJGIsWWCg7cdxEdvtlsj/gz+zSGZw4yPHMfwzP3MzzTNDxzCqJSmimnPhnawQuxz2dRW3nKEYi18CfAi4ZnzjE884SaBt87ztO+e1WlZYn23dei8SVtUX6+spxRyAa2Rach5S2ThK/Q73rtu1cjs24ivVVBQTIZCjlkY9euHHK1nn3tKuDUWJudgN/WEFSdSpQ/OJdu1RPgS1kyMBrEuZGboi8zDXGO/xlRm75FScxsBa6Jf4hcDJ8FMlupYwxBBP4ewzMfNTxzjx70UQtztO9OS2sUJaJ/kfLsl48jGlZplM6sGnINL0dcE2X0WgBDO3iL4in2PMMzj9S+O58oJSjiY8DjkTWoVyjLOQB4jGJf40Xad4Pe9t0LFPKE78tMSG9Sxq9DOyh7yod28BRyc2bK+azAQcBjhmdmicbpKVOyNoxmwpklhzdFkp5L+XEN/XYgltMy6jEDAnyfYrP/bcbN5q7ad3+CJNoW2B54RFmOqywnzepWhrKcMcpyrkV8fHGr5FXRTr+tZj/DM7/c6kFUoda97l8GTqv0ZWgH74Z2cBoSv3sz6S6KJIYhs2HWMpO1sASZ7WshS73bt5Aww1pIUm/rI4ChHXQCp9A9fY9EM3fCLUfspH33GsQBGte/zwdCZTnTlOUcpCynYva7spwhynIOUJbzQyS86xK6LVGdwDnady+rdH4L+J7hmUnB6xsaC4HDonV+VUI7eDK0AxuJiDoZsY7XIowjKFFz68QzPajIt4hkq2icJbWU14x4lYRcz95aQbsI7eAlwzO/gNQABdhBr1e/n3DzEZO0P/cWddIFC2nTN9KtNm6BRNWfC7yhLOd54BW6y0ZsiVjmdiM5FO5ZRPgeq9ffkEI78pC5GHniV2Iksi6uOHNsAPwvcFGt9WGi0LtZwCzDM0ci5nibbBbT4wzPHBvaQT1jeHtibV2OzHCbp7SplZWIe64oTa5eKigA0Vrha7FD22mtHjU88zR91w8XaN/dFxG40mTd7RAr1NmIFfPS6P3RlAvfq0gRpX2bKHzrAStyLH8jQ/svGp6Z5aZLopY0qlpTrtJSxBYBu4V28NXeFGcCsZCHdjAztIODkVmxtFxEKYXsjHqyIr1JMVGZy0ZsUFvIiCmirgIIENrBT5GnXuFiI4BbDc+8c+IvDxuvfXe69t3xaPU5xI/4ngxPQccAaI+91nfdL8sQH9KpwMe0717fqAz3BFYDnw/t4P7o73uEbGFWPzY8s9aICaitJEWt/39pArsNDaijEtrBLKr4wmLUO0ukpxpe2oOq2uxYiaEkxFTXTQWNE9rBzYZnvgnMoDt+8qR1awceY3imN2hox3R95zwf8JVqH8Cxl+41cpf3zhqyyZqz1nUMaEejBw5aP6R99aCZy14Z9VPa9IvxkKAm81BoB3eWHLsQmZ2rBWZPAC5CCkLFSfvPzeQvNTxTke5CKL1WQPUwstHAvYZn7h3aQb3DBxONECXUUpAqCzVH4CjLGUJ6Lut4ZTmDawx3HENCFk1DBBAgtIO5hmfuA0ynu+zfMODcjjWDzjE88xHgwYkzO+YtPGXek/s/eEDnqqWb2KhokBqGbrpmxtKpM/7UqDFmpEwdCe3gbcMzLwd+mHLuZYZn3hHaQdwHlFZl7XhkDZbG/kgKTDVKb5BpSHhWtVSxiYhB5OIqbXpCFitnmfFDWU5SXVEF/Fv7btqMPjHj2OLsQOW0vALbArtTW6GvRB9x3VXQOKEdvBHawSTg80hmdPy6hwJux+pBiwzPfHPFO5vP6OwYoDvbB6jO9gGqs2MAq5aNyJyb1UAqqZE3kJ5zOBy56eOkGQZMwzNPrtYgit28geqz6TqKE6QJ7aAD+BLppRgvMjzzkJQ2mYn6ujJD0y7HvrKcbZXlPE13ckDp6/koH68ae0SpQ7VwOOUTU1J1hVoLcSVWzWvYDBgntIM7DM+8G7EifgXJl4ozlnJ1ai19eDfe0A7WR7Uz01Sr4wzPnBTaQSGI+Y8UBygk8cso/9AL7aCrOrjhmQOR3LVrESd4NRbTHQ4YH3doeOa3KVeNS7nT8MxbEWGttay/QtZJWyGuCaN6c0BmuWdjnyciFbYrsTUS+PxSlTaDkILBmQpnRdstfC3hq0uAqRRvw/clZTk3at9dmKHffaig+jdFAAGiNcUMYEYUsnUsMgtOoLsidin1SilqCFFg8u2Ila8aNxie+XBoB6uRddj7VM9Wb0NmjEsMz3weSfIdgqhxaWlRBe4K7SBRRQvtYKrhmZNIiM6PMRpZwzaLwoasBbJsypplnXquspxfZNyY9VuUPywWa9/9qbKcw5ESmQWGAPcoyzlM++6rlTqMsihmUEHbbKgKWonQDp4O7eCK0A4+hdxQu1D+RB5C9Rukr3Ah6Y7bHZB4QEI7eJ9ytbQSmyBrveOQ4klZhe990kOlzqAx5vae8oPQbkgk4VDgV8pydqnWSFmOTXLYWiGh/DuUW6jHI+GVpyvLKTIgKcsZpCznOCRk8qOVrtsSAYwT3ZBtJBff9QzP/GSTh1QToR28QyRcKVxgeGYhFvNqJNKkUZwb2sF71RpEWft9pabp7J5uylKFuLDsiAjKefGEgEhI9lWWcxvJVe6eKRzXvvscsfKMMbZC9iVZrCxnrrKcW5Xl3A+8iOROFgqFrSXBANdyAYx2+HmC5Kf75kBgeOZRzR1VzUxDnNjVGEg0K4V2sBZRwZNKffSWy0I7KA0oTiS0gx8h5RpayeOIbaDeaIqTh7dEZrPFynIWKsv5E7Ib1RMkLyFWIBv8dKm52nd/QOW0s60RA85pwCSK7+d1iDpf5mNtqQAannkWklJUzbE5HJhteGajisH2mtAO1pOtWO2nDc88PTrnDSREK2vN0zRWAnZoB1eltizmDLJvQFNPNJI98pnQDpJU+CyGn2ptBiC5qqWz/AjEwHMglR3/SwFT++6LpV9o370A+b/OWnd2FbJ2nElCHZmWCGCUGPoTJAs5a7TINMMzb4lM8PUkrUxgJudwaAfzyba268oOCe3gndAOjkAih6pZ86rRjqQE7RXawc21nhzawaskW/4axSuI4O0V2sE5oR1UWodmCXaOR0NpyrcjGKV99/uITzNr8HQA7K999/FKDbTvTkeE+Doq74GyDPl/2Vv77oNUqC6vtG5oBfcyDM/cHSlQk9WgUMprwKTQDuqyhorWmNVuwMdCO5heQ39nAJ+k8o5Bc0I7+GXCeYOQRNUTkTy5Han8gFyG1BuZA9wd2sHfso6vyrgPQeJxN6d+W4prZKZYivj4QiAM7SDVuh1FpFxCZQPGK8BU7bv/itoPRMpOFBz+7Uh5wb9F3xvAfyN/47iSvv6OpLj9XPtuTRqJspzB0Rh3Qh7W66L+XtC+uzTWbjdk3R+ftZ9thQBOQAod7Y7svjuO9FmoE/nBH49ezawr0nSiGNJxiPV0DN3uohWIb+/10A5aFZq3QRMJzDhkRtLAP4E3tO82dC9BZTkm5ZuF3tN0AYwTxTNuizw9tkFutkJ85QdIoPbbiPC9FuUd5uRscCjLuQ64IHZoLXBeSwUwJ2djICrq9FeKdx5bDBzYcjdETk5/JlJ5b6d827/7tO8ubVooWk5OfyCKmNkTyQn9S7WUJGU5n0EyS0orKHyABNOTq6A5ORmJ6tD+ne5dpF5FtmN4DiljsQaJ8f0I4mesFEx+ivbd26GJwdg5Of2Asynewm1c9EoKo6zEeQXhgz4QipaTswFRa1pWnOeAw0uLBOcqaE5ODSjL2QvZCu/TSK7jSJI1ybWIG20B4CM7dpWlT+UCmJPTQ5TlfAJJE9sbiYbZBIlSehEJGHlQ+27Vbbr/Hz9xYyqDrBDfAAAAAElFTkSuQmCC">
    <p style="float: left; text-align: right; color: #003240; width: 100%;">{!! date('d-m-Y', time()) !!}</p>
    <!-- <table class='logo-table'><tr><td style='width:250px'></td>
    <td class="pdf-title">Тест по английски език</td></tr>
    </table> -->
    <h2 class="center titles">English Placement Test</h2>
    <h2 class="center titles">Тест за определяне на ниво по английски език</h2>
    {!! $html !!}
    <br>
    <p class="footer">Адрес: <span style="color: #299B47!important;">гр. София, ул. Акад. Борис Стефанов №35, South Mall</span> | Телефон: <span><a href="tel:+359894348521" style="color: #299B47!important;">+359 894 348 521</a></span> | Имейл: <span><a href="mailto:office@kurs.bg" style="color: #299B47!important;">office@kurs.bg</a></span></p>
</body>
</html>
