﻿<h2 class="green">Таблица с резултати по нива</h2>
<table>
    <tr class="">
        <td class="green">Въпроси</td>
        <td class="green center">1-36</td>
        <td class="green center">37-56</td>
        <td class="green center">57-76</td>
        <td class="green center">77-96</td>
        <td class="green center">97-116</td>
    </tr>
    @foreach( $rows as $key => $value )
        <tr>
            <td class="green">{{ $rowTitles[$key] }}</td>
            @foreach($results as $result)
                <td class="center">{{ $result[$value] }}</td>
            @endforeach
        </tr>
    @endforeach
</table>
<p class="footer">Адрес: <span style="color: #299B47!important;">гр. София, ул. Акад. Борис Стефанов №35, South Mall</span> | Телефон: <span><a href="tel:+359894348521" style="color: #299B47!important;">+359 894 348 521</a></span> | Имейл: <span><a href="mailto:office@kurs.bg" style="color: #299B47!important;">office@kurs.bg</a></span></p>