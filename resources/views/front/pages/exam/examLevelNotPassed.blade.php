@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <h1>Край на теста!</h1>
        <br/>
        <p>{!! nl2br(e(trans('app.exam_result'))) !!}</p>
        <!-- Грешни: { $wrong }
        Верни: { $correct } -->
    	@include('front.pages.exam.social_icons_text')
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop