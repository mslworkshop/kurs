﻿<div class="new-page"></div>
<h2 class="green">Краен резултат от теста</h2>
<table style="width: 50%">
    <tr>
        <td class="green">Въпроси:</td>
        <td class="center">{{ $totals['correct'] + $totals['wrong'] }}</td>
    </tr>
    <tr>
        <td class="green">Верни:</td>
        <td class="center">{{ $totals['correct'] }}</td>
    </tr>
    <tr>
        <td class="green">Грешни:</td>
        <td class="center">{{ $totals['wrong'] }}</td>
    </tr>
    <tr>
        <td class="green">%</td>
        <td class="center">{{ round($totals['correct'] / ($totals['correct'] + $totals['wrong']) * 100, 2) }}</td>
    </tr>
</table>