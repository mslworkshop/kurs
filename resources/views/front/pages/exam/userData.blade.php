﻿<h2 class="">Информация</h2>
<table>
    <tr>
        <td class="green">{{ trans('app.name') }}:</td>
        <td class="">{{ $userdata['name'] }}</td>
    </tr>
    <tr>
        <td class="green">{{ trans('app.email') }}:</td>
        <td class="">{{ $userdata['email'] }}</td>
    </tr>
    <tr>
        <td class="green">{{ trans('app.phone') }}:</td>
        <td class="">{{ $userdata['phone'] }}</td>
    </tr>
    <tr>
        <td class="green">{{ trans('app.format_of_education') }}:</td>
        <td class="">
            @foreach($userdata['form_of_education'] as $k => $obj)
                {{ \Config::get('constants.form_of_education_exam')[$obj] }} @if ($obj != end($userdata['form_of_education'])) {{ " / " }} @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <td class="green">{{ trans('app.course') }}:</td>
        <td class="">
            @foreach($userdata['course'] as $k => $obj)
                {{ \Config::get('constants.course')[$obj] }} @if ($obj != end($userdata['course'])) {{ " / " }} @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <td class="green">{{ trans('app.days') }}:</td>
        <td class="">
            @foreach($userdata['days'] as $k => $obj)
                {{ \Config::get('constants.days_other')[$obj] }} @if ($obj != end($userdata['days'])) {{ " / " }} @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <td class="green">{{ trans('app.timing') }}:</td>
        <td class="">
            @foreach($userdata['timing'] as $k => $obj)
                {{ \Config::get('constants.timing_exam')[$obj] }} @if ($obj != end($userdata['timing'])) {{ " / " }} @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <td class="green">{{ trans('app.form_of_education') }}:</td>
        <td class="">
            @foreach($userdata['educ_form'] as $k => $obj)
                {{ \Config::get('constants.educ_form_list')[$obj] }} @if ($obj != end($userdata['educ_form'])) {{ " / " }} @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <td class="green">Офис:</td>
        <td class="">
            @foreach($userdata['office'] as $k => $obj)
                {{ \Config::get('constants.educ_office')[$obj] }} @if ($obj != end($userdata['office'])) {{ " / " }} @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <td class="green">Допълнителна информация:</td>
        <td class="">{{ $userdata['details'] }}</td>
    </tr>
</table>