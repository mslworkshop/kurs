﻿@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <h2 style="color:#299B47;">Край на теста!</h2>
        <p>{!! nl2br(e(trans('app.exam_result'))) !!}</p>
    	
    	@include('front.pages.exam.social_icons_text')
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop