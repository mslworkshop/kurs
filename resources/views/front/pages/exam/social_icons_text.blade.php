﻿<div class="col-md-12 row">
    <br/>
    <p>Можете да ни последвате в социалните мрежи: </p>
     
    <ul class="footer-socials list-inline pull-left">
        <li>
            <a href="https://www.facebook.com/kursbg" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                <i class="fa fa-exam fa-facebook"></i>
            </a>
        </li>
        <li>
            <a href="https://plus.google.com/+KursBgbg" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                <i class="fa fa-exam fa-google-plus"></i>
            </a>
        </li>
        <li>
            <a href="https://www.instagram.com/kursbg/" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram">
                <i class="fa fa-exam fa-instagram"></i>
            </a>
        </li>
    </ul>
</div>