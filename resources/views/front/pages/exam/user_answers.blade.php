<div class="new-page"></div>
<h2>Въпроси и отговори</h2>
@if(!empty($questions))
    @foreach($questions as $k => $object)
    <div class="panel panel-default exam-panel">
        <h4 class="panel-title">{!! $object->id !!}. {!! $object->text !!} - @if(!isset($userAnswers[$object->id - 1]) || $userAnswers[$object->id - 1] != $object->correct)<span class="red">Грешен отговор</span> @else <span class="green">Верен отговор</span> @endif</h4>
        <div class="coll">
            @foreach($object->answers as $k => $answer)
                @if(!empty($answer))
                    @if(isset($userAnswers[$object->id - 1]) && $userAnswers[$object->id - 1] == $k)                       
                        @if($object->correct == $k)
                            <p class="green"><b>{!!  $k.") ".$answer !!}</b></p>
                        @else
                            <p class="red"><b>{!!  $k.") ".$answer !!}</b></p>
                        @endif
                    @else
                        @if($object->correct == $k)
                            <p class="green">{!!  $k.") ".$answer !!}</p>
                        @else
                            <p>{!!  $k.") ".$answer !!}</p>
                        @endif
                    @endif
                @endif
            @endforeach
        </div>
    </div>
    @endforeach
@endif