﻿@extends('front.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/front/plugins/sky-forms-pro/skyforms/css/sky-forms.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/css/pages/page_contact.css') }}">
@stop

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')
    <style type="text/css">
        .sky-form fieldset{
            background: #fff;
        }
        .header-text{
            font-size: 24px;
            padding-left: 30px;
            padding-top: 8px;
            border-bottom: 1px solid #eee;
        }
    </style>
    <div class="col-md-9">
    <style type="text/css">
        .with-combos{

        }
    </style>
        <form action="{{ route('requestExamSubmit') }}" method="post" class="sky-form with-combos @if(session()->has('successfuly_sended_request'))submited @endif">
            {!! csrf_field() !!}
            @if(session()->has('successfuly_sended_request'))
            <div class="message" style="display:block;">
                <i class="rounded-x fa fa-check"></i>
                <p>{{ trans('app.your_messages_is_successfuly_you_will_connected_with_us_soon') }}</p>
            </div>
            @else
            <h2 class="header-text">{{ trans('app.automatic_text') }} </h2>
            @include('errors.admin.errors')
            <fieldset>
                <section>
                    Преди да преминете към теста за определяне на ниво, моля попълнете информацията. <br>
                    Резултата от теста ще получите на посочения от Вас имейл.
                </section>
            </fieldset>
            <fieldset>
                <h2>Информация</h2>
                <section>
                    <label class="label"><strong>{{ trans('app.name') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="name" value="{{ old('name') }}"></label>
                </section>
                <section>
                    <label class="label"><strong>{{ trans('app.email') }}:</strong> <span class="color-red">*</span></label>
                    <label class="input">
                    <input type="text" name="email" value="{{ old('email') }}"></label>
                </section>
                <section>
                    <label class="label"><strong>{{ trans('app.phone') }}:</strong></label>
                    <label class="input">
                    <input type="text" name="phone" value="{{ old('phone') }}" placeholder="0894348521"></label>
                </section>
            </fieldset>
            <fieldset>

            <h2>Предпочитан курс</h2>
                <section>
                    <label class="label"><strong>{{ trans('app.format_of_education') }}:</strong> <span class="color-red">*</span></label>
                    @foreach(\Config::get('constants.form_of_education_exam') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="form_of_education[]" value="{{ $k }}" @if(old('form_of_education') && in_array($k, old('form_of_education'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                    <!-- <select class="form-control" name="form_of_education"> -->
                        <!-- <option value="{{ $k }}" @if($k == old('form_of_education')) selected="selected" @endif>{{ $obj }}</option> -->
                        <!-- <option value="">{{ trans('app.select') }}:</option> -->
                    <!-- </select> -->
                </section>
                <!-- <section>
                    <label class="label"><strong>{{ trans('app.language') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="language">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.language') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('language')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section> -->

                <!-- <section>
                    <label class="label"><strong>{{ trans('app.level') }}:</strong> <span class="color-red">*</span></label>
                    <select class="form-control" name="level">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.level') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('level')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select>
                </section> -->
                <section>
                    <label class="label"><strong>{{ trans('app.course') }}:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="course">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.course') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('course')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    <div class="row">
                        <div class="col-md-12">
                            @foreach(\Config::get('constants.course') as $k => $obj)
                            <!-- <div class="@if($k == 1) col-md-12 @else col-md-6 @endif"> -->
                                <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox1" name="course[]" value="{{ $k }}" @if(old('course') && in_array($k, old('course'))) checked="checked" @endif>{{ $obj }}</label>
                            <!-- </div> -->
                            @endforeach
                        </div>
                    </div>
                </section>

                <section>
                    <label class="label"><strong>Дни на провеждане:</strong> <span class="color-red">*</span></label>
                    @foreach(\Config::get('constants.days_other') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="days[]" value="{{ $k }}" @if(old('days') && in_array($k, old('days'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                    <!-- <select class="form-control" name="days">
                        <option value="">{{ trans('app.select') }}:</option>
                        <option value="{{ $k }}" @if($k == old('days')) selected="selected" @endif>{{ $obj }}</option>
                    </select> -->
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.timing') }}:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="timing">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.timing_exam') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('timing')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    @foreach(\Config::get('constants.timing_exam') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="timing[]" value="{{ $k }}" @if(old('timing') && in_array($k, old('timing'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                </section>

                <section>
                    <label class="label"><strong>{{ trans('app.form_of_education')}}:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="educ_form">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.educ_form_list') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('educ_form')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    @foreach(\Config::get('constants.educ_form_list') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="educ_form[]" value="{{ $k }}" @if(old('educ_form') && in_array($k, old('educ_form'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                </section>

                <section>
                    <label class="label"><strong>Предпочитан офис:</strong> <span class="color-red">*</span></label>
                    <!-- <select class="form-control" name="office">
                        <option value="">{{ trans('app.select') }}:</option>
                        @foreach(\Config::get('constants.educ_office') as $k => $obj)
                        <option value="{{ $k }}" @if($k == old('office')) selected="selected" @endif>{{ $obj }}</option>
                        @endforeach
                    </select> -->
                    @foreach(\Config::get('constants.educ_office') as $k => $obj)
                        <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="office[]" value="{{ $k }}" @if(old('office') && in_array($k, old('office'))) checked="checked" @endif>{{ $obj }}</label>
                    @endforeach
                </section>

                <section>
                    <label class="label"><strong>Допълнителна информация:</strong></label>
                    <textarea name="details" class="form-control" maxlength="12500" rows="5"></textarea>
                </section>

            </fieldset>
            <fieldset class="tag-box">
                <h2>Инструкции</h2>
                <ul class="list-unstyled">
                   <li>Всеки въпрос от теста има три или четири варианта на отговор (a, b, c, d).</li>
                   <li>Всеки въпрос има само един коректен отговор.</li>
                   <li>Маркирайте отговора, който смятате за верен.</li>
                </ul>
            </fieldset>

            
            <footer>
                <button type="submit" class="btn-u">{{ trans('app.start_now') }}</button>
            </footer>
            @endif
        </form>
    </div><!--/col-md-9-->

    <!--/col-md-3-->
</div><!--/row-->

@stop

