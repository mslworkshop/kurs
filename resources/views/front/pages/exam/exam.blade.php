@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <h1>Тест за определяне на ниво по английски език</h1>
        <form class="tag-box tag-box-v3" action="{{ route('nextStep') }}" method="post" id="sky-form3">
            {!! csrf_field() !!}
            <input type="hidden" name="step" value="{{$next}}">
            <!-- <div class="headline"><h2>{{ trans('app.faq_for_language_learning') }}</h2></div> -->
            @if(!empty($questions))
            <div class="panel-group acc-v1 margin-bottom-40" id="accordion">
                @foreach($questions as $k => $object)
                <div class="panel panel-default exam-panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">{!! $object->id !!}. {!! $object->text !!}</h4>
                        <!-- -->
                    </div>
                    <div id="collapse{{ $object->id }}" class="coll">
                        <!-- <div class="panel-body">{!! $object->text !!}</div> -->
                        @foreach($object->answers as $k => $answer)
                            @if(!empty($answer))
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="answer_{{ $object->id }}" id="input" value="{{ $k }}">
                                        {!!  $k.") ".$answer !!}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                @endforeach
                <button type="submit" class="btn-u mt25">{{ trans('app.continue') }}</button>
            </div>
            @endif
        </form>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop