@extends('front.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/front/css/pages/page_clients.css') }}">
@stop

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            @if(!empty($objects))
            @foreach($objects as $object)
            <div class="row clients-page">
                <div class="col-md-2">
                    @if(isset($object->images[0]))
                    <img src="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$object->id.'/'.$object->images[0]->filename, 140, 140) }}" class="img-responsive hover-effect" alt="{{ htmlspecialchars($object->title) }}" />
                    @endif
                </div>
                <div class="col-md-10">
                    <h3>{{ $object->title }}</h3>
                    <p>{!! $object->content !!}</p>
                </div>
            </div>
            @endforeach
            <div class="text-right">
                {!! $objects->render() !!}
            </div>
            @endif
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop