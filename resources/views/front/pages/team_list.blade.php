@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            @if(!empty($objects))
            <div class="row team-v6 margin-bottom-20">
            @foreach($objects as $object)
                <div class="col-md-3 col-sm-6 md-margin-bottom-20">
                    @if(isset($object->images[0]))
                    <img class="img-responsive" src="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$object->id.'/'.$object->images[0]->filename, 180, 180) }}" alt="{{ htmlspecialchars($object->title) }}">
                    @endif
                    <span>{{ $object->title }}</span>
                    <small>{!! $object->content !!}</small>
                </div>
            @endforeach
            </div>
            @endif
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop