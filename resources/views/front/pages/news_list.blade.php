@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            @if(!empty($objects))
            @foreach($objects as $object)
            <div class="row margin-bottom-20">
                @if(isset($object->images[0]))
                <div class="col-sm-4 sm-margin-bottom-20">
                    <img class="img-responsive" src="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$object->id.'/'.$object->images[0]->filename, 350, 220) }}" alt="{{ htmlspecialchars($object->title) }}">
                </div>
                @endif
                <div class="col-sm-8 news-v3">
                    <div class="news-v3-in-sm no-padding">
                        <h2><a href="{{ url($object->slug) }}" title="{{ $object->title }}">{{ $object->title }}</a></h2>
                        {!! str_limit($object->short_description, 210) !!}</p>
                        <a class="btn-more hover-effect" href="{{ url($object->slug) }}" title="{{ trans('app.more') }}">{{ trans('app.more') }} +</a>
                    </div>
                </div>
            </div>
            <hr>
            @endforeach
            <div class="text-right">
                {!! $objects->render() !!}
            </div>
            @endif
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop