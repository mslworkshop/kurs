@extends('front.layout')

@section('content')
<div class="row-fluid privacy">
    {!! $obj->content !!}
</div><!--/row-fluid-->

@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/front/js/app.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    App.init();
    StyleSwitcher.initStyleSwitcher();
    });
</script>
@stop