@extends('front.layout')

@section('content')

<div class="row margin-bottom-30">

    @include('front.partials.leftSideBar')

    <div class="col-md-9">
        <div class="tag-box tag-box-v3">
            <div class="message" style="display:block;">
                <!-- <i class="rounded-x fa fa-check"></i> -->
                <p>{!! $obj->content !!}</p>
            </div><!--/col-md-9-->
        </div>
    </div>
</div>
@stop

