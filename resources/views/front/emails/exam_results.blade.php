﻿@if($isForUser == true)
    <p style="color: #005670!important;">Здравейте, {{$userdata["name"]}}! <br><br>
    Успешно попълнихте и ни изпратихте тест за определяне на ниво по английски език.<br>
    Ще Ви изпратим резултата му, както и подходящо предложение за обучение, в рамките на работния
    ден.</p>
@else
    <p style="color: #005670!important;">Резултат от тест за ниво по английски език на {{$userdata["name"]}}</p>
@endif
<div class="col-md-12 text-center social-icons">
    <br/>
    <p style="width: 100%; color: #005670!important;">тел.: <span><a href="tel:+359894348521" style="color: #299B47!important;">+359 894 348 521</a></span> e-mail: <span><a href="mailto:office@kurs.bg" style="color: #299B47!important;">office@kurs.bg</a></span> web: <span><a href="www.kurs.bg" style="color: #299B47!important;">www.kurs.bg</span></a></p>
    <!-- <div class="contacts" style="width: 100%font-weight: bold!important">тел.: <div style="color: #299B47!important">+359 894 348 521</div> e-mail: <div style="color: #299B47!important">office@kurs.bg</div> web: <div style="color: #299B47!important">www.kurs.bg</div></div> -->
    <br/>
    <p>
        <a href="https://www.facebook.com/kursbg" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
            <img src="{{ url('/assets/img/facebook-icon.png') }} ">
        </a>
        <a href="https://plus.google.com/+KursBgbg" class="tooltips" data-toggle="tooltip" target="_blank" data-placement="top" title="" data-original-title="Google Plus">
            <img src="{{ url('/assets/img/google-icon.png') }} ">
        </a>
        <a href="https://www.instagram.com/kursbg/" class="tooltips" data-toggle="tooltip" target="_blank" data-placement="top" title="" data-original-title="Linkedin">
            <img src="{{ url('/assets/img/instagram-icon.png') }} ">
        </a>
    </p>
</div>
