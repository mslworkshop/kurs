﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="bg"> <!--<![endif]-->
<head>
    <title>KURS.bg - Резултати от онлайн изпит</title>
    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- custom css -->
    <style>
        * { font-family: DejaVu Sans, sans-serif; }
        body {
            padding: 0; margin: 0; color: #005670!important; font-size: 14px!important;
        }
        p {margin: 0;}
        a{
            text-decoration: none!important;
        }
    </style>
</head>

<body style="color: #005670!important">
    <strong>{{ trans('app.name') }}: </strong> {{ $name }}<br />
    <strong>{{ trans('app.email') }}: </strong> {{ $email }}<br />
    <strong>{{ trans('app.phone') }}: </strong> {{ $phone }}<br />
    <strong>{{ trans('app.language') }}: </strong>
        @foreach($language as $k => $obj)
            {{ \Config::get('constants.language')[$obj] }} @if ($obj != end($language)) {{ " / " }} @endif
        @endforeach
    <br />
    <strong>{{ trans('app.level') }}: </strong>
        @foreach($level as $k => $obj)
        {{ \Config::get('constants.level')[$obj] }} @if ($obj != end($level)) {{ " / " }} @endif

        @endforeach
    <br />
    <strong>{{ trans('app.format_of_education') }}: </strong>
        @foreach($form_of_education as $k => $obj)
        {{ \Config::get('constants.form_of_education_exam')[$obj] }} @if ($obj != end($form_of_education)) {{ " / " }} @endif
        @endforeach
    <br />
    <strong>{{ trans('app.course') }}: </strong>
        @foreach($course as $k => $obj)
        {{ \Config::get('constants.course')[$obj] }} @if ($obj != end($course)) {{ " / " }} @endif
        @endforeach
    <br />

    <strong>{{ trans('app.days') }}: </strong>
        @foreach($days as $k => $obj)
        {{ \Config::get('constants.days')[$obj] }} @if ($obj != end($days)) {{ " / " }} @endif
        @endforeach
    <br />
    <strong>{{ trans('app.timing') }}: </strong>
        @foreach($timing as $k => $obj)
        {{ \Config::get('constants.timing')[$obj] }} @if ($obj != end($timing)) {{ " / " }} @endif
        @endforeach
    <br />
    <strong>{{ trans('app.form_of_education') }}: </strong>
        @foreach($educ_form as $k => $obj)
        {{ \Config::get('constants.educ_form_list')[$obj] }} @if ($obj != end($form_of_education)) {{ " / " }} @endif
        @endforeach
    <br />
    <strong>Предпочитан офис: </strong>  
        @foreach($office as $k => $obj)
        {{ \Config::get('constants.educ_office')[$obj] }} @if ($obj != end($office)) {{ " / " }} @endif
        @endforeach
    <br />
    <strong>Допълнителна информация: </strong>{{ $details }}<br />
</body>
</html>
