<div style="color: #005670!important">
    <strong>{{ trans('app.name') }}: </strong> {{ $name }}<br />
    <strong>{{ trans('app.email') }}: </strong> {{ $email }}<br />
    <strong>{{ trans('app.phone') }}: </strong> {{ $phone }}<br />
    <strong>{{ trans('app.message') }}: </strong> {{ $message }}<br />
</div>