@extends('front.layout')

@section('containerClass')content-sm @stop

@section('preContainer')
<!--=== Slider ===-->
<div class="ms-layers-template">
<!-- masterslider -->
    <div class="master-slider ms-skin-black-2 round-skin" id="masterslider">
        @foreach($slider as $slide)
        <div class="ms-slide" style="z-index: 10" data-delay="5">
            <img src="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SLIDERS_DIR').$slide->image, 1600, 818) }}" data-src="{{ Croppa::url(url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SLIDERS_DIR').$slide->image), 1600, 818) }}" alt="">
            <div class="ms-layer ms-promo-info" data-effect="bottom(40)" data-duration="1500" data-delay="50" data-ease="easeOutExpo">{{ $slide->title }}</div>
            <div class="ms-layer ms-promo-info ms-promo-info-in" data-effect="bottom(60)" data-duration="1800" data-delay="280" data-ease="easeOutExpo">
                <span class="color-green">"{{ $slide->text1 }}"</span>
                <span class="ms-promo-sub">{{ $slide->text2 }}</span>
            </div>
        </div>
        @endforeach
    </div>
    <!-- end of masterslider -->
</div>
<!--=== End Slider ===-->
<!--=== Purchase Block ===-->
@if(isset($home_pages[3]))
<div class="purchase">
    <div class="container">
        <div class="row">
            <!-- Levels -->
            <div class="col-md-9 animated fadeInLeft">
                <div class="headline"><h2>{!! $home_pages[3]->title !!}</h2></div>
                <div class="row">
                    <div class="col-sm-4"> <a href="{{ url($home_pages[3]->slug) }}" title="{!! $home_pages[3]->title !!}"><img class="img-responsive" src="{{ asset('assets/front/img/main/levels.jpg') }}" alt=""></a>
                    </div>
                    <div class="col-sm-8">
                        {!! $home_pages[3]->short_description !!}
                    </div>
                </div>
            </div><!--/col-md-8-->

            <div class="col-md-3 btn-buy animated fadeInRight">
                <a href="{{ url('/zayavka-za-kurs') }}" title="{{ trans('app.request_course') }}" class="btn-u btn-u-lg"><i class="fa fa-cloud-download"></i> {{ trans('app.request_course') }}</a><br>
<a href="{{ url('/opredelete-ezikovo-nivo') }}" title="{{ trans('app.request_test') }}" class="btn-u btn-u-lg"><i class="fa fa-cloud-download"></i> {{ trans('app.request_test') }}</a>
            </div>
        </div>
    </div>
</div><!--/row-->
@endif
<!-- End Purchase Block -->
@stop

@section('content')
    <!-- Recent Works -->
    <div class="headline"><h2>{{ trans('app.kind_of_language_courses') }}</h2></div>
    <div class="row margin-bottom-20">
        @foreach($home_courses as $obj)
        <div class="col-md-4 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                @if(isset($obj->images[0]))
                <div class="thumbnail-img">
                    <div class="overflow-hidden">
                        <img class="img-responsive" src="{{ Croppa::url(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$obj->id.'/'.$obj->images[0]->filename, 350, 220) }}" alt="">
                    </div>
                    <a class="btn-more hover-effect" title="{{ trans('app.courses_for') }} {{ $obj->title }}" href="{{ url($obj->slug) }}">{{ trans('app.more') }} +</a>
                </div>
                @endif
                <div class="caption">
                    <h3><a class="hover-effect" title="{{ trans('app.courses_for') }} {{ $obj->title }}" href="{{ url($obj->slug) }}">{{ trans('app.courses_for') }} {{ $obj->title }}</a></h3>
                    {!! $obj->short_description !!}
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- End Recent Works -->
@stop