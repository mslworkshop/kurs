<div class="col-md-3">
    <!-- Contacts -->
    @if(isset($contactsLeftInfo))
    <div class="headline"><h2>{{ trans('app.contact_data') }}</h2></div>
    {!! $contactsLeftInfo !!}
    @elseif(!empty($leftSideBarMenu))
    <p class="menu-title">{{ $leftMenuTitle }}</p>
    <ul class="list-group sidebar-nav-v1" id="sidebar-nav">
        @each('front.partials.left_list_menu', $leftSideBarMenu, 'object')
    </ul>
    @endif

    <div class="thumbnail-img margin-bottom-30"> <a href="{{ url('/opredelete-ezikovo-nivo') }}"><img class="img-responsive" src="{{ asset('assets/front/img/main/levels.jpg') }}" alt=""></a></div>

    <!-- <div class="btn-buy animated fadeInLeft margin-bottom-30">
        <a href="{{ url('/zayavka-za-kurs') }}" class="btn-u btn-u-lg-left"><i class="fa fa-cloud-download"></i>  {{ trans('app.request_course') }}</a>
    </div> -->

    @if(!empty($leftTesimonial))
    <blockquote>
        <h2>{{ $leftTesimonial->title }}</h2>
        {!! $leftTesimonial->content !!}
    </blockquote>
    @endif

</div>