<li>
    <a href="{{ ($object['level'] > 0 || sizeof($object['children']) == 0? url($object['slug']): 'javascript:void(0);') }}" title="{{ $object['title'] }}">
        {{ $object['title'] }}
    </a>
    @if(!empty($object['children']))
    <ul>
        @each('front.partials.list_sitemap', $object['children'], 'object')
    </ul>
    @endif
</li>

