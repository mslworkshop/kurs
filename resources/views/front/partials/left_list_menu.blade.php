<li class="@if($object['level'] == 1 ) list-group-item @endif
@if(sizeof($object['children']) > 0 ) list-toggle @endif
@if(url($object['slug']) == \URL::current() || isset($activeMenu[$object['id']]))active @endif">
    <a @if(sizeof($object['children']) >0 )data-toggle="collapse" data-parent="#sidebar-nav" @endif
    href="{{ (sizeof($object['children']) == 0? url($object['slug']): '#collapse-typography'.$object['id']) }}"
    @if(sizeof($object['children']) == 0 && $object['level'] == 1) class="collapse" @endif title="{{ $object['title'] }}">
        {{ $object['title'] }}
    </a>
    @if(!empty($object['children']))
    <ul id="collapse-typography{{ $object['id'] }}" class="collapse @if(isset($activeMenu[$object['id']]))in @endif">
        @each('front.partials.left_list_menu', $object['children'], 'object')
    </ul>
    @endif
</li>


