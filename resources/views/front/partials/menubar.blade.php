<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="dropdown @if(\Request::is('/'))active @endif">
                <a href="{{ route('home')}}" class="dropdown-toggle">
                {{ trans('app.home') }}</a>
            </li>
            @each('front.partials.list_menu', $top_navigation->viewList(), 'object')
        </ul>
    </div><!--/end container-->
</div><!--/navbar-collapse-->