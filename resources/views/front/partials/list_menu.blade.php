<li class="dropdown{{ ($object['level'] > 0? '-submenu': '') }}
    @if(url($object['slug']) == \URL::current() || isset($activeMenu[$object['id']]))active @endif">
    <a href="{{ ($object['level'] > 0 || sizeof($object['children']) == 0? url($object['slug']): 'javascript:void(0);') }}"
    @if(sizeof($object['children']) > 0 && $object['level'] == 0) class="dropdown-toggle" data-toggle="dropdown" @endif title="{{ $object['title'] }}">
        {{ $object['title'] }}
    </a>
    @if(!empty($object['children']))
    <ul class="dropdown-menu">
        @each('front.partials.list_menu', $object['children'], 'object')
    </ul>
    @endif
</li>

