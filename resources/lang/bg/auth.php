<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'inactive' => 'Потребителят е деактивиран. Моля свържете се с администратора.',
    'failed' => 'Няма съвпадение.',
    'throttle' => 'Твърде много опити, моля пробвайте след :seconds секунди.',

];
