<?php

return [
    'pages' => 'Страници',
    'page' => 'Страница',
    'show_in_menu_bar' => 'Показва се в меню',
    'in_menu_bar' => 'Показва се в меню',
    'add_pages' => 'Добави страница',
    'edited' => ' е редактирана'
];