<?php

return [
    'sender' => 'noreply@kurs.bg',
    'toadmin_email' => 'office@kurs.bg',
    //settings
    'settings' =>[
                'is_multiple_upload_news' => false,
                'is_title_image_news' => false,
                'is_multiple_upload_pages' => false,
                'is_title_image_pages' => false,
                'max_upload_size' => 2000,
                ],
    //upload dirs
    'UPLOADS_DIR' => 'uploads/',
    'SLIDERS_DIR' => 'sliders/',
    'SIMPLE_IMAGES_DIR' => 'images/',
    'PAGES_DIR' => 'pages/',
    'NEWS_DIR' => 'news/',

    //request form select`s
    'language' =>[
                1 => 'Английски',
                2 => 'Немски',
                3 => 'Френски',
                4 => 'Испански',
                5 => 'Италиански',
                6 => 'Руски',
                7 => 'Гръцки',
                8 => 'Китайски'
    ],
    'level' =>[
                1 => 'А1',
                2 => 'А2',
                3 => 'В1',
                4 => 'В2',
                5 => 'С1',
                6 => 'C2'
    ],
    'course' =>[
                1 => 'По нива',
                2 => 'Разговорен',
                3 => 'Сертификатен',
                4 => 'Бизнес',
                5 => 'Специализиран',
    ],
    'days' =>[
                1 => 'През седмицата',
                2 => 'Събота и неделя',
    ],
    'days_other' =>[
                1 => 'Делнични дни',
                2 => 'Уикенд',
    ],
    'timing' =>[
                1 =>'Сутрешен курс',
                2 => 'Следобеден курс',
                3 => 'Вечерен курс',
    ],
    'form_of_education' =>[
                1 => 'Групово',
                2 => 'Индивидуално',
                3 => 'Онлайн обучение',
    ],
    'form_of_education_exam' =>[
                1 => 'Групово',
                2 => 'Индивидуално'
    ],
    'timing_exam' =>[
                1 =>'Сутрешен курс',
                2 => 'Дневен курс',
                3 => 'Вечерен курс',
    ],
    'educ_form_list' => [
                1 =>'Присъствен',
                2 => 'Дистанционен (онлайн)'
    ],
    'educ_office' => [
                1 =>'Орлов мост',
                2 => 'Център',
                3 => 'Студентски град'
    ]
];