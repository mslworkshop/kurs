<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use \Dimsav\Translatable\Translatable;

    //use SoftDeletes;

    //protected $dates = ['deleted_at'];

    public $translatedAttributes = ['title', 'content', 'meta_title', 'meta_description'];
    protected $fillable = ['title', 'content', 'meta_title', 'meta_description', 'home', 'slug', 'published'];

}
