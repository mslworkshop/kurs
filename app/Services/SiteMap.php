<?php

namespace App\Services;

use App\Pages;
use App\Images;
use Carbon\Carbon;
use Cache;

class SiteMap
{
    /**
   * Return the content of the Site Map
   */
    public function getSiteMap()
    {
        if (Cache::has('site-map')) {
          return Cache::get('site-map');
        }

        $siteMap = $this->buildSiteMap();
        Cache::add('site-map', $siteMap, 120);
        return $siteMap;
    }

  /**
   * Build the Site Map
   */
    protected function buildSiteMap()
    {
        $pagesInfo = $this->getPagesInfo();
        $dates = array_values($pagesInfo);
        sort($dates);
        $lastmod = last($dates);
        $url = str_finish(url(), '/');

        $xml = [];
        $xml[] = '<?xml version="1.0" encoding="UTF-8"?'.'>';
        $xml[] = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $xml[] = '  <url>';
        $xml[] = "    <loc>$url</loc>";
        $xml[] = "    <lastmod>$lastmod</lastmod>";
        $xml[] = '    <changefreq>daily</changefreq>';
        $xml[] = '    <priority>0.8</priority>';
        $xml[] = '  </url>';

        foreach ($pagesInfo as $slug => $lastmod) {
            if($slug != 'gallery'){
                $xml[] = '  <url>';
                $xml[] = "    <loc>{$url}$slug</loc>";
                $xml[] = "    <lastmod>$lastmod</lastmod>";
                $xml[] = "  </url>";
            }
        }

        $simpleImagesDate = $this->getSimpleImagesInfo();
        $xml[] = '  <url>';
        $xml[] = "    <loc>{$url}gallery</loc>";
        $xml[] = "    <lastmod>$simpleImagesDate</lastmod>";
        $xml[] = "  </url>";

        $xml[] = '</urlset>';

        return join("\n", $xml);
    }

    /**
    * Return all the pages as $url => $date
    */
    protected function getPagesInfo()
    {
        return Pages::where('created_at', '<=', Carbon::now())
                ->where('published', 1)
                ->orderBy('created_at', 'desc')
                ->lists('updated_at', 'slug')
                ->all();
    }

    /**
    * Return first images as $date
    */
    protected function getSimpleImagesInfo()
    {
        return Images::where('created_at', '<=', Carbon::now())
                ->where('object_type', 'App\SimpleImages')
                ->orderBy('created_at', 'desc')
                ->lists('updated_at')
                ->first();
    }

  }