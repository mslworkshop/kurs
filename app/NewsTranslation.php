<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'content', 'meta_title', 'meta_description', 'home', 'slug'];
}
