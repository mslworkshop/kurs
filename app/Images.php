<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $fillable = ['title', 'description', 'filename', 'object_id', 'object_type', 'sortorder', 'show'];

    public function object()
    {
        return $this->morphTo();
    }

}
