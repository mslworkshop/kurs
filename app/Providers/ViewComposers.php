<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposers extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if(strpos(\URL::current(), 'admin') === false){
            /*view()->composer('*', 'App\Http\ViewComposers\MenuComposer');
            view()->composer('*', 'App\Http\ViewComposers\ContactsComposer');
            view()->composer('*', 'App\Http\ViewComposers\UsefulInfoComposer');
            if(strpos(\URL::current(), 'termsofuse') === false && strpos(\URL::current(), 'privacy') === false)
            {
                view()->composer('*', 'App\Http\ViewComposers\LeftSideBarComposer');
            }*/

            if (!\Cache::has('pages')) {
                \App\Caching::pages_set();
            }

            $contactsInfo = \App\Caching::pages_get('pages')->where('slug', 'kontakti')->where('published', 1)->first();

            $slug1 = \Request::segment(1);
            $slug2 = \Request::segment(2);
            $slug = $slug1.($slug2? '/'.$slug2: '');
            $ob = \App\Caching::pages_get('pages')->where('slug', $slug)->where('published', 1)->first();

            //default parent
            if(empty($ob) || (isset($ob->parent_id) && $ob->parent_id == 0)){
                $ob = \App\Caching::pages_get('pages')->where('slug', 'kursove')->where('published', 1)->where('in_menu', 1)->first();
            }

            if($ob->parent_id != 0){
                $parents = \App\Caching::getParents($ob->parent_id);
                $parent_id = end($parents);
                $parent = \App\Caching::pages_get('pages')->where('id', $parent_id)->where('published', 1)->where('in_menu', 1)->first();
            }
            $leftMenuTitle = isset($parent)? $parent->title: $ob->title;

            $leftSideBarMenu = \App\Caching::childrenForMenu($ob->parent_id != 0? $parent_id:$ob->id);

            //testimonials
            $testim_parent = \App\Caching::pages_get('pages')->where('slug', 'mnenia-clienti')->first();
            $testimonial = \App\Caching::pageByIdWhitPaging_get($testim_parent->id);
            $leftTesimonial = $testimonial[0];
            $list = \App\Caching::pages_get('pages')->where('published', 1)->where('in_menu', 1)->all();

            //menu active
            foreach (\App\Caching::pages_get('pages')->where('published', 1)->all() as $page) {
                if(\URL::current() == url($page->slug)){
                    $parents = \App\Caching::getParents($page->id);
                    if(in_array($page->parent_id ,$parents)){
                        $activeMenu[$page->parent_id] = true;
                        break;
                    }
                   /* $parent_id = end($parents);
                    $activeMenu[$parent_id] = true;
                    break;*/
                }
            }

            $top_navigation = new \App\Http\Controllers\Admin\CategoriesController($list);
            $usefulInfo_parent = \App\Caching::pages_get('pages')->where('slug', 'polezno')->where('published', 1)->first();
            $usefulInfo = \App\Caching::pages_get('pages')->where('parent_id', $usefulInfo_parent->id)->where('published', 1)->sortByDesc('created_at')->take(3);

            \View::share(compact('leftSideBarMenu', 'leftMenuTitle', 'leftTesimonial', 'contactsInfo', 'top_navigation', 'activeMenu', 'usefulInfo'));
        }

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
