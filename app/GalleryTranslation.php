<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryTranslation extends Model
{
    protected $fillable = ['title', 'description', 'parent_id', 'level', 'slug', 'short_description',
    'meta_title', 'meta_description', 'home', 'published', 'sortorder'];
}
