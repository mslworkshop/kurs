<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagesTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'content', 'short_description', 'meta_title', 'meta_description',
    'published', 'home', 'slug', 'sortorder', 'parent_id', 'level', 'in_menu'];
}
