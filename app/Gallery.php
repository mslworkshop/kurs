<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['title', 'description', 'short_description', 'meta_title', 'meta_description'];
    protected $fillable = ['title', 'description', 'short_description',
    'meta_title', 'meta_description', 'slug', 'parent_id', 'level', 'home', 'published', 'sortorder'];
}
