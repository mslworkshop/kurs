<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
    use \Dimsav\Translatable\Translatable;

    //use SoftDeletes;

    //protected $dates = ['deleted_at'];

    public $translatedAttributes = ['title', 'content', 'short_description', 'meta_title', 'meta_description'];
    protected $fillable = ['title', 'content', 'short_description', 'meta_title', 'meta_description',
    'published', 'home', 'slug', 'sortorder', 'parent_id', 'level', 'in_menu'];

    public function images()
    {
        return $this->morphMany('App\Images', 'object');
        /*return \DB::table('images')
                ->where('object_id', 1)
                ->where('module', 'pages')
                ->where('show', 1)
                ->get();*/
        return $res;
    }
}
