<?php

namespace App;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Cache;
use DB;

use App\Slider;

class Caching
{
    static function sliders_set()
    {
        Cache::put('sliders', Slider::with('translations')->orderBy('sortorder')->get(), 0);
    }

    static function sliders_get()
    {
        return Cache::get('sliders');
    }

    static function news_set()
    {
        Cache::put('news', News::with('translations')->orderBy('created_at', 'desc')->get(), 0);
    }

    static function news_get()
    {
        return Cache::get('news');
    }

    static function pages_set()
    {
        Cache::put('pages', Pages::with('translations')->with('images')->orderBy('sortorder')->get(), 0);
    }

    static function pages_get()
    {
        return Cache::get('pages');
    }

    static function simpleImages_set()
    {
        Cache::put('simple_images', Images::where('object_id', 0)->where('object_type', 'App\SimpleImages')->orderBy('sortorder')->get(), 0);
    }

    static function simpleImages_get()
    {
        return Cache::get('simple_images');
    }

    static function simpleImagesWithPaginate_get($paginate = 32)
    {
        /*$page = isset($_GET['page'])? $_GET['page']: 0;
        return Cache::remember('simple_images_with_paginate:'.$page.':paginate:'.$paginate, 5, function() use($paginate) {
            return Images::where('object_id', 0)->where('object_type', 'App\SimpleImages')->orderBy('sortorder')->paginate($paginate);
        });*/
        $collection = Caching::simpleImages_get('simple_images')->where('object_id', 0)->sortBy('sortorder');

        return Caching::paginationFromCollection($collection, $paginate);
    }

    static function pageByIdWhitPaging_get($parent_id, $paginate = 3)
    {
        /*$page = isset($_GET['page'])? $_GET['page']: 0;
        return Cache::remember('page_by_id_with_paging:page_id:'.$parent_id.':page:'.$page.':paginate:'.$paginate, 5, function() use($parent_id, $paginate) {
            return Pages::with('translations')->where('parent_id', $parent_id)->where('published', 1)->orderBy('sortorder')->paginate($paginate);
        });*/
        //paging options

        //results
        $collection = Caching::pages_get('pages')->where('parent_id', $parent_id)->where('published', 1)->sortBy('sortorder');

        return Caching::paginationFromCollection($collection, $paginate);
    }

    /**
     * Pagination by collection objects
     * @param $collection (objects) collection objects
     * @param $perPage (int) how pages show
     * @return pagination object (object)
     */
    static function paginationFromCollection($collection, $perPage)
    {
        $page = isset($_GET['page'])? $_GET['page']: 1;
        //Slice the collection to get the items to display in current page
        $objects = $collection->slice(($page-1) * $perPage, $perPage)->all();

        //Create our paginator and pass it to the view
        return new \Illuminate\Pagination\LengthAwarePaginator($objects, count($collection), $perPage, $page, ['path' =>
            \URL::current(), 'query' => \Request::query()]);
    }

    /**
    * Get parent id`s.
    * @param $id
    * @param $model in cache
    * @return \Illuminate\Http\Response
    */
    static function getParents($id, $cache = 'pages')
    {
        $parents = [];
        $z = $id;
        while ($z != 0) {
            if($cache == 'pages')
            {
                $obj = Caching::pages_get('pages')->where('id', $z)->first();
            }
            $z = $obj->parent_id;
            $parents[] = $obj->id;
        }

        return $parents;

    }

    static function childsIds($id){

        $objects = Caching::pages_get('pages')->where('parent_id', $id)->all();
        $ids = [];
        if (!empty($objects)){
            foreach ($objects as $v) {
                $ids[] = $v->id;
                $ids = array_merge($ids, Caching::childsIds($v->id));
            }
        }
        return $ids;
    }

    static function childrenForMenu($id){

        $objects =Caching::pages_get('pages')->where('parent_id', $id)->where('in_menu', 1)->all();
        $subs = [];
        if (!empty($objects)){
            foreach ($objects as $v) {
                $v->children = Caching::childrenForMenu($v->id);
                $subs[] = $v;
            }
        }
        return $subs;
    }

    static function childs($id){

        $objects = Caching::pages_get('pages')->where('parent_id', $id)->where('level', 1)->all();
        $ob = [];
        if (!empty($objects)){
            foreach ($objects as $v) {
                $ob[] = $v;
                $ob = array_merge($ob, Caching::childs($v->id));
            }
        }
        return $ob;
    }

    public function grab(Route $route, Request $request)
    {
        $key = $this->keygen($request->url());

        if( Cache::has($key)){
            return Cache::get($key);
        }
    }

    public function set(Route $route, Request $request, Response $response)
    {
        $key = $this->keygen($request->url());

        if(!Cache::has($key)){
            Cache::put($key, $response->getContent(), 1);
        }
    }

    protected function keygen($url)
    {
        return 'route_' . str_slug( $url );
    }
}
