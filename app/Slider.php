<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['title', 'text1', 'text2'];
    protected $fillable = ['title', 'text1', 'text2', 'images', 'url', 'sortorder'];
}
