<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use Cache;
use App\Caching;
use App\Pages;

class LeftSidebarComposer
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function compose(View $view)
    {
        if (!Cache::has('pages')) {
            Caching::pages_set();
        }

        $slug = \Request::segment(1);
        $ob = Caching::pages_get('pages')->where('slug', $slug)->where('published', 1)->first();
        if(empty($ob))
        {
            //default parent
            $ob = Caching::pages_get('pages')->where('slug', 'courses')->where('published', 1)->where('in_menu', 1)->first();
        }

        if($ob->parent_id != 0)
        {
            $parent_id = Caching::getParent($ob->parent_id);
            $parent = Caching::pages_get('pages')->where('id', $parent_id)->where('published', 1)->where('in_menu', 1)->first();
        }
        $leftMenuTitle = isset($parent)? $parent->title: $ob->title;

        $leftSideBarMenu = Caching::childrenForMenu($ob->parent_id != 0? $parent_id:$ob->id);

        $view->with(compact('leftSideBarMenu', 'leftMenuTitle'));
    }

}
