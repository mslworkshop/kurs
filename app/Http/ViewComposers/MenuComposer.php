<?php

namespace App\Http\ViewComposers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Contracts\View\View;

use Cache;
use App\Caching;
use App\Pages;

class MenuComposer
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function compose(View $view)
    {
        if (!Cache::has('pages')) {
            Caching::pages_set();
        }

        $list = Caching::pages_get('pages')->where('published', 1)->where('in_menu', 1)->all();

        //menu active
        foreach (Caching::pages_get('pages')->where('published', 1)->all() as $page) {
            if(strpos(\URL::current(), $page->slug) !== false)
            {
                $parent_id = Caching::getParent($page->id);
                $data['activeMenu'][$parent_id] = true;
                break;
            }
        }

        $data['top_navigation'] = new \App\Http\Controllers\Admin\CategoriesController($list);

        $view->with($data);
    }

}
