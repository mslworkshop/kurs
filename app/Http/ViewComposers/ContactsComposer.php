<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use Cache;
use App\Caching;
use App\Pages;

class ContactsComposer
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function compose(View $view)
    {
        if (!Cache::has('pages')) {
            Caching::pages_set();
        }

        $contactsInfo = Caching::pages_get('pages')->where('slug', 'contacts')->where('published', 1)->first();
        $view->with(compact('contactsInfo'));
    }

}
