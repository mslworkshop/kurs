<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use Cache;
use App\Caching;
use App\Pages;

class UsefulInfoComposer
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function compose(View $view)
    {
        if (!Cache::has('pages')) {
            Caching::pages_set();
        }

        $usefulInfo_parent = Caching::pages_get('pages')->where('slug', 'useful-information')->where('published', 1)->first();
        $usefulInfo = Caching::pages_get('pages')->where('parent_id', $usefulInfo_parent->id)->where('published', 1)->sortByDesc('created_at')->take(3);

        $view->with(compact('usefulInfo'));
    }

}
