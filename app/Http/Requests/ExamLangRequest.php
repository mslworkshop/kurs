<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExamLangRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            // 'phone' => 'required|numeric|digits_between:1,10',
            // 'language' => 'required',
            // 'level' => 'required',
            'course' => 'required',
            'days' => 'required',
            'timing' => 'required',
            'form_of_education' => 'required',
            'educ_form' => 'required',
            'office' => 'required',
            // 'check_privacy' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'language.required' => trans('app.field').trans('app.language').trans('app.is_required'),
            'level.required' =>  trans('app.field').trans('app.level').trans('app.is_required'),

            'course.required' =>  trans('app.field').trans('app.course').trans('app.is_required'),
            'days.required' =>  trans('app.field').trans('app.days').trans('app.is_required'),
            'timing.required' =>  trans('app.field').trans('app.timing').trans('app.is_required'),
            'form_of_education.required' =>  trans('app.field').trans('app.format_of_education').trans('app.is_required'),
            'educ_form.required' =>  trans('app.field').trans('app.form_of_education').trans('app.is_required'),
            'check_privacy.required' =>  trans('app.field').trans('app.privacy').trans('app.is_required'),
            'office.required' =>  trans('app.field').trans('app.office').trans('app.is_required'),
        ];
    }
}
