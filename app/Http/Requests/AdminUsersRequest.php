<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\Request as Http_Request;

class AdminUsersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Http_Request $request)
    {
        $password = '';
        if($request->password)
        {
            $password = 'required|min:6';
        }
        return [
            'name' => 'required|min:2',
            'email' => 'required|email|unique:users,email,'.($request->route('id')? $request->route('id'): ''),
            'password' => $password,
        ];
    }
}
