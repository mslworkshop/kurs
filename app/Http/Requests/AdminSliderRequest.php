<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminSliderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'url' => 'required|min:2',
        ];

        foreach (\Config::get('translatable.locales') as $locale) {
            $rules['title_'.$locale] = 'sometimes|required|min:2|max:255';
            $rules['text1_'.$locale] = 'sometimes|required|min:2|max:255';
            $rules['text2_'.$locale] = 'sometimes|required|min:2|max:255';
        }
        // check if uploaded images
        $nbr = count($this->input('photo')) - 1;
        foreach(range(0, $nbr) as $index) {
            $rules['photo.'.$index] = 'image|max:'.\Config::get('constants.settings.max_upload_size').'|mimes:jpeg,gif,png,bmp,jpg';
        }

        return $rules;

    }
}
