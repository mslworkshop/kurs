<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminNewsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'slug' => 'sometimes|max:255|unique:news,slug',
            'created_at' => 'required|date',
        ];

        foreach (\Config::get('translatable.locales') as $locale) {
            $rules['title_'.$locale] = 'required|min:2|max:255';
            $rules['content_'.$locale] = 'required';
        }
        // check if uploaded images
        $nbr = count($this->input('photo')) - 1;
        foreach(range(0, $nbr) as $index) {
            $rules['photo.'.$index] = 'image|max:'.\Config::get('constants.settings.max_upload_size').'|mimes:jpeg,gif,png,bmp,jpg';
        }

        return $rules;
    }
}
