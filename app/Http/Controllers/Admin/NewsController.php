<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminNewsRequest;
use App\Http\Controllers\ImageController;

use Cache;
use App\Caching;
use App\News;
use App\Images;

class NewsController extends LayoutController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->shareViewData(trans('admin/news.news'), '', route('news.create'));

        if (!Cache::has('news')) {
            Caching::news_set();
        }
        $data['objects'] = Caching::news_get('news');

        return view('admin.news.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['max_file_to_upload'] = ceil(\Config::get('constants.settings.max_upload_size') / 1024).' MB';//intval(ini_get('upload_max_filesize')).'MB';

        $this->shareViewData(trans('admin/news.simple_news'), trans('admin/app.add'),'');
        return view('admin.news.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminNewsRequest $request)
    {
        $item = new News();
        $item->slug = $request['slug'];

        $date = explode('-', $request['created_at']);
        $item->created_at = \Carbon::createFromDate($date[0], $date[1], $date[2]);
        $item->save();

        foreach (\Config::get('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->title = $request['title_'.$locale];
            $item->translateOrNew($locale)->content = $request['content_'.$locale];
            $item->translateOrNew($locale)->meta_title = $request['meta_title_'.$locale];
            $item->translateOrNew($locale)->meta_description = $request['meta_description_'.$locale];
        }

        $item->save();

        Caching::news_set();

        // store images
        if($request->file('photo'))
        {
            $dir = \Config::get('constants.NEWS_DIR').$item->id;
            $image_response = ImageController::storeImages($request, $item->id, $dir, 'App\News');
            if($image_response)
            {
                return $image_response;
            }
        }

        return redirect()->route('news.list')->with('created', trans('admin/news.simple_news').' '.$item->title.' '.trans('admin/app.created').'.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['object'] = Caching::news_get('news')->find($id);
        $data['images'] = Images::where('object_id', $id)->where('object_type', 'App\News')->orderBy('sortorder')->get();
        $data['method'] = 'edit';

        $data['max_file_to_upload'] = ceil(\Config::get('constants.settings.max_upload_size') / 1024).' MB';//intval(ini_get('upload_max_filesize')).'MB';

        $this->shareViewData(trans('admin/news.simple_news') . ' ' . $data['object']->title, trans('admin/app.edit'));

        return view('admin.news.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminNewsRequest $request, $id)
    {
        $item = Caching::news_get('news')->find($id);
        $date = explode('-', $request['created_at']);
        $day = explode(' ', $date[2]);
        $item->created_at = \Carbon::createFromDate($date[0], $date[1], $day[0]);
        //$item->slug = $request['slug'];
        $item->save();

        foreach (\Config::get('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->title = $request['title_'.$locale];
            $item->translateOrNew($locale)->content = $request['content_'.$locale];
            $item->translateOrNew($locale)->meta_title = $request['meta_title_'.$locale];
            $item->translateOrNew($locale)->meta_description = $request['meta_description_'.$locale];
        }

        $item->save();

        Caching::news_set();

        // store images
        if($request->file('photo'))
        {
            $dir = \Config::get('constants.NEWS_DIR').$id;
            $image_response = ImageController::storeImages($request, $id, $dir, 'App\News');
            if($image_response)
            {
                return $image_response;
            }
        }

        if($request->oldsorting)
        {
            $sort = 0;
            foreach ($request->oldsorting as $id) {
                Images::where('id', $id)->update(['sortorder' => $sort, 'title' => $request->image_title[$sort]]);
                $sort++;
            }
        }

        return redirect()->route('news.list')->with('updated', trans('admin/news.simple_news').' '.$item->title.' '.trans('admin/app.updated').'.');
    }

    /**
     * Publish news.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request, $id)
    {
        $item = Caching::news_get('news')->find($id);
        $item->published = $request->published;
        $item->save();

        Caching::news_set();

        return redirect()->route('news.list')->with('updated', trans('admin/news.simple_news').' '.$item->title.' '.trans('admin/app.updated').'.');
    }

    /**
     * Show news to home page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request, $id)
    {
        $item = Caching::news_get('news')->find($id);
        $item->home = $request->home;
        $item->save();

        Caching::news_set();

        return redirect()->route('news.list')->with('updated', trans('admin/news.simple_news').' '.$item->title.' '.trans('admin/app.updated').'.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Caching::news_get('news')->find($id);
        $title = $object->title;

        // delete all created images
        $images = Images::where('object_id', $id)->where('object_type', 'App\News')->get();
        $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.NEWS_DIR').$id.'/');
        foreach ($images as $image) {
            $delete_images = new ImageController;
            $delete_images->deleteImages($dir, $image->filename);
            $image->delete();
        }
        //delete dir
        $success = \File::deleteDirectory($dir);

        $object->delete();

        Caching::news_set();

        return redirect()->route('news.list')->with('deleted', trans('admin/news.simple_news').' '.$title.' '.trans('admin/app.deleted').'.');
    }

    public function deleteImage($id)
    {
        $image = Images::where('id', $id)->where('object_type', 'App\News')->first();
        $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.NEWS_DIR').$image->object_id.'/');

        $image_inst = new ImageController;
        $image_inst->deleteImages($dir, $image->filename);

        $image->delete();

    }

    /**
     * Sorting.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   /* public function sorting(Request  $request)
    {
        foreach ($request->item as $sort => $id) {
            Slider::where('id', $id)->update(['sortorder' => $sort]);
        }

        Caching::news_set();

        return response(['text' => 'ok'], 200)->header('Content-Type', 'application/json'); //Status code 200: OK
    }*/
}
