<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AdminUsersRequest;
use App\Http\Controllers\Controller;
use App\User;

class UsersController extends LayoutController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->shareViewData(trans('admin/user.users'), '', route('users.create'));

        $data['all_users'] = User::orderBy('created_at', 'desc')->get();

        return view('admin.users.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->shareViewData(trans('admin/user.users'), trans('admin/app.add'));
        $data['method'] = 'create';
        return view('admin.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(AdminUsersRequest $request) {

        $user_data = new User;

        $user_data->name = $request->name;
        $user_data->email = $request->email;
        $user_data->password = bcrypt($request->password);
        $user_data->save();

        return redirect()->route('users.list')->with('created', trans('admin/user.user').' '.$user_data->name.' '.trans('admin/app.created').'.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['object'] = User::find($id);

        $data['method'] = 'edit';

        $this->shareViewData(trans('admin/user.users'), trans('admin/app.edit') . ' ' . $data['object']->name);

        return view('admin.users.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(AdminUsersRequest $request, $id) {

        $user_data = User::find($id);

        $user_data->name = $request->name;
        $user_data->email = $request->email;
        if($request->password)
        {
            $user_data->password = bcrypt($request->password);
        }
        $user_data->save();

        return redirect()->route('users.list')->with('updated', trans('admin/user.user').' '.$user_data->name.' '.trans('admin/app.updated').'.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $user_data = User::find($id);

        User::destroy($id);

        return redirect()->route('users.list')->with('deleted', trans('admin/user.user').' '.$user_data->name.' '.trans('admin/app.deleted').'.');
    }
}
