<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AdminSendEmailsRequest;
use App\Http\Controllers\Controller;
use App\Emails;
use Mail;

class SendEmailsController extends LayoutController
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->shareViewData(trans('admin/app.newsletter'));
        $emails = Emails::orderBy('created_at', 'desc')->get();

        return view('admin.emails.create', compact('emails'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSendEmailsRequest $request)
    {
        $all = Emails::get();
        foreach ($all as $email) {
            Mail::send('admin.emails.layout', $request->all(), function($mail) use ($email, $request){
                $mail->subject($request->subject.' '.trans('app.site_name'));
                $mail->from(\Config('constants.toadmin_email'));
                $mail->to( $email->email);
            });
        }

        return redirect()->back()->with('message', trans('admin/app.newsletter_is_sent'));
    }
}
