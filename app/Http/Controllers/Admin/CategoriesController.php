<?php
//drawmyattention
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Caching;

class CategoriesController extends Controller
{
    /**
    * @var $items
    * @private
    */
    private $items;

    public function __construct($items) {
        $this->items = $items;
    }

    public function htmlList() {
        return $this->htmlFromArray($this->itemArray());
    }

    public function viewList() {
        return $this->itemArray();
    }

    /**
    * Convert a collection of data with parent_id / id data into
    * a nested array that allows for easy traversal and list
    * production.
    *
    * @return array $result The nested array of data.
    */
    private function itemArray() {
        $result = [];
        foreach($this->items as $item) {
            if ($item->parent_id == 0) {
                $newItem = array();
                $newItem['title'] = $item->title;
                $newItem['level'] = $item->level;
                $newItem['slug'] = $item->slug;
                $newItem['children'] = $this->itemWithChildren($item);
                $newItem['parent_id'] = $item->parent_id;
                $newItem['id'] = $item->id;
                $newItem['created_at'] = $item->created_at;
                $newItem['published'] = $item->published;
                $newItem['sortorder'] = $item->sortorder;
                $newItem['in_menu'] = $item->in_menu;
                $newItem['home'] = $item->home;
                array_push($result, $newItem);
            }
        }
        return $result;
    }

    /**
    * Find the individual child nodes item has. From the example documentation,
    * if the $item passed to this method was 'Italian', the result from this
    * would assign a 'Pasta' and 'Pizza' node to the $result array.
    *
    * @param array $item The item for which children will be found.
    * @return array $result
    */
    private function childrenOf($item) {
        $result = [];
        foreach($this->items as $i) {
            if ($i->parent_id == $item->id) {
                $result[] = $i;
            }
        }
        return $result;
    }

    /**
    * Build an array of child nodes that are nested to given item.
    * (I.e.any elements that have a parent_id of the item that
    * is passed into the method).
    *
    * @param array $item
    * @return array $result
    */
    private function itemWithChildren($item) {
        $result = [];
        $children = $this->childrenOf($item);
        foreach ($children as $child) {
            $newItem = array();
            $newItem['title'] = $child->title;
            $newItem['level'] = $child->level;
            $newItem['slug'] = $child->slug;
            $newItem['children'] = $this->itemWithChildren($child);
            $newItem['parent_id'] = $child->parent_id;
            $newItem['id'] = $child->id;
            $newItem['created_at'] = $child->created_at;
            $newItem['published'] = $child->published;
            $newItem['sortorder'] = $item->sortorder;
            $newItem['in_menu'] = $child->in_menu;
            $newItem['home'] = $child->home;
            array_push($result, $newItem);
        }
        return $result;
    }

    private function htmlFromArray($array, $what = 'options', $pref = '') {

        $html = '';
        foreach($array as $item)
        {
            $selected = $disabled = $curr = '';

            $pref = str_repeat(' &mdash; ', $item['level']);

            if(\Request::segment(2) == 'pages' && \Request::route('id'))
            {
                $curr = Caching::pages_get('pages')->where('id', (int)\Request::route('id'))->first();
                $selected = $curr->parent_id == $item['id']? 'selected="selected"': '';
                $disabled = $curr->id == $item['id']? 'disabled="disabled"': '';
            }
            $is_children_count = count($item['children']) > 0;
            if($what == 'order-list')
            {
                $html .= '<ul>';
                $html .= '<li>'.$item['title'].'</li>';
                if($is_children_count)
                {
                    $html .= $this->htmlFromArray($v);
                }
                $html .= '</ul>';
            }
            else
            {

                $html .= '<option value="'.$item['id'].'|'.($item['level']).'"'.$selected.$disabled.'>'.$pref.$item['title'].'</option>';
                if($is_children_count)
                {
                    $html .= $this->htmlFromArray($item['children'], $what);
                }
            }
        }
        return $html;
    }

    private function viewFromArray($array, $view) {

        $html = '';
        foreach($array as $object)
        {
            $is_children_count = count($object['children']) > 0;
                for ($i = 1; $i <= $object['level']; $i++) {
                    $object['pref'] = ' &mdash;';
                }

            $html .= view('admin.pages.html_list', compact($object));
            if($is_children_count)
            {
                $html .= $this->viewFromArray($object['children'], $view);
            }

        }
        return $html;
    }

}
