<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminPagesRequest;
use App\Http\Controllers\ImageController;

use Cache;
use App\Caching;
use App\Pages;
use App\Images;

class PagesController extends LayoutController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->shareViewData(trans('admin/pages.pages'), '', route('pages.create'), trans('admin/pages.add_pages'));

        $list = Caching::pages_get('pages')->all();
        $data['list'] = new CategoriesController($list);

        return view('admin.pages.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['max_file_to_upload'] = ceil(\Config::get('constants.settings.max_upload_size') / 1024).' MB';//intval(ini_get('upload_max_filesize')).'MB';

        $categories = Caching::pages_get('pages')->all();
        $data['categories'] = new CategoriesController($categories);

        $this->shareViewData(trans('admin/pages.page'), trans('admin/app.add'),'');

        return view('admin.pages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminPagesRequest $request)
    {
        $item = new Pages();
        $item->slug = $request['slug'];
        $parent_id_level = explode('|', $request['parent_id_level']);
        $item->parent_id = $parent_id_level[0];
        $item->level = $parent_id_level[0] == 0 && $parent_id_level[1] == 0? $parent_id_level[1]: $parent_id_level[1] + 1;

        $item->save();

        foreach (\Config::get('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->title = $request['title_'.$locale];
            $item->translateOrNew($locale)->content = $request['content_'.$locale];
            $item->translateOrNew($locale)->short_description = $request['short_description_'.$locale];
            $item->translateOrNew($locale)->meta_title = $request['meta_title_'.$locale];
            $item->translateOrNew($locale)->meta_description = $request['meta_description_'.$locale];
        }

        $item->save();

        // store images
        if($request->file('photo'))
        {
            $dir = \Config::get('constants.PAGES_DIR').$item->id;
            $image_response = ImageController::storeImages($request, $item->id, $dir, 'App\Pages');
            if($image_response)
            {
                return $image_response;
            }
        }

        Caching::pages_set();

        return redirect()->route('pages.list')->with('created', trans('admin/pages.page').' '.$item->title.' '.trans('admin/app.created').'.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['object'] = Caching::pages_get('pages')->find($id);
        $data['images'] = Images::where('object_id', $id)->where('object_type', 'App\Pages')->orderBy('sortorder')->get();
        $data['method'] = 'edit';

        $data['max_file_to_upload'] = ceil(\Config::get('constants.settings.max_upload_size') / 1024).' MB';//intval(ini_get('upload_max_filesize')).'MB';

        $categories = Caching::pages_get('pages')->all();
        $data['categories'] = new CategoriesController($categories, null);

        $this->shareViewData(trans('admin/pages.page') . ' ' . $data['object']->title, trans('admin/app.edit'));

        return view('admin.pages.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminPagesRequest $request, $id)
    {
        $item = Caching::pages_get('pages')->find($id);
        $parent_id_level = explode('|', $request['parent_id_level']);
        if($parent_id_level[0] != $request->old_cat)
        {
            $children = $this->hasChildren($id);

            if(!in_array($parent_id_level[0], $this->childs($id)))
            {
                //dd('not in array');
                $item->level = $parent_id_level[0] == 0 && $parent_id_level[1] == 0? $parent_id_level[1]: $parent_id_level[1] + 1;
                if(!empty($children))
                {
                    $this->updateHierarchy($id, $item->level);
                }

                $item->parent_id = $parent_id_level[0];
            }

            if(empty($children))
            {
                //dd('empty');
                $item->level = $parent_id_level[0] == 0 && $parent_id_level[1] == 0? $parent_id_level[1]: $parent_id_level[1] + 1;
                $item->parent_id = $parent_id_level[0];
            }

        }
        //$item->slug = $request['slug'];

        foreach (\Config::get('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->title = $request['title_'.$locale];
            $item->translateOrNew($locale)->content = $request['content_'.$locale];
            $item->translateOrNew($locale)->short_description = $request['short_description_'.$locale];
            $item->translateOrNew($locale)->meta_title = $request['meta_title_'.$locale];
            $item->translateOrNew($locale)->meta_description = $request['meta_description_'.$locale];
        }
        $item->slug = $request->slug;
        $item->save();

        // store images
        if($request->file('photo'))
        {
            $dir = \Config::get('constants.PAGES_DIR').$id;
            $image_response = ImageController::storeImages($request, $id, $dir, 'App\Pages');
            if($image_response)
            {
                return $image_response;
            }
        }

        if($request->oldsorting)
        {
            $sort = 0;
            foreach ($request->oldsorting as $id) {
                Images::where('id', $id)->update(['sortorder' => $sort, 'title' => $request->image_title[$sort]]);
                $sort++;
            }
        }

        Caching::pages_set();

        return redirect()->route('pages.list')->with('updated', trans('admin/pages.page').' '.$item->title.' '.trans('admin/pages.edited').'.');
    }

    private function updateHierarchy($id, $level){

        $objects = Pages::where('parent_id', $id)->select(['id', 'parent_id'])->get();
        if (!empty($objects)){
            $level = $level + 1;
            foreach ($objects as $v) {
                $object = Pages::find($v->id);
                $object->level = $level;
                $object->save();
                if(!empty($this->hasChildren($v->id))){
                    $this->updateHierarchy($v->id, $level);
                }
            }
            return true;
        }
        return false;
    }

    private function hasChildren($id){

        $objects = Pages::where('parent_id', $id)->select(['id', 'parent_id'])->get();
        $subs = [];
        if (!empty($objects)){
            foreach ($objects as $v) {
                $v->children = $this->hasChildren($v->id);
                $subs[] = $v;
            }
        }
        return $subs;
    }

    private function childs($id){

        $objects = Pages::where('parent_id', $id)->select(['id'])->get();
        $ids = [];
        if (!empty($objects)){
            foreach ($objects as $v) {
                $ids[] = $v->id;
                $ids = array_merge($ids, $this->childs($v->id));
            }
        }
        return $ids;
    }

    /**
     * Publish pages.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request, $id)
    {
        $item = Caching::pages_get('pages')->find($id);
        $item->published = $request->published;
        $item->save();

        Caching::pages_set();

        return redirect()->route('pages.list')->with('updated', trans('admin/pages.page').' '.$item->title.' '.trans('admin/app.updated').'.');
    }

    /**
     * Show pages in menu bar.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function in_menu(Request $request, $id)
    {
        $item = Caching::pages_get('pages')->find($id);
        $item->in_menu = $request->in_menu;
        $item->save();

        Caching::pages_set();

        return redirect()->route('pages.list')->with('updated', trans('admin/pages.page').' '.$item->title.' '.trans('admin/app.updated').'.');
    }

    /**
     * Show pages to home page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request, $id)
    {
        $item = Caching::pages_get('pages')->find($id);
        $max = Caching::pages_get('pages')->max('home');
        if($request->home != 0)
        {
            $home = ($max + 1);
        }
        else
        {
            $home = $request->home;
        }
        $item->home = $home;
        $item->save();

        Caching::pages_set();

        return redirect()->route('pages.list')->with('updated', trans('admin/pages.page').' '.$item->title.' '.trans('admin/app.updated').'.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Caching::pages_get('pages')->find($id);
        $title = $object->title;
        $this->deleteHierarchy($id, $object->parent_id);
        // delete all created images
        $images = Images::where('object_id', $id)->where('object_type', 'App\Pages')->get();
        $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$id.'/');
        foreach ($images as $image) {
            $delete_images = new ImageController;
            $delete_images->deleteImages($dir, $image->filename);
            $image->delete();
        }
        //delete dir
        $success = \File::deleteDirectory($dir);

        $object->delete();

        Caching::pages_set();

        return redirect()->route('pages.list')->with('deleted', trans('admin/pages.page').' '.$title.' '.trans('admin/app.deleted').'.');
    }

    private function deleteHierarchy($id, $parent_id){
        $objects = Pages::where('parent_id', $id)->select(['id', 'parent_id', 'level'])->get();
        if (!empty($objects)){
            foreach ($objects as $v) {
                $object = Pages::find($v->id);
                //echo($object->level.'<br>');
                if(!empty($this->hasChildren($v->id)))
                {
                   $this->deleteHierarchy($v->id, $v->id);
                }
                $object->parent_id = $parent_id;
                $object->level = $v->level - 1;
                //echo('id:'.$v->id.' parent_id:'.$object->parent_id.' level:'.$object->level.'<br>');

                $object->save();

            }
            return true;
        }
        return false;
    }

    public function deleteImage($id)
    {
        $image = Images::where('id', $id)->where('object_type', 'App\Pages')->first();
        $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.PAGES_DIR').$image->object_id.'/');

        $image_inst = new ImageController;
        $image_inst->deleteImages($dir, $image->filename);

        $image->delete();

    }

    /**
    * Sorting.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function sorting(Request $request, $id = null)
    {
        //dd($request->d);
        $l = 1;
        $p = 0;
        foreach ($request->d as $val) {
            //print_r($val);
            //update
            //echo('id: '.$val['id'].', parent: 0, level: 0, sortorder: 0<br>');
            $object = Pages::find($val['id']);
            $object->parent_id = 0;
            $object->level = 0;
            $object->sortorder = $p;
            $object->save();

            if(!empty($val['children']))
            {
                $i = 0;
                $this->upCh($val['children'], $val['id'], $l, $i);
            }
            $p++;
        }

        Caching::pages_set();

        //return response(['text' => 'ok'], 200)->header('Content-Type', 'application/json'); //Status code 200: OK
    }

    private function upCh($arr, $parent_id, $l, $position = 0){
        //$objects = Pages::where('parent_id', $id)->select(['id', 'parent_id', 'level'])->get();
        if (is_array($arr)){
            //dd($arr);
            foreach ($arr as $val)
            {
                //print_r($val);
                if(!empty($val['id'])){
                    //update
                    $object = Pages::find($val['id']);
                    $object->parent_id = $parent_id;
                    $object->level = $l;
                    $object->sortorder = $position++;
                    $object->save();
                    //echo('id: '.$val['id'].', parent: '.$parent_id.', level: '.($object->level).', sortorder: '.($object->sortorder).'<br>');
                }elseif(!empty($val[0]['id'])){
                    //break;
                }else{
                    //$this->upCh($arr['children'], $val, ($l+1));
                }

                if(!empty($val['children']))
                {
                    //$position = 0;
                    foreach ($val['children'] as $child)
                    {
                        //update
                        $chobject = Pages::find($child['id']);
                        $chobject->parent_id = $val['id'];
                        $chobject->level = ($l+1);
                        $chobject->sortorder = $position++;
                        $chobject->save();
                        //echo('id: '.$child['id'].', parent: '.$val['id'].', level: '.($chobject->level).', sortorder: '.($chobject->sortorder).'<br>');
                        if(!empty($child['children']))
                        {
                            $this->upCh($child, $parent_id, ($l+1));
                        }

                    }
                }
            }
        }
    }

}
