<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;

use Cache;
use App\Caching;
use App\Pages;
use App\Images;

class SimpleImagesController extends LayoutController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->shareViewData(trans('admin/app.gallery'), '', route('simple.images.create'));

        $data['objects'] = Caching::simpleImages_get()->all();

        return view('admin.images.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['max_file_to_upload'] = ceil(\Config::get('constants.settings.max_upload_size') / 1024).' MB';//intval(ini_get('upload_max_filesize')).'MB';

        $this->shareViewData(trans('admin/app.image'), trans('admin/app.add'),'');

        return view('admin.images.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // store images
        if($request->file('photo'))
        {
            $dir = \Config::get('constants.SIMPLE_IMAGES_DIR').'0';
            $image_response = ImageController::storeImages($request, 0, $dir, 'App\SimpleImages');
            if($image_response)
            {
                return $image_response;
            }
        }

        Caching::simpleImages_set();

        return redirect()->route('simple.images.list')->with('created', trans('admin/app.image').' '.trans('admin/app.created_').'.');
    }

    /**
     * Sorting.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sorting(Request  $request)
    {
        $sort = 0;
        foreach ($request->item as $id) {
            Images::where('id', $id)->where('object_type', 'App\SimpleImages')->update(['sortorder' => $sort]);
            $sort++;
        }

        Caching::simpleImages_set();

        return response(['text' => 'ok'], 200)->header('Content-Type', 'application/json'); //Status code 200: OK
    }

    public function destroy($id)
    {
        $image = Images::where('id', $id)->where('object_type', 'App\SimpleImages')->first();
        $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SIMPLE_IMAGES_DIR').$image->object_id.'/');

        $image_inst = new ImageController;
        $image_inst->deleteImages($dir, $image->filename);

        $image->delete();

        Caching::simpleImages_set();

        return redirect()->back()->with('deleted', trans('admin/app.image').' '.trans('admin/app.deleted').'.');

    }

}
