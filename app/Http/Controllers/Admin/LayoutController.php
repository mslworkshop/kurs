<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class LayoutController extends Controller
{
    public function __construct() {

        if (!\Cache::has('pages')) {
            \App\Caching::pages_set();
        }

        if (!\Cache::has('simple_images')) {
            \App\Caching::simpleImages_set();
        }

    }

    protected function shareViewData($base_location = null, $location_info = null, $add_button = null, $add_button_text = null) {
        View::share('base_location', $base_location);
        View::share('add_button', $add_button);
        View::share('add_button_text', $add_button_text);
        View::share('location_info', $location_info);
    }
}
