<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AdminSliderRequest;
use App\Http\Controllers\Controller;

use Cache;
use App\Caching;
use App\Slider;

class SliderController extends LayoutController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->shareViewData(trans('admin/slider.slider'), '', route('sliders.create'));

        if (!Cache::has('sliders')) {
            Caching::sliders_set();
        }
        $data['objects'] = Caching::sliders_get('sliders');

        return view('admin.sliders.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['max_file_to_upload'] = intval(ini_get('upload_max_filesize')).'MB';

        $this->shareViewData(trans('admin/slider.slider'), trans('admin/app.add'),'');
        return view('admin.sliders.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSliderRequest $request)
    {
        $item = new Slider();
        $item->url = $request['url'];
        $item->save();

        foreach (\Config::get('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->title = $request['title_'.$locale];
            $item->translateOrNew($locale)->text1 = $request['text1_'.$locale];
            $item->translateOrNew($locale)->text2 = $request['text2_'.$locale];
        }

        $item->save();

        // store images
        $this->storeImage($request->file('photo'), $item->id);

        Caching::sliders_set();

        return redirect()->route('sliders.list')->with('created', trans('admin/slider.slider').' '.$item->title.' '.trans('admin/app.created').'.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['object'] = Caching::sliders_get('sliders')->find($id);
        $data['method'] = 'edit';

        $data['max_file_to_upload'] = intval(ini_get('upload_max_filesize')).'MB';

        $this->shareViewData(trans('admin/slider.slider'), trans('admin/app.edit') . ' ' . $data['object']->title);

        return view('admin.sliders.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminSliderRequest $request, $id)
    {
        $item = Caching::sliders_get('sliders')->find($id);
        $item->url = $request['url'];
        $item->save();

        foreach (\Config::get('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->title = $request['title_'.$locale];
            $item->translateOrNew($locale)->text1 = $request['text1_'.$locale];
            $item->translateOrNew($locale)->text2 = $request['text2_'.$locale];
        }

        $item->save();

        // store images
        $this->storeImage($request->file('photo'), $item->id);

        Caching::sliders_set();

        return redirect()->route('sliders.list')->with('updated', trans('admin/slider.slider').' '.$item->title.' '.trans('admin/app.updated').'.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Caching::sliders_get('sliders')->find($id);
        $title = $object->title;
        // delete all created images
        $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SLIDERS_DIR'));

        $images = new \App\Http\Controllers\ImageController;
        $images->deleteImages($dir, $object->image);
        $object->delete();

        Caching::sliders_set();

        return redirect()->route('sliders.list')->with('deleted', trans('admin/slider.slider').' '.$title.' '.trans('admin/app.deleted').'.');
    }

    public function deleteImage($id)
    {
        $object = Caching::sliders_get('sliders')->find($id);
        $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SLIDERS_DIR'));

        $images = new \App\Http\Controllers\ImageController;
        $images->deleteImages($dir, $object->image);

        $object->image = '';
        $object->save();

        Caching::sliders_set();

        return redirect()->route('sliders.edit', $id);
    }

    /**
     * Sorting.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sorting(Request  $request)
    {
        foreach ($request->item as $sort => $id) {
            Slider::where('id', $id)->update(['sortorder' => $sort]);
        }

        Caching::sliders_set();

        return response(['text' => 'ok'], 200)->header('Content-Type', 'application/json'); //Status code 200: OK
    }

    /**
     * Store Image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeImage($image, $object_id)
    {
        if($image && $object_id)
        {
            $dir = public_path(\Config::get('constants.UPLOADS_DIR').\Config::get('constants.SLIDERS_DIR'));
            //create album dir if not exists
            if(!\File::isDirectory(public_path(\Config::get('constants.UPLOADS_DIR')))){
                \File::makeDirectory(public_path(\Config::get('constants.UPLOADS_DIR')));
            }
            if(!\File::isDirectory($dir)){
                \File::makeDirectory($dir);
            }

            $filename = $object_id.'_'.$image->getClientOriginalName();
            $image->move($dir, $filename);
            //update file name
            $item = Slider::find($object_id);
            $item->image = $filename;
            $item->save();
        }
    }
}
