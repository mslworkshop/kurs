<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImageManipulationController extends Controller
{
    /**
     * Image Resize & Crop Helper
     * @param  integer $width     width
     * @param  integer $height    height
     * @param  string  $uploadPath Source image directory
     * @param  integer $thumbPath    Source image thumb directory
     * @param  integer $filename     Target width
     * @param  integer $prefix     Thumbs prefix
     * @param  integer $prefix_position     Where the put prefix
     * @param  integer $image_url     Show new created image
     * @param  integer $cropx    Cropped images x
     * @param  integer $cropy    Cropped images y
     * @param  integer  $cropwidth     Cropped width
     * @param  integer  $cropheight     Cropped height
     * @param  bol $resize_base resize original image
     * @param  intiger $neworoginsize   original image size
     * @param  bol $is_watermark insert watermark on image
     * @return responce
     */
    public function makeThumb($width = null, $height = null, $uploadPath, $thumbPath = '', $filename, $prefix = 'thumb_',
        $prefix_position = 'after_string', $image_url = '', $cropx = '', $cropy = '',
        $cropwidth = null , $cropheight = null, $resize_base = false, $neworoginsize = '', $is_watermark = false, $is_thumbs = false){

        /*return response(array('msg' => trans('user/profile.image_is_uploaded'),'image_url' => $image_url), 200) // 200 Status Code: Standard response for successful HTTP request
                    ->header('Content-Type', 'application/json');*/
        if($is_thumbs)
        {
            $extension = strtolower(File::extension($uploadPath.$filename));
            //if thumb path is not set
            if($thumbPath == '')
            {
                $thumbPath = $uploadPath;
            }

            if(!is_dir($thumbPath))
            {
                File::makeDirectory($thumbPath);
            }

            // thumb prefix and prefix position
            if($prefix){
                if($prefix_position == 'before_string'){
                    $thumb_name = $filename.$prefix;
                }else{
                    $thumb_name = $prefix.$filename;
                }
            }else{
                $thumb_name = $prefix.$filename;
            }
            $path_parts = pathinfo($uploadPath.$thumb_name);

            $thumb = $thumbPath.$path_parts['filename'].'.'.$extension;

            //upload original image
            File::copy($uploadPath.$filename, $uploadPath.$thumb_name);

            //make instance for original image
            $img = Image::make($uploadPath.$thumb_name);

            $size = getimagesize($uploadPath.$filename);
            $original_ratio = $size[0] / $size[1];
            $new_ratio = $width / $height;

            if($cropwidth != '' && $cropheight != ''){

                $img->crop($cropwidth, $cropheight, $cropx, $cropy)
                    ->fit($width, $height);

                // create a new Image instance for inserting watermark
                if($width > 500){
                    $watermark = Image::make(public_path().'/assets/front/images/watermark.png');
                    $img->insert($watermark, 'bottom-right');
                }

                $img->save($thumb, 100);

                /*if($cropx != 0 || $cropy != 0)
                {
                    $img->crop($cropwidth, $cropheight, $cropx, $cropx)
                        //->resize($width, $height)
                        ->fit($width, $height, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })
                        ->save($thumbPath.$path_parts['filename'].'.'.$extension, 100);

                }
                else
                {
                    $img->fit($width, $height, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })
                        ->save($thumbPath.$path_parts['filename'].'.'.$extension, 100);
                }*/
                $new_base_image = '';
                $resize_base = false;
                if($resize_base){
                    //resize base image
                    $new_base_image = $this->resizeBaseImage($uploadPath.$filename, $neworoginsize);
                }

                //if($new_base_image){
                    return response(array('msg' => trans('user/profile.image_is_uploaded'),'image_url' => $image_url), 200) // 200 Status Code: Standard response for successful HTTP request
                        ->header('Content-Type', 'application/json');
                //}
            }

            if($size[0] >= $width){
                //print($size[0].'>='.$width);
                if($size[0] >= $width){
                    $w  = $width;
                    $h = round($size[1] * $w / $size[0]);
                }elseif($size[1] >= $height){
                    $h = $height;
                    $w = round($size[0] * $h / $size[1]);
                }else{
                    $w = $size[0];
                    $h = $size[1];
                }

                $sx = 0;
                $sy = 0;
                //$w = $size[0];
                //$h = $size[1];

                //$img->crop($w, $h, $sx, $sy)->save($thumbPath . $path_parts['filename'] . '_'.$width.'x'.$height.'.' . $extension, 100);
                //$img->fit($width, $height)->save($thumbPath . $path_parts['filename'] . '_'.$width.'x'.$height.'.' . $extension, 100);
                $img->fit($width, $height, function ($constraint) {
                    //$constraint->aspectRatio();
                    //$constraint->upsize();
                });

            /*}elseif($original_ratio >= $new_ratio){
                if($original_ratio >= $new_ratio)
                {
                    print('crop: '.$original_ratio.'>='.$new_ratio.'sizes:'.$width.'X'.$height);
                    $w = round($size[1] * $new_ratio);
                    $h = $size[1];
                    $sx = round(($size[0] - $w) / 2);
                    $sy = 0;
                    $img->crop($w, $h, $sx, $sy)->save($thumbPath.$path_parts['filename'].'.'.$extension, 100);

                }
                else
                {
                    //print('crop: otherwise');
                    $w = $size[0];
                    $h = round($size[0] * $height / $width);
                    $sx = 0;
                    $sy = (round($size[1] - $h) / 2);
                    $img->crop($w, $h, $sx, $sy)->save($thumbPath.$path_parts['filename'].'.'.$extension, 100);

                }
        */
            //if height is null
            }elseif($width && $height == null){
                //print('width'.$width);
                $img->fit($width, null, function ($constraint) {
                    //$constraint->aspectRatio();
                    //$constraint->upsize();
                });

            //if width is null
            }elseif($height && $width == null){
                //print('height'.$height);
                $img->fit(null, $height, function ($constraint) {
                    //$constraint->aspectRatio();
                    //$constraint->upsize();
                });
            //otherwise
            } else {

                //print('otherwise: fit by new:'.$width.'X'.$height);
                /*$$h = $size[1] * ($width / $size[0]);
                $w = $width;
                img->fit($w, $h, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($thumbPath.$path_parts['filename'].'.'.$extension, 100);*/
                $img->fit($width, $height);
            }
            /*}else{

                dd('other');
                if($size[0]>$v['width']){
                    $th_x=$v['width'];
                    $th_y=round($size[1]*$th_x/$size[0]);
                }
                else{
                    $th_x=$size[0];
                    $th_y=$size[1];
                    }
                if($th_y>$v['height']){
                    $th_y=$v['height'];
                    $th_x=round($size[0]*$th_y/$size[1]);
                }
                $sx=0;
                $sy=0;
                $w=$size[0];
                $h=$size[1];
            }*/

            // create a new Image instance for inserting watermark
            if($width > 500 && $is_watermark){
                $watermark = Image::make(public_path().'/assets/front/images/watermark.png');
                $img->insert($watermark, 'bottom-right');
            }

            //save thumb
            $img->save($thumb, 100);

        }

        $new_base_image = '';
        if($resize_base){
            //resize base image
            $new_base_image = $this->resizeBaseImage($uploadPath.$filename, $neworoginsize);
        }

        if($new_base_image){
            return response(array('msg' => trans('user/profile.image_is_uploaded'),'image_url' => $image_url), 200) // 200 Status Code: Standard response for successful HTTP request
                    ->header('Content-Type', 'application/json');
        }

    }

    /**
     * [resizeBaseImage resize if image > 2mb]
     * @param  [type] $image [path and filename]
     * @return [type]        [description]
     */
    public function resizeBaseImage($image, $newsize = null){

        if(!$newsize){
            $newsize = 2000;//\Config::get('constants.global_base_image_width_resize');
        }

        //make instance for original image
        $img = Image::make($image);

        /*$size = getimagesize($image);
        if($newsize != '' && $size[0]> $newsize){
            $w = $newsize;
            $h = round($size[1] * $w / $size[0]);
        }
        else{
            $w = $size[0];
            $h = $size[1];
        }*/
        return $img->resize($newsize, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($image, 100);

    }

    /**
     * [deleteImages delete image by our name]
     * @param  [string] $images_dir [image destination]
     * @param  [string] $image_name [full image name without prefix]
     */
    public function deleteImages($images_dir, $image_name) {

        if($images_dir && $image_name){
            foreach (glob($images_dir."*".$image_name."*") as $filename) {
                @unlink($filename);
            }
        }
}
}
