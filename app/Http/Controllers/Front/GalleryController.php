<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cache;
use App\Caching;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images =  Caching::simpleImagesWithPaginate_get();
        //
        $obj = Caching::pages_get('pages')->where('slug', 'galeria')->first();
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];
        view()->share('metas', compact('metas'));
        view()->share('title', $obj->title);

        $pageInstance = new \App\Http\Controllers\Front\PagesController();
        view()->share('breadcrumbs', $pageInstance->breadcrumbs($obj->id));

        return view('front.pages.gallery_list', compact('images'));

    }
}
