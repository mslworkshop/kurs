<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cache;
use App\Caching;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Cache::has('sliders')) {
            Caching::sliders_set();
        }

        if (!Cache::has('pages')) {
            Caching::pages_set();
        }

        $slider = Caching::sliders_get('sliders')->all();

        //home pages
        $home_pages = Caching::pages_get('pages')->where('published', 1)->reject(function ($item) {
                        return $item['home'] <= 0;
                    });
        $home_pages = $home_pages->sortBy('home')->values()->all();

        //courses
        $home_course_parent = Caching::pages_get('pages')->where('slug', 'kursove')->where('published', 1)->first();
        $home_courses = collect($home_pages)
                        ->where('parent_id', $home_course_parent->id)
                        ->where('level', 1)
                        ->where('published', 1)
                        ->SortBy('sortorder')
                        ->values()
                        ->all();
        return view('front.home', compact('slider','home_pages', 'home_courses'));
    }
}
