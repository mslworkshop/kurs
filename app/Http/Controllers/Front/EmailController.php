<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\EmailRequest;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    /**
     * Email it.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EmailRequest $request)
    {

        \DB::table('emails')->insert(['email' => $request->email, 'created_at' => \Carbon\Carbon::now()->toDateTimeString()]);

        return redirect()->back()->with('email_ok', trans('app.successful'));
    }
}
