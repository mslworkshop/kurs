<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ContactsRequest;
use App\Http\Requests\LangRequest;
use App\Http\Requests\ExamLangRequest;

use App\Http\Controllers\Controller;

use Cache;
use File;
use App;
use App\Caching;
use App\Pages;

use App\Services\SiteMap;
use View;

class PagesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category, $slug = null)
    {
        $slug = $category.(isset($slug)? '/'.$slug: '');
        $obj = Caching::pages_get('pages')->where('slug', $slug)->where('published', 1)->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];
        if (in_array($obj->parent_id, array(30, 43))) {
            $more = Caching::pages_get('pages')
                ->where('parent_id', $obj->parent_id)
                ->where('published', 1)
                ->take(5);
        }

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));
        return view('front.pages.view', compact('obj', 'more'));
    }

    /**
     * Show the form for conatcts.
     *
     * @return \Illuminate\Http\Response
     */
    public function contacts()
    {
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'kontakti')->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('contactsLeftInfo', $obj->content);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.contacts', compact('obj'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeContacts(ContactsRequest $request)
    {
        $data = ['content' => view('front.emails.contacts', $request->all())];
        \Mail::send('admin.emails.layout', $data, function($mail) use ($request){
            $mail->subject(trans('app.contacts_subject').' '.trans('app.site_name'));
            $mail->from($request->email);
            $mail->to(\Config('constants.toadmin_email'));
        });

        //return redirect()->back()->with('successfuly_sended_request', true);
         return redirect('/blagodarim-vi');
    }

    /**
     * Show the useful Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function news()
    {
        $slug = \Request::segment(1);
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', $slug)->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];
        $objects = Caching::pageByIdWhitPaging_get($obj->id, 4);

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.news_list', compact('objects'));
    }

    /**
     * Show the testemonials.
     *
     * @return \Illuminate\Http\Response
     */
    public function testimonials()
    {
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'mnenia-clienti')->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];
        $objects = Caching::pageByIdWhitPaging_get($obj->id, 5);

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.testimonials_list', compact('objects'));
    }

    /**
     * Show the sitemap.
     *
     * @return \Illuminate\Http\Response
     */
    public function sitemap()
    {
        $list = Caching::pages_get('pages')->where('published', 1)->where('in_menu', 1)->all();
        $sitemap = new \App\Http\Controllers\Admin\CategoriesController($list);
        $sitemap = $sitemap->viewList();

        view()->share('title', trans('app.sitemap'));
        view()->share('breadcrumbs', [0 => '<li>'.trans('app.sitemap').'</li>']);

        return view('front.partials.sitemap', compact('sitemap'));
    }

    /**
     * Show the team.
     *
     * @return \Illuminate\Http\Response
     */
    public function team()
    {
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'team')->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];
        $objects = Caching::pages_get('pages')->where('published', 1)->where('parent_id', $obj->id)->all();

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.team_list', compact('objects'));
    }

    /**
     * Show the clients.
     *
     * @return \Illuminate\Http\Response
     */
    public function clients()
    {
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'clients')->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];
        $objects = Caching::pageByIdWhitPaging_get($obj->id, 5);

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.clients_list', compact('objects'));
    }

    /**
     * Show the terms of use.
     *
     * @return \Illuminate\Http\Response
     */
    public function termsPrivacy()
    {
        $slug = \Request::segment(1);
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', $slug)->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.termsofuse', compact('obj'));
    }

    /**
     * Show the faq.
     *
     * @return \Illuminate\Http\Response
     */
    public function faq()
    {
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'vaprosi')->first();
        if(empty($obj))
        {
            abort(404);
        }
        $metas = ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description];
        $objects = Caching::pages_get('pages')->where('published', 1)->where('parent_id', $obj->id)->values()->all();

        view()->share('metas', $metas);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.faq', compact('objects'));
    }

    /**
     * Show the form for conatcts.
     *
     * @return \Illuminate\Http\Response
     */
    public function request()
    {
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'zayavka-za-kurs')->first();
        if(empty($obj))
        {
            abort(404);
        }
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.lang_request', compact('obj'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendRequest(LangRequest $request)
    {
        $data = ['content' => view('front.emails.lang_request', $request->all())];
        \Mail::send('admin.emails.layout', $data, function($mail) use ($request){
            $mail->subject(trans('app.request_rate').' '.trans('app.site_name'));
            $mail->from($request->email);
            $mail->to('martin.lazarov2@gmail.com');
            $mail->to(\Config('constants.toadmin_email'));
        });

        //return redirect()->back()->with('successfuly_sended_request', true);
        return redirect('/blagodarim-vi-za-zayavkata');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    var $steps = array(
        1 => 36,
        2 => 56,
        3 => 76,
        4 => 96,
        5 => 116,
    );
    var $passLevel = array(
        1 => 29,
        2 => 15,
        3 => 15,
        4 => 15,
        5 => 15
    );
    /*var $steps = array(
        1 => 16,
        2 => 26,
        3 => 36,
        4 => 46,
        5 => 56
    );
    var $passLevel = array(
        1 => 3,
        2 => 1,
        3 => 1,
        4 => 1,
        5 => 1
    );*/

    public function exam(Request $request)
    {
        // $request->session()->put('exam_step', 0); // deprecated
        $request->session()->put('exam_answers', array());
        // $test = $request->session()->get('user_exam_data');
        // var_dump($test);
        return $this->nextStep($request);
    }

    public function nextStep(Request $request)
    {
        $step = $request->get('step', 0);
        // $step = $request->session()->get('exam_step');
        // echo $step = $request->get('step') || 1;
        // var_dump($step);
        $examAnswers = $request->session()->get('exam_answers');
        if (isset($examAnswers[$step])) {
            // array_pop($examAnswers);
            end($examAnswers);
            $step = key($examAnswers);
            if ($step > count($this->steps)) {
                $step = count($this->steps);
            }
        // var_dump($examAnswers);
        // $request->session()->put('exam_answers');
        }
        $iseLevelPassed = $this->isLevelPassed($step, $request);
        // var_dump($iseLevelPassed);
        // exit;
        if ($step > 0) {
            $examAnswers[$step] = $iseLevelPassed;
            // var_dump($iseLevelPassed);exit;
            $request->session()->put('exam_answers', $examAnswers);
        }
        $next = ++$step;
        if (!isset($this->steps[$next])) {
            // Final Level Reached and submitted Succesfully
            $attachments = $this->tableWithResults($request);
            $this->sendExamResults($request, $iseLevelPassed['passed'], $attachments);
            return redirect('exam-end');
        }
        else if($iseLevelPassed['passed'] === false){
            //Level not passed
            $correct = $iseLevelPassed['correct'];
            $wrong = $iseLevelPassed['wrong'];
            $attachments = $this->tableWithResults($request);
            $this->sendExamResults($request, $iseLevelPassed['passed'], $attachments);
            return redirect('exam-end');
        }
        $questions = $this->getQuestions($next);
        // $request->session()->put('exam_step', $next); // deprecated
        // $step = $next;
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'test-za-nivo-po-angliiski')->first();
        if(empty($obj))
        {
            abort(404);
        }
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));
        return view('front.pages.exam.exam', compact('questions', 'step', 'next', 'obj'));
    }

    public function examEnd(){
        return view('front.pages.exam.examFinishSuccess');
    }

    public function getQuestions($step = 1, $toStep = false)
    {
        $path = base_path().'/data/english_questions.json';
        $obj = json_decode(File::get($path));
        $questions = $obj->questions;
        $toQ = $this->steps[$step];
        $fromQ = isset($this->steps[$step - 1]) ? $this->steps[$step - 1] : 0;
        if ($toStep !== false && $toStep > 0) {
            $fromQ = 0;
            $toQ = $this->steps[$toStep];
        }
        $output = array();
        foreach ($questions as $question) {
            if ($question->id <= $fromQ) {
                continue;
            }
            if ($question->id > $toQ) {
                break;
            }
            $output[] = $question;
        }
        return $output;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function isLevelPassed($step, $answers)
    {
        // var_dump($step);
        if ($step < 1) {
            return true;
        }
        $questions = $this->getQuestions($step);
        $outputAnswers = array();
        $correct = 0;
        $wrong = 0;
                // var_dump($answers->all());
        $userAnswers = array();
        foreach ($questions as $question) {
            $recordAnswer = "";
            if (isset($answers['answer_'.$question->id])) {
                $outputAnswers['answer_'.$question->id] = $recordAnswer = $answers['answer_'.$question->id];
                if ($answers['answer_'.$question->id] === $question->correct) {
                    // echo $question->id." -> Correct! <br>";
                    $correct++;
                }
                else{
                    // echo $question->id.$question->correct." -> Error! <br>";
                    $wrong++;
                }
            }
            else{
                // echo $question->id." -> Error#! <br>";
                $wrong++;
            } 
            $userAnswers[$question->id] = $recordAnswer;
        }
        return array(
            'passed' => $this->passLevel[$step] < $correct,
            'step' => $step,
            'correct' => $correct,
            'wrong' => $wrong,
            'userAnswers' => $userAnswers
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tableWithResults(Request $request)
    {
        $levels = ['A1', 'A2', 'B1', 'B2', 'C1'];
        $rows = ['level', 'total', 'correct', 'wrong', 'percentCorrect'];
        $rowTitles = ['Ниво', 'Общо въпроси', 'Верни', 'Грешни', '%'];
        $totals = array(
            'correct' => 0,
            'wrong' => 0
        );
        /*results = array(
            array(
                'level' => $levels[0],
                'total' => 25,
                'correct' => 15,
                'wrong' => 10,
                'percentCorrect' => 15 / (15 + 10)
            ),
            array(
                'level' => $levels[1],
                'total' => 25,
                'correct' => 15,
                'wrong' => 10,
                'percentCorrect' => 15 / (15 + 10)
            )
        );*/
        $answers = $request->session()->get('exam_answers');
        $i = 0;
        foreach ($answers as $step => $data) {
            $total = $data['correct'] + $data['wrong'];
            $results[] = array(
                'level' => $levels[$i],
                'total' => $total,
                'correct' => $data['correct'],
                'wrong' => $data['wrong'],
                'percentCorrect' => round(
                    ($data['correct'] / $total * 100)
                , 2)
            );
            $totals['correct'] += $data['correct'];
            $totals['wrong'] += $data['wrong'];
            $i++;
        }
        if (count($results) < count($this->steps)) {
            for ($i=count($results); $i < count($this->steps); $i++) { 
                $results[] = array(
                    'level' => $levels[$i],
                    'total' => 20,
                    'correct' => '-',
                    'wrong' => '-',
                            'percentCorrect' => '-'
                );
            }
        }
        // echo "<pre>";
        // return view('front.pages.exam.examResults', compact('results', 'rows', 'rowTitles', 'totals'));
        $userdata = $request->session()->get('user_exam_data');

        /*var_dump($userdata);
        if (!$userdata) {
            return false;
            return redirect('/request-exam');
        }*/
        $last = end($answers);
        $questions = $this->getQuestions(1, $last['step']);
        $userAnswers = array();
        foreach ($answers as $answers) {
            $userAnswers = array_merge($userAnswers, $answers['userAnswers']);
        }
        $keys = [
            "course" => "course",
            "form_of_education" => "form_of_education_exam",
            "days" => "days_other",
            "timing" => "timing_exam",
            "educ_form" => "educ_form_list",
            "office" => "educ_office"
        ];
        $userdata["name"] = isset($userdata["name"]) ? $userdata["name"] : '-';
        // $userdata["email"] = isset($userdata["email"]) ? $userdata["email"] : '-';
        $userdata["phone"] = isset($userdata["phone"]) ? $userdata["phone"] : '-';

        // $userdata["course"] = count($userdata["course"]) > 0 ? $userdata["course"] : array();
        // var_dump($userdata);
        foreach ($keys as $key => $contstant) {
            $text = "";
            foreach ($userdata[$key] as $key => &$value) {
                $text .= config('constants.'.$contstant)[$value].', ';
            }
            $userdata[$key] = $text;
        }
        $userdata["details"] = isset($userdata["details"]) ? $userdata["details"] : '';

        $userTable = (string) View::make('front.pages.exam.userData', compact('userdata'))->render();
        $examResults = (string) View::make('front.pages.exam.examResults', compact('results', 'rows', 'rowTitles'))->render();
        $grandTotal = (string) View::make('front.pages.exam.grand_total', compact('totals'))->render();
        $userAnswersHtml = (string) View::make('front.pages.exam.user_answers', compact('questions', 'userAnswers'))->render();
        $scale = (string) View::make('front.pages.exam.scale')->render();

        $pdfUser = App::make('dompdf.wrapper');
        $pdfAdmin = App::make('dompdf.wrapper');
        $footer = '<p class="footer">Адрес: <span style="color: #299B47!important;">гр. София, ул. Акад. Борис Стефанов №35, South Mall</span> | Телефон: <span><a href="tel:+359894348521" style="color: #299B47!important;">+359 894 348 521</a></span> | Имейл: <span><a href="mailto:office@kurs.bg" style="color: #299B47!important;">office@kurs.bg</a></span></p>';
        $html = $userTable."<br>".$footer.$grandTotal."<br><br>".$userAnswersHtml; //.$examResults
        $html_wrap = (string) View::make('front.pages.exam.html_wrap', compact('html'))->render();
        $pdfUser->loadHTML($html_wrap);
        $userContent = $pdfUser->output();


        $html = $userTable."<br>".$examResults."<br><br>".$grandTotal."<br><br>".$scale."<br><br>".$userAnswersHtml;
        $html_wrap = (string) View::make('front.pages.exam.html_wrap', compact('html'))->render();
        $pdfAdmin->loadHTML($html_wrap);
        $fullContent = $pdfAdmin->output();

        $time = time(true);
        $filePathUser = 'uploads/exams/exam-results'.$time.'.pdf';
        $filePathFull = 'uploads/exams/full-exam-results'.$time.'.pdf';
        file_put_contents($filePathUser, $userContent);
        file_put_contents($filePathFull, $fullContent);
        return array(
            'user' => $filePathUser,
            'full' => $filePathFull
        );
    }
    function requestExam(){
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', 'zayavka-za-kurs')->first();
        if(empty($obj))
        {
            abort(404);
        }
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));

        return view('front.pages.exam.exam_request', compact('questions', 'step', 'obj'));
    }
    function requestExamSubmit(ExamLangRequest $request){
        $request->session()->put('user_exam_data', $request->all());
        return redirect('exam');
    }

    function generatePdf(){
        $html = (string) View::make('front.partials.voucher_pdf', $data);
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($html);
        $output = $pdf->output();
        $filePath = 'uploads/exams/exam-results'.time(true).'.pdf';
        file_put_contents($filePath, $output);
        // $this->sendVouchers($data->toArray(), $filePath);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendExamResults(Request $request, $isPassed, $attachments)
    {
        $userdata = $request->session()->get('user_exam_data');
        $isForUser = true;
        $data = [
            'content' => view('front.emails.exam_results', compact('userdata', 'isForUser'))
        ];
        \Mail::send('admin.emails.layout', $data, function($mail) use ($userdata, $attachments){
            $mail->subject("Благодарим Ви за изпратения тест!");
            $mail->from(\Config('constants.sender'));
            $mail->to(\Config('constants.toadmin_email'));
            $mail->to('martin.lazarov2@gmail.com');
            $mail->to($userdata['email']);
            // $mail->attach($attachments['user']);
        });
        $isForUser = false;
        $data = [
            'content' => view('front.emails.exam_results', compact('userdata', 'isForUser'))
        ];
        \Mail::send('admin.emails.layout', $data, function($mail) use ($userdata, $attachments){
            $mail->subject($userdata['name'] . " – тест за ниво английски език");
            $mail->from(\Config('constants.sender'));
            $mail->to(\Config('constants.toadmin_email'));
            $mail->to('martin.lazarov2@gmail.com');
            $mail->to('grahovska@noavis.com');

            foreach ($attachments as $file) {
                $mail->attach($file);
            }
        });

        //return redirect()->back()->with('successfuly_sended_request', true);

    }


    /**
     * Display a messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function message()
    {
        $obj = Caching::pages_get('pages')->where('published', 1)->where('slug', \Request::segment(1))->first();
        if(empty($obj)){
            abort(404);
        }
        view()->share('metas', ['meta_title' => $obj->meta_title, 'meta_description' => $obj->meta_description]);
        view()->share('title', $obj->title);
        view()->share('breadcrumbs', $this->breadcrumbs($obj->id));
        return view('front.pages.message', compact('obj'));
    }

    /**
     *Breadcrumbs.
     *
     * @return \Illuminate\Http\Response
     */
    public function breadcrumbs($id)
    {
        $path = [];
        $z = $id;
        $k = 1;
        while ($z != 0) {
            $obj = Caching::pages_get('pages')->where('id', $z)->first();
            $z = $obj->parent_id;
            if($obj->id != $id)
            {
                if($obj->level > 0 || sizeof(Caching::childsIds($obj->parent_id)) == 0)
                {
                    $path[] = '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="'.url('/'.$obj->slug).'"><span itemprop="name">'.$obj->title.'</span></a><meta itemprop="position" content="'.$k.'" /></li>';
                    $k++;
                }
                else
                {
                    $path[] = '<li><a href="javascript:void(0);">'.$obj->title.'</a></li>';
                }
            }
            else
            {
                $path[] = '<li class="active"><span itemprop="title">'.$obj->title.'</span></li>';
            }
        }
        return array_reverse($path);

    }

    public function siteMapXml(SiteMap $siteMap)
    {
        $map = $siteMap->getSiteMap();

        return response($map)->header('Content-type', 'text/xml');
    }
}
