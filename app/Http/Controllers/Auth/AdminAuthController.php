<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AdminAuthController extends Controller {

    protected $redirectPath = '/admin';

    protected $loginPath = '/admin/login';

    protected $redirectAfterLogout = '/admin/login';

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;


    public function getLogin()
    {
        return view('admin.auth.login');
    }

    public function getPassword()
    {
        return view('admin.auth.password');
    }

    public function getRegister()
    {
        return view('admin.auth.register');
    }

    public function getReset()
    {
        return view('admin.auth.reset');
    }


}