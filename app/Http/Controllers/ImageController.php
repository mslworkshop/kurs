<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Images;

class ImageController extends ImageManipulationController
{

    static function storePageImage($request, $object_id, $title = '', $sorting = false, $upload_dir, $module, $description = '')
    {
        $request_file = $request;

        //validation
        $validation = ImageController::validateImageBySettings($request_file, true);
        //response when message validation not maxsize
        if($validation){
            return $validation;
        }

        $guid = ImageController::uuid();
        $newFilename = strtolower($guid.'.'.$request_file->getClientOriginalExtension());

        $album = (object)[];
        $album->dir = public_path(\Config::get('constants.UPLOADS_DIR').$upload_dir);
        //create dir if not exists
        if(!\File::isDirectory(public_path(\Config::get('constants.UPLOADS_DIR')))){
            \File::makeDirectory(public_path(\Config::get('constants.UPLOADS_DIR')));
        }
        /*if(!\File::isDirectory(public_path(\Config::get('constants.UPLOADS_DIR').$module))){
            \File::makeDirectory(\Config::get('constants.UPLOADS_DIR').$module);
        }*/
        if(!\File::isDirectory($album->dir)){
           \File::makeDirectory($album->dir);
        }

        //delete auth user cache
        //Caching::deleteAuthUserCache(Auth::user()->id);

        //upload original image
        $request_file->move($album->dir, $newFilename);

        if($sorting)
        {
            // add images in db
            $image = new Images;
            $image->filename = $newFilename;
            $image->title = $title;
            $image->description = $description;
            $image->object_id = $object_id;
            $image->object_type = $module;
            $image->save();
            // create thumbs
            //ImagesController::createImageThumbs($newFilename, $request_file, $album, $guid, null, 189);

            //Caching::userImages_set(Auth::user()->id);
            return $image;
        }
        else
        {
            //$new_thumbs_res = ImagesController::createImageThumbs($newFilename, $request_file, $album, $guid, null, 189);

            //Caching::userImages_set(Auth::user()->id);

            //return $new_thumbs_res;
        }

    }

    static function validateImageBySettings($request_file, $is_cover = false){

        $rules = ['photo' => 'image|max:'.\Config::get('constants.settings.max_upload_size').'|mimes:jpeg,gif,png,bmp,jpg'];

        $validator = \Validator::make(['photo' => $request_file], $rules);
        if ($validator->fails())
        {
            return response($validator->messages(), 422)->header('Content-Type', 'application/json');// 422 Status Code: Unprocessable Entity
        }
    }

    static function storeOldImage($newFilename, $user_image)
    {
        $albums = Albums::find($user_image->album_id);
        $album = (object)[];
        $album->dir = public_path().\Config::get('constant.uploads_dir').\Config::get('constant.albums_dir').$user_image->album_id.'/';
        $album->id = $user_image->album_id;

        //delete default old profile image
        $this->deleteImages($album->dir, $user_image->filename);

        //update new image name
        $albums->userImages()->where('id', $user_image->id)->update(['filename' => $newFilename]);

        return $album;
    }

    static function createImageThumbs($filename, $request_file, $album, $guid, $crop, $returnWidthSize = 1010, $what_image = '')
    {
        $for_preview = $sizes = '';

        if($what_image == 'avatar')
        {
           // $sizes = \Config::get('constant.AVATAR_SIZES');
        }
        else
        {
            //$sizes = \Config::get('constant.IMAGE_SIZES');
        }
        if($sizes)
        {
            foreach ($sizes as $value) {

                $image_url = asset(\Config::get('constant.UPLOAD_DIR').$album->dir.'/'.$value['width'].'_'.$guid.'.'.strtolower((is_object($request_file)? $request_file->getClientOriginalExtension(): $request_file)));

                //create cache if avatar images
                if($what_image == 'avatar' && in_array($value, $sizes)){
                    //Caching::userAvatars_set(Auth::user()->id, $value['width'], $image_url);
                }

                //return image destination
                if($value['width'] == $returnWidthSize){

                    $for_preview = makeThumb($value['width'], $value['height'], $album->dir, '', $filename, $value['width'].'_', '', $image_url, (isset($crop)? $crop->x: ''), (isset($crop)? $crop->y: ''), (isset($crop)? $crop->width: ''), (isset($crop)? $crop->height: ''), true, '', true);

                }else{
                    makeThumb($value['width'], $value['height'], $album->dir, '', $filename, $value['width'].'_', '', '', (isset($crop)? $crop->x: ''), (isset($crop)? $crop->y: ''), (isset($crop)? $crop->width: ''), (isset($crop)? $crop->height: ''), false, '', true);
                }

            }
        }
        else
        {
            makeThumb('', '', $album->dir, '', $filename, '', '', '', '', '', '', '', false, '', true);
            $for_preview = $filename;
        }


        return $for_preview;

    }

    /**
     * Sort images.
     *
     * @param  Request  $request
     * @return Response
     */
    public function sorting(Request $request)
    {
        if(!empty($request->all()))
        {
            $user = User::find(Auth::user()->id);
            $k = 0;
            $album = explode('|',$request->item[0]);
            foreach ($request->item as $item)
            {
                if(strpos($item, '|')>0){
                    $ids = explode('|', $item);
                    $user->images()->where('album_id', $ids[0])->where('id', $ids[1])->update(['sortorder' => $k]);
                    $visible_images_ids[] = $ids[1];
                    $k++;
                }
            }
            //update not if not visible images
            /*foreach (Caching::userImagesByAlbum_get(Auth::user()->id, $album[0]) as $i => $v)
            {
                if(!in_array($v['id'], $visible_images_ids))
                {
                    $user->images()->where('album_id', $album[0])->where('id', $v['id'])->update(['sortorder' => $k + 1]);
                    $k++;
                }
            }*/

            //Caching::userImages_set(Auth::user()->id);
        }
        else
        {
            abort(404);
        }

    }

    static function storeImages($request, $object_id, $dir, $module)
    {
        $i = 0;
        foreach ($request->file('photo') as $val)
        {
            if($val)
            {
                $title = $request->image_title[$i]? $request->image_title[$i]: '';
                $image = ImageController::storePageImage($val, $object_id, $title, true, $dir, $module);
                //if errors
                if($image->original)
                {
                    return back()->withErrors($image->original);
                }
                //$img_ids[] = $image->id;
                $images_new[$i] = $image;
                $i++;
            }
        }
        //update count images in album and new images sort
        if($i > 0 )
        {
            //sort uploaded images
            $sorting = $request->sorting? explode(',', str_replace('item-', '', $request->sorting)): '';
            if(!empty($sorting))
            {
                //update all images in album with sortorder to rand
                Images::where('object_id', $object_id)->where('object_type', $module)->update(['sortorder' => rand(1000, 1200)]);

                foreach ($sorting as $k => $v)
                {
                    if(isset($images_new[$k])){
                        $images_new[$k]->update(['sortorder' => $k]);
                    }
                }
            }
        }
    }

    static function uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
