<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::filter( 'cache.grab', 'App\Caching@grab');
Route::filter( 'cache.set', 'App\Caching@set');


Route::get('/ezikovi-kursove', function(){ 
    return Redirect::to('/', 301); 
});

Route::get('/', ['as' => 'home', 'uses' => 'Front\HomeController@index'])->before('cache.grab')->after('cache.set');

Route::group(['prefix' => 'admin'], function() {

    Route::group(['middleware' => 'auth.admin'], function() {
        //Route::get('/', ['as' => 'admin.home', 'uses' => 'Admin\HomeController@index']);
        Route::get('/', ['as' => 'admin.home', 'uses' => 'Admin\PagesController@index']);

        //users
        Route::group(['prefix' => 'users'], function() {
            Route::get('/', ['as' => 'users.list', 'uses' => 'Admin\UsersController@index']);
            Route::get('/{id?}/edit', ['as' => 'users.edit', 'uses' => 'Admin\UsersController@edit']);
            Route::post('/{id?}/edit', ['as' => 'users.edit', 'uses' => 'Admin\UsersController@update']);
            Route::get('/create', ['as' => 'users.create', 'uses' => 'Admin\UsersController@create']);
            Route::post('/create', ['as' => 'users.create', 'uses' => 'Admin\UsersController@store']);
            Route::delete('/{id?}/destroy', ['as' => 'users.destroy', 'uses' => 'Admin\UsersController@destroy']);
        });

        //sliders
        Route::group(['prefix' => 'sliders'], function() {
            Route::get('/', ['as' => 'sliders.list', 'uses' => 'Admin\SliderController@index']);
            Route::get('/{id?}/edit', ['as' => 'sliders.edit', 'uses' => 'Admin\SliderController@edit']);
            Route::post('/{id?}/edit', ['as' => 'sliders.edit', 'uses' => 'Admin\SliderController@update']);
            Route::get('/create', ['as' => 'sliders.create', 'uses' => 'Admin\SliderController@create']);
            Route::post('/create', ['as' => 'sliders.create', 'uses' => 'Admin\SliderController@store']);
            Route::delete('/{id?}/destroy', ['as' => 'sliders.destroy', 'uses' => 'Admin\SliderController@destroy']);
            Route::post('/sorting', ['as' => 'sliders.sorting', 'uses' => 'Admin\SliderController@sorting']);
            Route::get('/{id}/delete-image/', ['as' => 'sliders.deleteimage', 'uses' => 'Admin\SliderController@deleteImage']);
        });

        //news
        Route::group(['prefix' => 'news'], function() {
            Route::get('/', ['as' => 'news.list', 'uses' => 'Admin\NewsController@index']);
            Route::get('/{id?}/edit', ['as' => 'news.edit', 'uses' => 'Admin\NewsController@edit']);
            Route::post('/{id?}/edit', ['as' => 'news.edit', 'uses' => 'Admin\NewsController@update']);
            Route::post('/{id?}/publish', ['as' => 'news.publish', 'uses' => 'Admin\NewsController@publish']);
            Route::post('/{id?}/home', ['as' => 'news.home', 'uses' => 'Admin\NewsController@home']);
            Route::get('/create', ['as' => 'news.create', 'uses' => 'Admin\NewsController@create']);
            Route::post('/create', ['as' => 'news.create', 'uses' => 'Admin\NewsController@store']);
            Route::delete('/{id?}/destroy', ['as' => 'news.destroy', 'uses' => 'Admin\NewsController@destroy']);
            //Route::post('/sorting', ['as' => 'news.sorting', 'uses' => 'Admin\NewsController@sorting']);
            Route::get('/{id}/delete-image/', ['as' => 'news.deleteimage', 'uses' => 'Admin\NewsController@deleteImage']);
        });

        //pages
        Route::group(['prefix' => 'pages'], function() {
            Route::get('/', ['as' => 'pages.list', 'uses' => 'Admin\PagesController@index']);
            Route::get('/{id?}/edit', ['as' => 'pages.edit', 'uses' => 'Admin\PagesController@edit']);
            Route::post('/{id?}/edit', ['as' => 'pages.edit', 'uses' => 'Admin\PagesController@update']);
            Route::post('/{id?}/publish', ['as' => 'pages.publish', 'uses' => 'Admin\PagesController@publish']);
            Route::post('/{id?}/in_menu', ['as' => 'pages.menu', 'uses' => 'Admin\PagesController@in_menu']);
            Route::post('/{id?}/home', ['as' => 'pages.home', 'uses' => 'Admin\PagesController@home']);
            Route::get('/create', ['as' => 'pages.create', 'uses' => 'Admin\PagesController@create']);
            Route::post('/create', ['as' => 'pages.create', 'uses' => 'Admin\PagesController@store']);
            Route::delete('/{id?}/destroy', ['as' => 'pages.destroy', 'uses' => 'Admin\PagesController@destroy']);
            Route::post('/sorting', ['as' => 'pages.sorting', 'uses' => 'Admin\PagesController@sorting']);
            Route::get('/{id}/delete-image/', ['as' => 'pages.deleteimage', 'uses' => 'Admin\PagesController@deleteImage']);
        });

        //newsletter
        Route::group(['prefix' => 'newsletter'], function() {
            Route::get('/create', ['as' => 'send.emails', 'uses' => 'Admin\SendEmailsController@create']);
            Route::post('/create', ['as' => 'send.emails', 'uses' => 'Admin\SendEmailsController@store']);
        });

        //newsletter
        Route::group(['prefix' => 'simple_images'], function() {
            Route::get('/', ['as' => 'simple.images.list', 'uses' => 'Admin\SimpleImagesController@index']);
            Route::get('/create', ['as' => 'simple.images.create', 'uses' => 'Admin\SimpleImagesController@create']);
            Route::post('/create', ['as' => 'simple.images.create', 'uses' => 'Admin\SimpleImagesController@store']);
            Route::delete('/{id?}/destroy', ['as' => 'simple.images.destroy', 'uses' => 'Admin\SimpleImagesController@destroy']);
            Route::post('/sorting', ['as' => 'simple.images.sorting', 'uses' => 'Admin\SimpleImagesController@sorting']);
        });
    });

    // Authentication routes...
    Route::get('/login', 'Auth\AdminAuthController@getLogin');
    Route::post('/login', 'Auth\AdminAuthController@postLogin');
    Route::get('/logout', 'Auth\AdminAuthController@getLogout');

    // Registration routes...
    Route::get('/register', 'Auth\AdminAuthController@getRegister');
    Route::post('/register', 'Auth\AdminAuthController@postRegister');
});
/* Redirects */

Route::get('/za-nas', function(){ 
    return Redirect::to('/za-nas/misia', 301); 
});
Route::get('/kurs-po-angliiski-ezik', function(){ 
    return Redirect::to('/kursove/angliiski-ezik', 301); 
});
Route::get('/kurs-po-nemski-ezik', function(){ 
    return Redirect::to('/nemski-ezik', 301); 
});
Route::get('/kurs-po-ispanski-ezik', function(){ 
    return Redirect::to('/ispanski-ezik', 301); 
});
Route::get('/kurs-po-frenski-ezik', function(){ 
    return Redirect::to('/frenski-ezik', 301); 
});
Route::get('/kurs-po-italianski-ezik', function(){ 
    return Redirect::to('/italianski-ezik', 301); 
});
Route::get('/kurs-po-ruski-ezik', function(){ 
    return Redirect::to('/ruski-ezik', 301); 
});


Route::get('/онлайн-обучение', function(){ 
    return Redirect::to('/training/onlain-obuchenie', 301); 
});
Route::get('/firmeno-ezikovo-obuchenie', function(){ 
    return Redirect::to('/training/firmeno-obuchenie', 301); 
});
Route::get('/faq', function(){ 
    return Redirect::to('/vaprosi', 301); 
});
Route::get('/blog', function(){ 
    return Redirect::to('/polezno', 301); 
});

Route::get('/otstapki-ezikovi-kursove', function(){ 
    return Redirect::to('/otstapki', 301); 
});
Route::get('/otstapki-ezikovi-kursove/#comment-3', function(){ 
    return Redirect::to('/otstapki', 301); 
});
Route::get('/otstapki-ezikovi-kursove/#comment-2', function(){ 
    return Redirect::to('/otstapki', 301); 
});
Route::get('/individualno-obuchenie-anglijiski-ezik-sofia/', function(){ 
    return Redirect::to('/individualno-obuchenie-po-anglijski-v-sofiya-studentski-grad', 301); 
});
Route::get('/individualno-obuchenie-anglijiski-ezik-sofia/#comment-43', function(){ 
    return Redirect::to('/individualno-obuchenie-po-anglijski-v-sofiya-studentski-grad', 301); 
});

Route::get('/individualno-obuchenie-anglijiski-ezik-sofia/#comment-42', function(){ 
    return Redirect::to('/individualno-obuchenie-po-anglijski-v-sofiya-studentski-grad', 301); 
});
Route::get('/individualno-obuchenie-anglijiski-ezik-sofia/#comment-7', function(){ 
    return Redirect::to('/individualno-obuchenie-po-anglijski-v-sofiya-studentski-grad', 301); 
});
Route::get('/osobenosti-anglijski-ezik-anglichani', function(){ 
    return Redirect::to('/polezno', 301); 
});
Route::get('/top-5-greshki-uchene-angliiski-ezik', function(){ 
    return Redirect::to('/polezno', 301); 
});
Route::get('/zashto-da-uchim-angliiski-ezik', function(){ 
    return Redirect::to('/polezno', 301); 
});

Route::get('/kak-barzo-da-uchim-angliiski-ezik', function(){ 
    return Redirect::to('/polezno', 301); 
});

Route::get('/za-kontakti', function(){ 
    return Redirect::to('/kontakti', 301); 
});



/*Route::get('/ezikovi-kursove', function(){ 
    return Redirect::to('/', 301); 
});*/

/*************/
Route::post('/emailit', ['as' => 'email.it', 'uses' => 'Front\EmailController@index']);
Route::get('/kontakti', ['as' => 'kontakti', 'uses' => 'Front\PagesController@contacts']);
Route::post('/kontakti', ['as' => 'kontakti', 'uses' => 'Front\PagesController@storeContacts']);
Route::get('/galeria', ['as' => 'gallery', 'uses' => 'Front\GalleryController@index']);//->before('cache.grab')->after('cache.set');
Route::get('/mnenia-clienti', ['as' => 'testimonials', 'uses' => 'Front\PagesController@testimonials']);//->before('cache.grab')->after('cache.set');
Route::get('/polezno', ['as' => 'useful.information', 'uses' => 'Front\PagesController@news']);//->before('cache.grab')->after('cache.set');
Route::get('/novini', ['as' => 'news', 'uses' => 'Front\PagesController@news']);//->before('cache.grab')->after('cache.set');
Route::get('/sitemap', ['as' => 'sitemap', 'uses' => 'Front\PagesController@sitemap'])->before('cache.grab')->after('cache.set');
Route::get('/team', ['as' => 'team', 'uses' => 'Front\PagesController@team'])->before('cache.grab')->after('cache.set');
Route::get('/clients', ['as' => 'clients', 'uses' => 'Front\PagesController@clients']);//->before('cache.grab')->after('cache.set');
Route::get('/termsofuse', ['as' => 'termsofuse', 'uses' => 'Front\PagesController@termsPrivacy'])->before('cache.grab')->after('cache.set');
Route::get('/privacy', ['as' => 'privacy', 'uses' => 'Front\PagesController@termsPrivacy'])->before('cache.grab')->after('cache.set');
Route::get('/vaprosi', ['as' => 'faq', 'uses' => 'Front\PagesController@faq'])->before('cache.grab')->after('cache.set');
Route::get('/zayavka-za-kurs', ['as' => 'zayavka-za-kurs', 'uses' => 'Front\PagesController@request']);
Route::post('/zayavka-za-kurs', ['as' => 'zayavka-za-kurs', 'uses' => 'Front\PagesController@sendRequest']);

Route::get('/test-za-nivo-po-angliiski', ['as' => 'requestExam', 'uses' => 'Front\PagesController@requestExam']);
Route::post('/request-exam', ['as' => 'requestExamSubmit', 'uses' => 'Front\PagesController@requestExamSubmit']);
Route::get('/exam', ['as' => 'exam', 'uses' => 'Front\PagesController@exam']);
Route::post('/nextStep', ['as' => 'nextStep', 'uses' => 'Front\PagesController@nextStep']);

Route::get('/exam-end', ['as' => 'examEnd', 'uses' => 'Front\PagesController@examEnd']);
// Route::get('/exam-fail', ['as' => 'examFail', 'uses' => 'Front\PagesController@examFail']);

//thank you pages
Route::get('/thank-you', ['as' => 'message', 'uses' => 'Front\PagesController@message']);
Route::get('/thank-you-request', ['as' => 'message', 'uses' => 'Front\PagesController@message']);

get('sitemap.xml', 'Front\PagesController@siteMapXml');

Route::group(['prefix' => '/{category}'], function() {
    Route::get('/{slug?}', ['uses' => 'Front\PagesController@index'])->before('cache.grab')->after('cache.set');//->where('slug', '([A-Za-z0-9\-\/]+)');
});
//Route::get('{slug}', ['uses' => 'Front\PagesController@index']);//->where('slug', '([A-Za-z0-9\-\/]+)');
